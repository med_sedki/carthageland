package com.tunisie.dotit.carthageland.global;

import android.content.Context;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.tunisie.dotit.carthageland.Utils.Constants;
import com.tunisie.dotit.carthageland.models.Parc;
import com.tunisie.dotit.carthageland.models.Restaurant;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * Created on 2/4/18.
 */

public class AppUtils {

    private static final String TAG = AppUtils.class.getSimpleName();


    /**
     * This method converts device specific pixels to density independent pixels.
     *
     * @param px      A value in px (pixels) unit. Which we need to convert into db
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent dp equivalent to px value
     */
    public static float convertPixelsToDp(float px, @Nullable Context context) {
        if (context == null)
            return 0;
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / (metrics.densityDpi / 160f);
        return dp;
    }


    /**
     * This method converts dp unit to equivalent pixels, depending on device density.
     *
     * @param dp      A value in dp (density independent pixels) unit. Which we need to convert into
     *                pixels
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent px equivalent to dp depending on device density
     */
    public static float convertDpToPixel(float dp, @Nullable Context context) {
        if (context == null)
            return 0;
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return px;
    }

    /**
     * ic_status_validated if user is connected to the Internet
     *
     * @return A boolean value
     */
    public static boolean isNetworkAvailable(@Nullable Context context) {
        if (context != null) {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            return netInfo != null && netInfo.isConnected();

        }

        return false;
    }


    public static String now() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(Constants.Parameters.DATE_FORMAT_NOW);
        return sdf.format(cal.getTime());
    }


    public static String getDate(long time) {
        Calendar cal = Calendar.getInstance(Locale.FRENCH);
        cal.setTimeInMillis(time * 1000);
        String date = DateFormat.format("dd/MM/yyyy", cal).toString();
        return date;
    }


    public static final String md5(final String message) {
        try {

            MessageDigest digest = MessageDigest
                    .getInstance("MD5");
            digest.update(message.getBytes());
            byte messageDigest[] = digest.digest();

            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < messageDigest.length; i++) {
                String h = Integer.toHexString(0xFF & messageDigest[i]);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }


    public static boolean isValidEmail(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static void changeEditErrorTextStyle(final RelativeLayout relativelayout, final EditText editText, final ImageView icon, final Context context) {
        if (editText.getText().length() != 0) {
            editText.setGravity(Gravity.CENTER_VERTICAL);
            //relativelayout.setBackgroundResource(R.drawable.solid_background_gray_error);
            //icon.setImageResource(R.drawable.error_icon);
            icon.setVisibility(View.VISIBLE);
        }
    }

    public static void changeEditTextStyle(final RelativeLayout relativelayout, final EditText editText, final ImageView icon, final Context context) {
        if (editText.getText().length() != 0) {
            editText.setGravity(Gravity.CENTER_VERTICAL);
            //relativelayout.setBackgroundResource(R.drawable.solid_background_gray);
            //icon.setImageResource(R.drawable.error_icon);
            icon.setVisibility(View.INVISIBLE);
        }
    }

    public static List<Restaurant> getHammametRestaurants() {
        List<Restaurant> menuItems = new ArrayList<>();

       /* menuItems.add(new Restaurant( R.string.label_profile, R.string.profile_ic));
        menuItems.add(new Restaurant( R.string.label_mywinnings, R.string.money_ic));
        menuItems.add(new Restaurant( R.string.label_addedwinnings, R.string.number_ic));
        menuItems.add(new Restaurant( R.string.label_rules, R.string.hammer_ic));
        menuItems.add(new Restaurant( R.string.label_cgu, R.string.cgu_ic));
        menuItems.add(new Restaurant( R.string.label_logout, R.string.quit_ic));*/

        return menuItems;
    }

    public static List<Restaurant> getCarthageLandAttractions() {
        List<Restaurant> menuItems = new ArrayList<>();




        return menuItems;
    }

    public static List<Parc> getHammametParcs() {
        List<Parc> menuItems = new ArrayList<>();

        /*menuItems.add(new Parc(R.string.parc1_hammamet_title, R.string.parc1_hammmamet_details));
        menuItems.add(new Parc( R.string.parc2_hammamet_title, R.string.parc2_hammmamet_details));
        menuItems.add(new Parc( R.string.parc3_hammamet_title, R.string.parc3_hammmamet_details));
        menuItems.add(new Parc( R.string.parc4_hammamet_title, R.string.parc4_hammmamet_details));
        menuItems.add(new Parc( R.string.parc5_hammamet_title, R.string.parc5_hammmamet_details));
        menuItems.add(new Parc( R.string.parc6_hammamet_title, R.string.parc6_hammmamet_details));
        menuItems.add(new Parc( R.string.parc7_hammamet_title, R.string.parc7_hammmamet_details));*/


        return menuItems;
    }

    public static List<Restaurant> getTunisRestaurants() {
        List<Restaurant> menuItems = new ArrayList<>();

        /*menuItems.add(new Restaurant( R.string.label_profile, R.string.profile_ic));
        menuItems.add(new Restaurant( R.string.label_mywinnings, R.string.money_ic));
        menuItems.add(new Restaurant( R.string.label_addedwinnings, R.string.number_ic));
        menuItems.add(new Restaurant( R.string.label_rules, R.string.hammer_ic));
        menuItems.add(new Restaurant( R.string.label_cgu, R.string.cgu_ic));
        menuItems.add(new Restaurant( R.string.label_logout, R.string.quit_ic));*/

        return menuItems;
    }

    public static List<Parc> getTunisParcs() {
        List<Parc> menuItems = new ArrayList<>();

        /*menuItems.add(new Parc(R.string.parc1_tunis_title, R.string.parc1_tunis_details));
        menuItems.add(new Parc( R.string.parc2_tunis_title, R.string.parc2_tunis_details));
        menuItems.add(new Parc( R.string.parc3_tunis_title, R.string.parc3_tunis_details));
        menuItems.add(new Parc( R.string.parc4_tunis_title, R.string.parc4_tunis_details));*/

        return menuItems;
    }


    /**
     * Make the first letter an upper case
     */
    public static String capitalizeFirstLetter(String name) {
        if (!TextUtils.isEmpty(name)) {
            return name.substring(0, 1).toUpperCase() + name.substring(1);
        }
        return name;
    }


}
