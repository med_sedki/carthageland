package com.tunisie.dotit.carthageland.fragments;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.tunisie.dotit.carthageland.R;
import com.tunisie.dotit.carthageland.Utils.Constants;
import com.tunisie.dotit.carthageland.activities.HomeActivity;
import com.tunisie.dotit.carthageland.adapters.ProductsAdapter;
import com.tunisie.dotit.carthageland.application.BaseApplication;
import com.tunisie.dotit.carthageland.custom.CustomTextView;
import com.tunisie.dotit.carthageland.global.APIClient;
import com.tunisie.dotit.carthageland.global.APIInterface;
import com.tunisie.dotit.carthageland.global.VoucherItemClickListener;
import com.tunisie.dotit.carthageland.models.Code;
import com.tunisie.dotit.carthageland.models.GiftsResponse;
import com.tunisie.dotit.carthageland.models.Product;
import com.tunisie.dotit.carthageland.models.Produit;
import com.tunisie.dotit.carthageland.models.UseVoucherResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;

/**
 * A simple {@link Fragment} subclass.
 */
public class PromotionsFragment extends BaseFragment implements VoucherItemClickListener {

    View view;
    private SweetAlertDialog progressDoalog;
    BaseApplication myApp;
    LinearLayout _linearL;
    LinearLayout _linearLC;
    private APIInterface mApiInerface;
    private RecyclerView mProductsRecycler;
    private ArrayList<Product> mProdcutsList = new ArrayList<>();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_promotions, container, false);
        myApp = ((BaseApplication) this.getActivity().getApplication());
        HomeActivity.toolbar.setNavigationIcon(R.drawable.back_arrow_ic);
        HomeActivity.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getFragmentManager() != null) {
                    Fragment currentFragment = getFragmentManager().findFragmentById(R.id.flContent);
                    if (currentFragment instanceof PromotionsFragment) {
                        navigateToHomeFragment();

                    }
                } else {
                    HomeActivity.openDrawer();
                }

            }
        });
        _linearL = (LinearLayout) view.findViewById(R.id.promotions);
        _linearLC = (LinearLayout) view.findViewById(R.id.cadeaux);
        mProductsRecycler = view.findViewById(R.id.products_recycler);
        //getPromotions();
        Toolbar toolbarTop = (Toolbar) getActivity().findViewById(R.id.toolbar);
        TextView mTitle = (TextView) toolbarTop.findViewById(R.id.toolbar_title);
        if (getPreferences().getCarthageLandLocation().toString().equals(Constants.Parameters.CL_TUNIS)) {
            mTitle.setText("Parc Tunis");
            getProductsList(Constants.Sites.TUNIS_PARAM);
        } else if (getPreferences().getCarthageLandLocation().toString().equals(Constants.Parameters.CL_HAMMAMET)) {
            mTitle.setText("Parc Hammamet");
            getProductsList(Constants.Sites.HHAMMAMET_PARAM);
        }

        Log.e("TAG", "USER TOKEN " + getPreferences().getToken().toString() + "");
        Log.e("TAG", "USER CLIENT ID " + getPreferences().getUserId().toString() + "");


        //getActivity().setTitle(getString(R.string.products_label));
        // Inflate the layout for this fragment
        return view;
    }

    public void fragmentreplace(Class fragmentClass, String tag) throws IllegalAccessException, java.lang.InstantiationException {
        FragmentManager fragmentManager = getFragmentManager();
        Fragment fragment = (Fragment) fragmentClass.newInstance();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.flContent, fragment, tag);
        fragmentTransaction.addToBackStack("tag"); //this will add it to back stack
        fragmentTransaction.commit();
    }

    private void navigateToHomeFragment() {
        Fragment fragment = null;
        try {
            closeFragment();
            //fragment = HomeFragment.class.newInstance();

        } catch (Exception e) {
            e.printStackTrace();
        }

        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        //fragmentManager.beginTransaction().replace(R.id.flContent, fragment).addToBackStack("tag").commit();
        fragmentManager.popBackStack();
    }

    private void closeFragment() {
        getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();

    }


    public void getProductsList(String site) {
        mApiInerface = APIClient.getClient().create(APIInterface.class);

        //showProgressBar();
        progressDoalog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        progressDoalog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressDoalog.setTitleText("Chargement");
        progressDoalog.setCancelable(true);
        progressDoalog.show();

        /**
         GET List Resources
         **/
        Call<GiftsResponse> call = mApiInerface.getProducts(getPreferences().getToken(), site);
        call.enqueue(new Callback<GiftsResponse>() {
            @Override
            public void onResponse(Call<GiftsResponse> call, retrofit2.Response<GiftsResponse> response) {


                //  Log.d("TAG", response.body().getResponse() + " response get poi ");
                Log.e("TAG", "products " + response + "");
                if (response.isSuccessful() && response.body() != null && response.body().getOfferList() != null) {
                    if (!response.body().getOfferList().isEmpty()) {
                        for (int i = 0; i < response.body().getOfferList().size(); i++) {

                            Product product = response.body().getOfferList().get(i);
                            Log.e("TAG", product.getName().toString());
                            Log.e("TAG", "the product code is " + product.getCode().toString());
                            mProdcutsList.add(product);

                        }
                        //hideProgressBar();
                        progressDoalog.dismissWithAnimation();

                        VoucherItemClickListener voucherItemClickListener = new VoucherItemClickListener() {
                            @Override
                            public void onItemClicked(final String code) {
                                progressDoalog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
                                progressDoalog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
                                progressDoalog.setTitleText("Chargement");
                                progressDoalog.setCancelable(true);
                                progressDoalog.show();

                                progressDoalog.setTitleText("Confirmation d'achat")
                                        .setContentText("Voulez vous acheter ce produit ? ")
                                        .setConfirmText("OK").setCancelText("Annuler")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {

                                                HashMap<String, String> codeArrayList = new HashMap<String, String>();
                                                Code codeObject = new Code();
                                                codeObject.setCode(code);
                                                codeObject.setNumber(1);
                                                codeArrayList.put("code", codeObject.getCode());
                                                codeArrayList.put("nb", "1");


                                                ArrayList<Map> arrayMap = new ArrayList<>();
                                                arrayMap.add(codeArrayList);

                                                useVoucher(arrayMap);

                                            }
                                        })
                                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                progressDoalog.dismissWithAnimation();

                                            }
                                        }).changeAlertType(SweetAlertDialog.NORMAL_TYPE);


                            }
                        };

                        mProductsRecycler.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
                        mProductsRecycler.setAdapter(new ProductsAdapter(mProdcutsList, getActivity(), voucherItemClickListener));
                        mProductsRecycler.setVisibility(View.VISIBLE);

                    } else {
                        hideProgressBar();
                        Toast.makeText(getActivity(), getString(R.string.error_empty_list), Toast.LENGTH_SHORT).show();
                    }

                } else {

                    Toast.makeText(getActivity(), getString(R.string.error_server), Toast.LENGTH_SHORT).show();
                    hideProgressBar();
                }

            }

            @Override
            public void onFailure(Call<GiftsResponse> call, Throwable t) {
                //hideProgressBar();
                progressDoalog.setTitleText("Erreur!")
                        .setContentText("Une erreur est survenue, veuillez vérifier votre connexion Internet ou essayer plus tard.")
                        .setConfirmText("OK")
                        .changeAlertType(SweetAlertDialog.ERROR_TYPE);
                Toast.makeText(getActivity(), getString(R.string.error_server), Toast.LENGTH_SHORT).show();
                call.cancel();
            }
        });
    }

    public void useVoucher(final ArrayList<Map> codeArray) {
        mApiInerface = APIClient.getClient().create(APIInterface.class);


        showProgressBar();

        /**
         GET List Resources
         **/

        if (codeArray != null) {

            // prepare call in Retrofit 2.0

            try {
                JsonObject paramObject = new JsonObject();
                paramObject.addProperty("id_client", getPreferences().getUser().getId_client());

                JsonObject dataObject = new JsonObject();
                dataObject.addProperty("code", (codeArray.get(0).get("code").toString()));
                dataObject.addProperty("nb", "1");


                ArrayList<JsonObject> arrayMap = new ArrayList<>();
                arrayMap.add(dataObject);

                Gson gson = new Gson();
                JsonElement jsonElement = gson.toJsonTree(arrayMap);
                paramObject.add("data", jsonElement);


                Call<UseVoucherResponse> call = mApiInerface.useVoucher(getPreferences().getToken(), paramObject);
                Log.e("TAG", "the params body is " + paramObject.toString());
                call.enqueue(new Callback<UseVoucherResponse>() {
                    @Override
                    public void onResponse(Call<UseVoucherResponse> call, retrofit2.Response<UseVoucherResponse> response) {


                        //  Log.d("TAG", response.body().getResponse() + " response get poi ");
                        Log.d("TAG", response + "");
                        if (response.isSuccessful() && response.body() != null && response.body().getResponse() != null) {
                            if (response.body().getCode().equals("200")) {
                                hideProgressBar();
                                progressDoalog.setTitleText("")
                                        .setContentText("Votre Voucher est crée avec succés.")
                                        .setConfirmText("OK")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                progressDoalog.dismissWithAnimation();

                                            }
                                        })
                                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                progressDoalog.dismissWithAnimation();

                                            }
                                        })
                                        .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);

                            } else if (response.body().getCode().equals("400")) {
                                hideProgressBar();
                                if (response.body().getMessage().getMessage().toString() != null) {
                                    progressDoalog.setTitleText("Erreur!")
                                            .setContentText(response.body().getMessage().getMessage().toString())
                                            .setConfirmText("OK")
                                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                @Override
                                                public void onClick(SweetAlertDialog sDialog) {
                                                    progressDoalog.dismissWithAnimation();

                                                }
                                            })
                                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                @Override
                                                public void onClick(SweetAlertDialog sDialog) {
                                                    progressDoalog.dismissWithAnimation();

                                                }
                                            })
                                            .changeAlertType(SweetAlertDialog.ERROR_TYPE);
                                }

                                //   Toast.makeText(getActivity(), getString(R.string.error_empty), Toast.LENGTH_SHORT).show();
                            }

                        } else {

                            Toast.makeText(getActivity(), getString(R.string.error_server), Toast.LENGTH_SHORT).show();
                            progressDoalog.setTitleText("Erreur!")
                                    .setContentText("Un problème est survenu, veuillez essayer plus tard!")
                                    .setConfirmText("OK")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            progressDoalog.dismissWithAnimation();

                                        }
                                    })
                                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            progressDoalog.dismissWithAnimation();

                                        }
                                    })
                                    .changeAlertType(SweetAlertDialog.ERROR_TYPE);
                            hideProgressBar();
                        }

                    }

                    @Override
                    public void onFailure(Call<UseVoucherResponse> call, Throwable t) {
                        hideProgressBar();
                        Toast.makeText(getActivity(), getString(R.string.error_server), Toast.LENGTH_SHORT).show();
                        progressDoalog.setTitleText("Erreur!")
                                .setContentText("Un problème est survenu, veuillez essayer plus tard!")
                                .setConfirmText("OK")
                                .changeAlertType(SweetAlertDialog.ERROR_TYPE);
                        hideProgressBar();
                        call.cancel();
                    }
                });

            } catch (JsonIOException e) {
                e.printStackTrace();
            }

        }
    }

    public void getPromotions() {

        progressDoalog = new SweetAlertDialog(this.getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        progressDoalog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressDoalog.setTitleText("Chargement");
        progressDoalog.setCancelable(true);
        progressDoalog.show();
        RequestQueue queue = Volley.newRequestQueue(this.getActivity());


        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.promotionsUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("PROMOTIONS", "Response is " + response);
                Log.i("VOLLEY", response);

                parseJSONResponse(response);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDoalog.setTitleText("Erreur!")
                        .setContentText("Une erreur est survenue, veuillez vérifier votre connexion Internet ou essayer plus tard.")
                        .setConfirmText("OK")
                        .changeAlertType(SweetAlertDialog.ERROR_TYPE);
                Log.e("VOLLEYY", error.toString());
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("X-Auth-Token", getPreferences().getToken());

                return params;
            }


        };

        queue.add(stringRequest);
    }

    public String parseJSONResponse(String jsonResponse) {

        try {

            final JSONObject json = new JSONObject(jsonResponse);

            String code = json.getString("code");
            Log.e("Scan json", String.valueOf(code));


            if (code.equals("200")) {

                JSONObject response = json.getJSONObject("response");
                JSONArray cadeaux = response.getJSONArray("cadeaux");
                JSONArray produits = response.getJSONArray("produits");

                for (int i = 0; i < produits.length(); i++) {
                    JSONObject pro = (JSONObject) produits.get(i);
                    Produit produit = new Produit();
                    produit.setNom_produit(pro.getString("name"));
                    produit.setDesignation(pro.getString("description"));
                    produit.setPrixTND(pro.getString("price"));
                    produit.setPrixPTS("");
                    addViews(produit);

                }

               /* for (int j=0; j<cadeaux.length(); j++) {
                    JSONObject cdo = (JSONObject) cadeaux.get(j);

                    Produit cadeau = new Produit();
                    cadeau.setNom_produit(cdo.getString("name"));
                    cadeau.setDesignation(cdo.getString("description"));
                    cadeau.setPrixTND("");
                    cadeau.setPrixPTS(cdo.getString("pdf"));
                    addViews(cadeau);

                }*/
                progressDoalog.dismiss();


            } else {
                progressDoalog.setTitleText("Erreur!")
                        .setContentText("Veuillez vérifier votre login/mot de passe")
                        .setConfirmText("OK")
                        .changeAlertType(SweetAlertDialog.ERROR_TYPE);
                String msgErr = json.getString("message");
                Log.e("msgErr ", msgErr);

            }
            return code;

        } catch (JSONException ex) {

            ex.printStackTrace();
            return "250";
        }
    }


    private void addViews(final Produit produit) {
        try {

            Log.e("cadeaux ", "cdo " + produit.getPrixPTS());
            View v = LayoutInflater.from(getActivity()).inflate(R.layout.promo, null);
            Log.e("cadeaux ", "cdo " + produit.getNom_produit());
            if (produit.getPrixPTS().equals("")) {
                Log.e("Promo ", "produits");
                _linearL.addView(v);
                _linearL.requestLayout();
            } else {
                Log.e("Promo ", "cadeaux");
                //_linearLC.addView(v);
                //_linearLC.requestLayout();
            }


            CustomTextView product_name = v.findViewById(R.id.product_name);
            product_name.setText(produit.getNom_produit());


            CustomTextView product_desc = v.findViewById(R.id.product_desc);
            product_desc.setText(produit.getDesignation());

            CustomTextView product_price = v.findViewById(R.id.product_price);
            if (produit.getPrixPTS().equals("")) {
                product_price.setText(produit.getPrixTND() + " DT");
            } else {
                product_price.setText(produit.getPrixPTS() + " points");
            }


        } catch (NullPointerException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onItemClicked(String code) {

    }
}
