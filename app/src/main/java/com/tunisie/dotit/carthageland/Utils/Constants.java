package com.tunisie.dotit.carthageland.Utils;

/**
 * Created by eniso on 17/10/2017.
 */

public class Constants {

    //private static String baseUrl  = "http://192.168.1.126:80/CL/web/app.php/api/";
    //public static String baseUrl = "http://clubcarthageland.com.tn/app_dev.php/api/";
    //public static String baseUrl = "http://carthageland.dotit-corp.com/app_dev.php/api/";
    public static String baseUrl = "http://clubcarthageland.com.tn/api/";
    public static String baseImageUrl = "http://clubcarthageland.com.tn/";
    public static String poiImmagesUrl = "http://clubcarthageland.com.tn/uploads/img/poi/";
    public static String parcsImagesUrl = "http://clubcarthageland.com.tn/uploads/img/parcs/";
    public static String loginUrl = baseUrl + "login";
    public static String subscribeUrl = baseUrl + "register";
    public static String soldeWithEmailUrl = baseUrl + "soldeWithEmail";
    public static String promotionsUrl = baseUrl + "listeCadeauxOffres";
    public static String profilInfosUrl = baseUrl + "profilInformatins";
    public static String govUrl = baseUrl + "getGovernorates";
    public static String govUrlAlg = baseUrl + "algeriaGovernorates";
    public static String countriesUrl = baseUrl + "getAllCountries";
    public static String zipCodesUrl = baseUrl + "getZipCodeByGovernorate";
    public static String zipCodesUrlAlg = baseUrl + "algeriaZipCode";
    public static String sendMessageUrl = baseUrl + "envoiMessage";
    public static String subjectsUrl = baseUrl + "listeSubjects";
    public static String historiqueUrl = baseUrl + "historiqueAchat";
    public static String historiqueVoucherUrl = baseUrl + "historiqueVoucher";
    public static String historiqueVBoutiqueUrl = baseUrl + "historiqueVoucherBoutique";
    public static String pointsConfigUrl = baseUrl + "getPointsConfig";
    public static String listeParrainageUrl = baseUrl + "listeParrainage";
    public static String messagesUrl = baseUrl + "boiteMessagerie";
    public static String assignCarteUrl = baseUrl + "assigneCarte";
    public static String parrainerUrl = baseUrl + "parrainerquelquUn";
    public static String editProfilUrl = baseUrl + "updateClient";
    public static String convPtsUrl = baseUrl + "voucherPointsConvert";
    public static String clientUsersUrl = baseUrl + "getClientUsers";
    public static String delUsersUrl = baseUrl + "delClientUsers";
    public static String sendLinkUrl = baseUrl + "emailSendLink";
    public static String setClientUsers = baseUrl + "setClientUsers";
    public static String cvHammametXebsiteUrl = "https://hammamet.carthageland.com.tn/index.php?cmd=tickets&action=step1&frs=20";
    public static String cvTunisWebsiteUrl = "https://tunis.carthageland.com.tn/index.php?cmd=tickets&action=step1&frs=19";


    public class Parameters {
        public static final String AVAILABLE_WINNINGS_PARAM = "available_winnings";
        public static final String HAMMAMET_POIs = "hammamet_pois";
        public static final String TUNIS_POIs = "pois";
        public static final String HAMMAMET_Parcs = "hammamet_parcs";
        public static final String TUNIS_Parcs = "tunis_parcs";
        public static final String HAMMAMET_Restaurants = "hammamet_restaurants";
        public static final String TUNIS_Restaurants = "tunis_restaurants";
        public static final String LAST_WINNERS_PARAM = "last-winners";
        public static final String FIRST_NAME = "first_name";
        public static final String LAST_NAME = "last_name";
        public static final String EMAIL = "email";

        public static final String MULTIPART = "multipart/form-data";
        public static final String WEB_FIELD_PHOTO = "profile_image";

        public static final String DELETE_PHOTO = "delete_image";
        public static final String DETAILS_PARAM = "details";

        public static final String TITLE_PARAM = "title";
        public static final String SOURCE_PARAM = "source";
        public static final String ID_PARAM = "looId";

        public static final int LIVE_TAG = 0;
        public static final int REPLAY_TAG = 1;

        public static final String LOTTERY_RESPONSE = "lottery_response";

        public static final float HEADER_BING_RATION = 0.085f;

        // profile and user
        public static final int MIN_PASSWORD_SIZE = 6;
        public static final int MAX_PASSWORD_SIZE = 25;
        public static final int MAX_CODE_SIZE = 40;
        public static final int MAX_EMAIL_SIZE = 50;
        public static final String WHITE_SPACE = "^\\s*$";
        public static final String RULES_SELECTED = "1";
        public static final String RULES_NOT_SELECTED = "0";

        // select image dialog
        public static final int CAMERA_SELECTED = 0;
        public static final int GALLERY_SELECTED = 1;
        public static final int DELETE_SELECTED = 2;
        public static final int CANCEL_SELECTED = 3;
        public static final String CGU_FROM_SINGUP = "sing_up";
        public static final String CGU_FROM_MENU = "menu";

        public static final int MAX_VALUES_ADAPTER = 10000;
        public static final int DEFAULT_CREDIT_VALUE = 1;
        public static final int MAX_GRID_COUNT = 5;
        public static final int MAX_GRID_NUMBERS_COUNT = 25;
        public static final int MAX_NUMBERS = 45;
        public static final String DELETE_IMAGE_VALUE = "deleted";
        public static final String SUCCESS = "success";
        public static final int MAX_PROGRESS = 61;
        public static final int BARCODE_IMAGE_WIDTH = 600;
        public static final int BARCODE_IMAGE_HEIGHT = 400;
        public static final int MAX_LINE_COUNT = 5;

        public static final String DATE_FORMAT_NOW = "yyyy-MM-dd HH:mm:ss";
        public static final String CHOSEN_GRID = "chosen_grid";
        public static final int WINNER_INDEX = 1;
        public static final String END_POINT = "end_point";
        public static final String SELECTED_GIFT = "selected_gift";
        public static final String DRAW_STATE_TAG = "draw_state_tag";

        public static final String PUSH_DATA_type = "push_data_type";
        public static final String PUSH_DATA_MESSAGE = "push_data_message";
        public static final String CL_TUNIS = "CLTunis";
        public static final String CL_HAMMAMET = "CLHammamet";
        public static final String RESTAURANTS_PARAM = "RESTAURANT";
    }

    public class Categories {
        public static final String attractions_filter = "Attractions";
        public static final String toilets_filter = "WC";
        public static final String restaurants_filter = "Restaurants";
        public static final String kiosque_filter = "Guichets";
        public static final String counters_filter = "Kiosques";
        public static final String basics_filter = "6";

    }

    public class Sites {
        public static final String HHAMMAMET_PARAM = "Hammamet";
        public static final String TUNIS_PARAM = "Tunis";
    }
}
