package com.tunisie.dotit.carthageland.dagger2;

import javax.inject.Qualifier;

/**
 * Created by MacBook on 8/28/17.
 */

@Qualifier
public @interface ApplicationContext {
}
