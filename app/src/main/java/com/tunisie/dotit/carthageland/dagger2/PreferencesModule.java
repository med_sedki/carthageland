package com.tunisie.dotit.carthageland.dagger2;

import android.content.Context;


import com.tunisie.dotit.carthageland.global.SharedPreferences;

import dagger.Module;
import dagger.Provides;

/**
 * Created on 2/2/18.
 */

@Module(includes = ContextModule.class)
public class PreferencesModule {

    @Provides
    @ApplicationScope
    public SharedPreferences sharedPreferences(@ApplicationContext Context context) {
        return new SharedPreferences(context);
    }

}
