package com.tunisie.dotit.carthageland.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.tunisie.dotit.carthageland.R;
import com.tunisie.dotit.carthageland.application.BaseApplication;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends BaseFragment {

    View view;
    Switch switch_notif;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_settings, container, false);
        Toolbar toolbarTop = (Toolbar) getActivity().findViewById(R.id.toolbar);
        TextView mTitle = (TextView) toolbarTop.findViewById(R.id.toolbar_title);
        mTitle.setText("Paramètres");

        switch_notif = view.findViewById(R.id.switch_notif);
        switch_notif.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton cb, boolean on) {
                if (on) {
                    BaseApplication.getInstance().activatePush();
                    Log.e("SETTINGS", "Activate notifications");
                } else {
                    BaseApplication.getInstance().deactivatePush();
                    Log.e("SETTINGS", "Deactivate notifications");
                }
            }
        });

        return view;
    }

}
