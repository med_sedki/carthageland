package com.tunisie.dotit.carthageland.models;

/**
 * Created by eniso on 06/10/2017.
 */

public class Image {

    private String id_image;
    private Album album;
    private String urlImage;
    private String urlLocal;

    public Image(String id_image, Album album, String urlImage, String urlLocal) {
        this.id_image = id_image;
        this.album = album;
        this.urlImage = urlImage;
        this.urlLocal = urlLocal;
    }

    public String getId_image() {
        return id_image;
    }

    public void setId_image(String id_image) {
        this.id_image = id_image;
    }

    public Album getAlbum() {
        return album;
    }

    public void setAlbum(Album album) {
        this.album = album;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    public String getUrlLocal() {
        return urlLocal;
    }

    public void setUrlLocal(String urlLocal) {
        this.urlLocal = urlLocal;
    }
}
