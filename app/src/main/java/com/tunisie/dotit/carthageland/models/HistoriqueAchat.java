package com.tunisie.dotit.carthageland.models;

/**
 * Created by eniso on 06/10/2017.
 */

public class HistoriqueAchat extends Historique {

    private String NbPointsConv;

    /*public HistoriqueAchat(String id_historique, String date, String montant, String nbPointsConv) {
        super(id_historique, date, montant, nbPointsConv);
        NbPointsConv = nbPointsConv;
    }*/

    public String getNbPointsConv() {
        return NbPointsConv;
    }

    public void setNbPointsConv(String nbPointsConv) {
        NbPointsConv = nbPointsConv;
    }
}
