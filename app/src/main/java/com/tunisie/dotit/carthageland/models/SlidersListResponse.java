package com.tunisie.dotit.carthageland.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Sedki YOUZBECHI  on 22/03/2021.
 */
public class SlidersListResponse {

    //    @SerializedName("id")
//    private String id;
    @SerializedName("code")
    private String code;
    @SerializedName("filename")
    private String filename;
    @SerializedName("site")
    private String site;
    //    @SerializedName("enabled")
//    private Boolean enabled;
//    @SerializedName("file")
//    private String file;
    @SerializedName("webPath")
    private String webPath;


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getWebPath() {
        return webPath;
    }

    public void setWebPath(String webPath) {
        this.webPath = webPath;
    }
}
