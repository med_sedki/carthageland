package com.tunisie.dotit.carthageland.adapters.holder;

import android.app.Application;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tunisie.dotit.carthageland.BaseApplication;
import com.tunisie.dotit.carthageland.R;
import com.tunisie.dotit.carthageland.Utils.Constants;
import com.tunisie.dotit.carthageland.adapters.ParcsAdapter;
import com.tunisie.dotit.carthageland.custom.CustomTextView;
import com.tunisie.dotit.carthageland.global.ParcItemClickListener;
import com.tunisie.dotit.carthageland.models.Parc;
import com.squareup.picasso.Picasso;


/**
 * Created on 2/15/18.
 */

public class ParcsHolder extends BaseHolder {

    TextView mTitle, mDetails;
    public ImageView mImageView;
    LinearLayout mMainLayout ;

    public ParcsHolder(View itemView) {
        super(itemView);
        mTitle = itemView.findViewById(R.id.tv_restaurant_title);
        mImageView = itemView.findViewById(R.id.iv_restaurant);
        mDetails = itemView.findViewById(R.id.tv_restaurant_details);
        mMainLayout = itemView.findViewById(R.id.main_parc_layout);

    }

    public void bind(final Parc menuItem,final ParcItemClickListener parcItemClickListener) {
        mTitle.setText(menuItem.getName());
        mDetails.setText(menuItem.getDescription());
        Log.e("TAG","the image url is "+Constants.parcsImagesUrl + menuItem.getFilename());
        if (menuItem.getFilename() != null) {



            Picasso.with(com.tunisie.dotit.carthageland.application.BaseApplication.getInstance()).load(Constants.parcsImagesUrl + menuItem.getFilename()).placeholder(R.drawable.placeholder).into(mImageView);
        }
        mMainLayout.setOnClickListener(getItemClickListener(menuItem, parcItemClickListener));
    }


    @NonNull
    private View.OnClickListener getItemClickListener(final Parc menuItem, final ParcItemClickListener mAccountMenuClickListener) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAccountMenuClickListener.onItemClicked(menuItem);
            }
        };
    }

}
