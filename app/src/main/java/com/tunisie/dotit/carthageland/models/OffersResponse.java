package com.tunisie.dotit.carthageland.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OffersResponse {

    @SerializedName("cadeaux")
    private List<Gift> cadeaux ;
    @SerializedName("produits")
    private List<Product> produits ;

    public List<Gift> getCadeaux() {
        return cadeaux;
    }

    public void setCadeaux(List<Gift> cadeaux) {
        this.cadeaux = cadeaux;
    }

    public List<Product> getProduits() {
        return produits;
    }

    public void setProduits(List<Product> produits) {
        this.produits = produits;
    }
}
