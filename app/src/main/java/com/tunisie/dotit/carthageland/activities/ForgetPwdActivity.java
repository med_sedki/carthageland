package com.tunisie.dotit.carthageland.activities;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.iid.FirebaseInstanceId;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.tunisie.dotit.carthageland.R;
import com.tunisie.dotit.carthageland.Utils.Constants;
import com.tunisie.dotit.carthageland.custom.CustomButton;
import com.tunisie.dotit.carthageland.custom.CustomEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import static com.tunisie.dotit.carthageland.global.AppUtils.isValidEmail;

public class ForgetPwdActivity extends BaseActivity implements View.OnClickListener {

    private ImageView mReturnBtn;
    private CustomButton mForgotPassword;
    private CustomEditText mEmailEd;
    private SweetAlertDialog mProgressDialog;
    private String mTokenDevice;
    public static Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_pwd);
        mTokenDevice = FirebaseInstanceId.getInstance().getToken();
        Log.e("TAG", "token push " + mTokenDevice);

        mForgotPassword = findViewById(R.id.btn_reset);
        mEmailEd = findViewById(R.id.txt_email);
        mForgotPassword.setOnClickListener(this);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Toolbar toolbarTop = (Toolbar) findViewById(R.id.toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationIcon(R.drawable.back_arrow_ic);
        View bottomSheet = findViewById(R.id.bottom_sheet);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                return false;
            }
        });
    }

    private boolean isValidInput() {
        if (TextUtils.isEmpty(mEmailEd.getText().toString())) {
            final SweetAlertDialog errDialog = new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE);
            errDialog.setTitleText(getResources().getString(R.string.error_title));
            errDialog.setContentText(getResources().getString(R.string.error_empty));
            errDialog.show();
        } else if (!isValidEmail(mEmailEd.getText().toString())) {
            final SweetAlertDialog errDialog = new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE);
            errDialog.setTitleText(getResources().getString(R.string.error_title));
            errDialog.setContentText(getResources().getString(R.string.invalid_email_format));
            errDialog.show();
        } else if (mEmailEd.getText().toString().length() > Constants.Parameters.MAX_PASSWORD_SIZE) {
            final SweetAlertDialog errDialog = new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE);
            errDialog.setTitleText(getResources().getString(R.string.error_title));
            errDialog.setContentText(getResources().getString(R.string.error_mail_length_max));
            errDialog.show();
        } else {
            return true;
        }
        return false;
    }

    private void resetPassword() {
        if (isValidInput()) {
            if (isNetworkAvailable()) {
                setLoadingDialog();
                // Instantiate the RequestQueue.
                try {
                    RequestQueue queue = Volley.newRequestQueue(this);
                    JSONObject jsonBody = new JSONObject();
                    jsonBody.put("mail", mEmailEd.getText().toString());
                    final String requestBody = jsonBody.toString();

                    Log.e("TAG", "Constants.sendLinkUrl " + Constants.sendLinkUrl);

                    StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.sendLinkUrl, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.e("TAG", "Response is " + response);
                            Log.i("V OLLEY", response);

                            parseJSONResponse(response);
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            mProgressDialog.setTitleText(getResources().getString(R.string.error_title))
                                    .setContentText(getResources().getString(R.string.fb_connect_canceled))
                                    .setConfirmText(getResources().getString(R.string.ok))
                                    .changeAlertType(SweetAlertDialog.ERROR_TYPE);
                            Log.e("VOLLEYY", error.toString());
                        }
                    }) {
                        @Override
                        public String getBodyContentType() {
                            return "application/json; charset=utf-8";
                        }

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                Log.e("requestBody ", requestBody);
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };
                    queue.add(stringRequest);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                showErrorNetworkDialog();
            }
        }
    }

    public String parseJSONResponse(String jsonResponse) {
        try {
            final JSONObject json = new JSONObject(jsonResponse);
            String code = json.getString("code");
            Log.e("Scan json", String.valueOf(code));

            if (code.equals("200")) {
                mProgressDialog.setTitleText(getResources().getString(R.string.success_title))
                        .setContentText(getResources().getString(R.string.success_title))
                        .setConfirmText(getResources().getString(R.string.ok))
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                mProgressDialog.dismissWithAnimation();
                                //Intentx myIntent = new Intent(RegisterActivity.this, HomeActivity.class);
                                //startActivity(myIntent);
                                finish();
                            }
                        })
                        .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);

                JSONObject response = json.getJSONObject("response");
                JSONObject message = response.getJSONObject("message");
            } else if (code.equals("400")) {
                mProgressDialog.setTitleText(getResources().getString(R.string.error_title))
                        .setContentText(getResources().getString(R.string.error_not_found))
                        .setConfirmText(getResources().getString(R.string.ok))
                        .changeAlertType(SweetAlertDialog.ERROR_TYPE);
                String msgErr = json.getString("message");
                Log.e("msgErr ", msgErr);
            } else {
                mProgressDialog.setTitleText(getResources().getString(R.string.error_title))
                        .setContentText(getResources().getString(R.string.fb_connect_canceled))
                        .setConfirmText(getResources().getString(R.string.ok))
                        .changeAlertType(SweetAlertDialog.ERROR_TYPE);
                String msgErr = json.getString("message");
                Log.e("msgErr ", msgErr);
            }
            return code;
        } catch (JSONException ex) {
            ex.printStackTrace();
            return "250";
        }
    }

    private void setLoadingDialog() {
        mProgressDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        mProgressDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        mProgressDialog.setTitleText(getResources().getString(R.string.loading));
        mProgressDialog.setCancelable(true);
        mProgressDialog.show();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onClick(View v) {
        if (v == mForgotPassword) {
            resetPassword();
        }
    }
}
