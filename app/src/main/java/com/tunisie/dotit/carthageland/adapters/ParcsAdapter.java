package com.tunisie.dotit.carthageland.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tunisie.dotit.carthageland.R;
import com.tunisie.dotit.carthageland.adapters.holder.ParcsHolder;
import com.tunisie.dotit.carthageland.global.ParcItemClickListener;
import com.tunisie.dotit.carthageland.global.VoucherItemClickListener;
import com.tunisie.dotit.carthageland.models.Parc;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 2/15/18.
 */

public class ParcsAdapter extends RecyclerView.Adapter<ParcsHolder> {

    private List<Parc> mParcs;
    public Context mContext ;
    private ParcItemClickListener mParcItemListener ;

    public ParcsAdapter(ArrayList<Parc> parcList, String location,Context ctx,ParcItemClickListener parcItemClickListener) {
       /* if (location.toString().equals(Constants.Parameters.CL_TUNIS)) {
            mParcs = AppUtils.getTunisParcs();
        } else if (location.toString().equals(Constants.Parameters.CL_HAMMAMET)) {
            mParcs = AppUtils.getHammametParcs();
        }*/
       mParcs= parcList ;
       mParcItemListener = parcItemClickListener ;

        this.mContext=ctx ;
        //mParcs = AppUtils.getMenuItems();
    }

    @Override
    public ParcsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_parcs, parent, false);
        return new ParcsHolder(view);
    }


    @Override
    public void onBindViewHolder(ParcsHolder holder, int position) {
        Parc parc = mParcs.get(position);
        holder.bind(parc,mParcItemListener);
    }

    @Override
    public int getItemCount() {
        return mParcs.size();
    }
}