package com.tunisie.dotit.carthageland.models;

/**
 * Created by eniso on 06/10/2017.
 */

public class Produit {

    private String id_produit;
    private String nom_produit;
    private String designation;
    private String prixTND;
    private String prixPTS;
    private CategorieProduit categorie;

    public Produit() {
    }

    public Produit(String id_produit, String nom_produit, String designation, String prixTND, String prixPTS, CategorieProduit categorie) {
        this.id_produit = id_produit;
        this.nom_produit = nom_produit;
        this.designation = designation;
        this.prixTND = prixTND;
        this.prixPTS = prixPTS;
        this.categorie = categorie;
    }

    public String getId_produit() {


        return id_produit;
    }

    public void setId_produit(String id_produit) {
        this.id_produit = id_produit;
    }

    public String getNom_produit() {
        return nom_produit;
    }

    public void setNom_produit(String nom_produit) {
        this.nom_produit = nom_produit;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getPrixTND() {
        return prixTND;
    }

    public void setPrixTND(String prixTND) {
        this.prixTND = prixTND;
    }

    public String getPrixPTS() {
        return prixPTS;
    }

    public void setPrixPTS(String prixPTS) {
        this.prixPTS = prixPTS;
    }

    public CategorieProduit getId_categorie() {
        return categorie;
    }

    public void setId_categorie(CategorieProduit categorie) {
        this.categorie = categorie;
    }
}
