package com.tunisie.dotit.carthageland.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tunisie.dotit.carthageland.R;
import com.tunisie.dotit.carthageland.adapters.holder.RestaurantHolder;
import com.tunisie.dotit.carthageland.models.POI;

import java.util.ArrayList;

/**
 * Created on 2/15/18.
 */

public class RestaurantsAdapter extends RecyclerView.Adapter<RestaurantHolder> {

    private final Context mContext;
    private ArrayList<POI> mRestaurants;

    public RestaurantsAdapter(ArrayList<POI> poiList ,String location,Context ctx ) {
        /*if (location.toString().equals(Constants.Parameters.CL_TUNIS)) {
            mRestaurants = AppUtils.getTunisRestaurants();
        } else if (location.toString().equals(Constants.Parameters.CL_HAMMAMET)) {
            mRestaurants = AppUtils.getHammametRestaurants() ;
        }*/
        mRestaurants = poiList ;
        this.mContext = ctx ;
    }

    @Override
    public RestaurantHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_restaurant, parent, false);
        return new RestaurantHolder(view);
    }


    @Override
    public void onBindViewHolder(RestaurantHolder holder, int position) {
        POI restaurant = mRestaurants.get(position);
        holder.bind(restaurant);
    }

    @Override
    public int getItemCount() {
        return mRestaurants.size();
    }
}