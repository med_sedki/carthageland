package com.tunisie.dotit.carthageland.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by eniso on 06/10/2017.
 */

public class CategoriePOI {


    @SerializedName("code")
    private String code;
    @SerializedName("name")
    private String name;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
