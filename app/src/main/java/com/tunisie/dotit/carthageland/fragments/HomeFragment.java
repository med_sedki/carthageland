package com.tunisie.dotit.carthageland.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tunisie.dotit.carthageland.R;
import com.tunisie.dotit.carthageland.activities.HomeActivity;
import com.tunisie.dotit.carthageland.application.BaseApplication;
import com.tunisie.dotit.carthageland.custom.CustomViewPager;

public class HomeFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match

    TabLayout tabLayout;
    CustomViewPager viewPager;

    int int_items = 2;
    View view;
    BaseApplication myApp;
    private ViewPager mViewPager;
    private TabLayout mTabsLayout;
    private int[] tabIcons = {
            R.drawable.history_ic,
            R.drawable.ic_gift
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        Log.i("Left", "onCreate()");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_home, container, false);
        tabLayout = (TabLayout) view.findViewById(R.id.tabs);

        if (getPreferences().getConnectedMode().equals("true")) {
            HomeActivity.toolbar.setNavigationIcon(R.drawable.drawer_menu_ic);
        } else {
            HomeActivity.toolbar.setNavigationIcon(null);
        }

        int[] colors = {Color.rgb(255, 255, 255), Color.rgb(255, 242, 0)};

        // getActivity().onBackPressed();

        viewPager = (CustomViewPager) view.findViewById(R.id.viewpager);
        viewPager.setPagingEnabled(false);

        if (getPreferences().getConnectedMode().equals("true")) {
            HomeActivity.toolbar.setNavigationIcon(R.drawable.drawer_menu_ic);
        }
        HomeActivity.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeActivity.openDrawer();
            }
        });

        /**
         *Set an Apater for the View Pager
         */
        viewPager.setAdapter(new MyAdapter(getChildFragmentManager()));

        myApp = ((BaseApplication) getActivity().getApplication());

        /**
         * Now , this is a workaround ,
         * The setupWithViewPager dose't works without the runnable .
         * Maybe a Support Library Bug .
         */

        tabLayout.post(new Runnable() {
            @Override
            public void run() {
                if (isAdded()) {
                    tabLayout.setupWithViewPager(viewPager);
                    //setupTabIcons();
                }
            }
        });

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tabLayout.getSelectedTabPosition() == 0) {
                    getPreferences().setCarthageLandLocation("CLHammamet");
                    Log.e("Plan ", getPreferences().getCarthageLandLocation());
                } else if (tabLayout.getSelectedTabPosition() == 1) {
                    getPreferences().setCarthageLandLocation("CLTunis");
                    Log.e("Plan ", getPreferences().getCarthageLandLocation());
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        Toolbar toolbarTop = (Toolbar) getActivity().findViewById(R.id.toolbar);
        TextView mTitle = (TextView) toolbarTop.findViewById(R.id.toolbar_title);
        mTitle.setText("Carthage Land");

        return view;
    }

    private void setupTabIcons() {
        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
    }

    class MyAdapter extends FragmentPagerAdapter {

        public MyAdapter(FragmentManager fm) {
            super(fm);
        }

        /**
         * Return fragment with respect to Position .
         */

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0: {
                    return new ParcChoiceHmammametFragment();
                }

                case 1: {
                    return new ParcChoiceTunisFragment();
                }
                /*  case 2 : return new TabFragment3();*/
            }
            return null;
        }

        @Override
        public int getCount() {
            return int_items;
        }

        /**
         * This method returns the title of the tab according to the position.
         */

        @Override
        public CharSequence getPageTitle(int position) {

            switch (position) {
                case 0:
                    return "Carthage Land Hammamet";
                case 1:
                    return "Carthage Land Tunis";
               /* case 2 :
                    return getResources().getString(R.string.Tab3_inquire);*/
            }
            return null;
        }
    }

    private Fragment navigateToParcsChoiceHammammetFragment() {
        Fragment fragment = null;
        try {
            fragment = new ParcChoiceHmammametFragment();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return fragment;
    }

    private Fragment navigateToParcsChoiceTunisFragment() {
        Fragment fragment = null;
        try {
            fragment = new ParcChoiceTunisFragment();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return fragment;
    }

    private void closeFragment() {
        getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
    }
}