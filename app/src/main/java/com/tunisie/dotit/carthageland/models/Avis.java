package com.tunisie.dotit.carthageland.models;

/**
 * Created by eniso on 06/10/2017.
 */

public class Avis {

    private String id_avis;
    private String contenu;
    private String nbEtoiles;
    private Client client;
    private String date;

    public Avis(String id_avis, String contenu, String nbEtoiles, Client client, String date) {
        this.id_avis = id_avis;
        this.contenu = contenu;
        this.nbEtoiles = nbEtoiles;
        this.client = client;
        this.date = date;
    }

    public String getId_avis() {
        return id_avis;
    }

    public void setId_avis(String id_avis) {
        this.id_avis = id_avis;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public String getNbEtoiles() {
        return nbEtoiles;
    }

    public void setNbEtoiles(String nbEtoiles) {
        this.nbEtoiles = nbEtoiles;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
