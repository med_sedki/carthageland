package com.tunisie.dotit.carthageland.application;

import android.app.Application;
import android.util.Log;

import com.facebook.FacebookSdk;
import com.tunisie.dotit.carthageland.dagger2.ApplicationComponent;
import com.tunisie.dotit.carthageland.dagger2.ContextModule;
import com.tunisie.dotit.carthageland.dagger2.DaggerApplicationComponent;
import com.tunisie.dotit.carthageland.global.SharedPreferences;
import com.tunisie.dotit.carthageland.models.Carte;
import com.tunisie.dotit.carthageland.models.Client;

/**
 * Created by eniso on 06/10/2017.
 */

public class BaseApplication extends Application {


    private String carthage_location;
    private String email;
    private String token;
    private String id_user;
    private Client client;
    private Carte carte;
    private static BaseApplication mInstance;
    private ApplicationComponent mComponent;
    public static boolean mIsPushActivated = true ;

    @Override
    public void onCreate() {
        super.onCreate();

        mInstance = this;
        mComponent = DaggerApplicationComponent.builder().contextModule(new ContextModule(mInstance)).build();
        FacebookSdk.sdkInitialize(getApplicationContext());
        Log.e("BaseApplication", "onCreate");


    }

    public static BaseApplication getInstance() {
        return mInstance;
    }

    public String getCarthage_location() {
        return carthage_location;
    }

    public void setCarthage_location(String carthage_location) {
        this.carthage_location = carthage_location;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Carte getCarte() {
        return carte;
    }

    public void setCarte(Carte carte) {
        this.carte = carte;
    }

    public void clearUserCache() {
        getPreferences().clearSharedPreferences();
    }

    public ApplicationComponent getApplicationComponent() {
        return mComponent;
    }

    public SharedPreferences getPreferences() {
        return mComponent.getPreferences();
    }

    public boolean activatePush() {
        mIsPushActivated = true;
        return mIsPushActivated;
    }

    public boolean deactivatePush() {
        mIsPushActivated = false;
        return mIsPushActivated;
    }

}
