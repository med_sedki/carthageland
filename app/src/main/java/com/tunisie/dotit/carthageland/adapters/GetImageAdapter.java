package com.tunisie.dotit.carthageland.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.tunisie.dotit.carthageland.R;
import com.tunisie.dotit.carthageland.application.BaseApplication;

import java.util.ArrayList;

/**
 * Created by Sedki YOUZBECHI  on 09/03/2021.
 */
public class GetImageAdapter extends PagerAdapter {

    Context mContext;
    ArrayList<String> mArraylist;
    LayoutInflater layoutInflater;

    public GetImageAdapter(Context context, ArrayList<String> arrayList) {
        this.mContext = context;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mArraylist = arrayList;
        Log.e("mArraylist", mArraylist.toString());
    }


    @Override
    public boolean isViewFromObject(View v, Object obj) {
        return v == ((ImageView) obj);
    }

    @Override
    public int getCount() {
        if (mArraylist != null) {
            return mArraylist.size();
        }
        return 0;
    }


    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = layoutInflater.inflate(R.layout.viewpager_item, container, false);
        ImageView mImageView = itemView.findViewById(R.id.viewPagerItem_image1);
        mImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        Picasso.with(BaseApplication.getInstance()).load(mArraylist.get(position))
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder).into(mImageView);

        if (mImageView.getParent() != null)
            ((ViewGroup) mImageView.getParent()).removeView(mImageView); // <- fix
        container.addView(mImageView, 0);
        return mImageView;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((ImageView) object);
    }


//    public int getItemId(int position) {
//        return position;
//    }
//
//    @Override
//    public void notifyDataSetChanged() {
//        long[] newItemIds = new long[getCount()];
//        for (int i = 0; i < newItemIds.length; i++) {
//            newItemIds[i] = getItemId(i);
//        }
//
//        super.notifyDataSetChanged();
//    }
}
