package com.tunisie.dotit.carthageland.models;

/**
 * Created by eniso on 06/10/2017.
 */

public class CategorieProduit {
    private String id_categorie;
    private String name;

    public CategorieProduit() {
    }

    public CategorieProduit(String id_categorie, String name) {
        this.id_categorie = id_categorie;
        this.name = name;
    }

    public String getId_categorie() {
        return id_categorie;
    }

    public void setId_categorie(String id_categorie) {
        this.id_categorie = id_categorie;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
