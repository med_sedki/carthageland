package com.tunisie.dotit.carthageland.models;

/**
 * Created by eniso on 06/10/2017.
 */

public class Message {

    private String id_message;
    private String objet;
    private String content;
    private String title;
    private String date;

    public Message() {
    }

    public Message(String id_message, String objet, String content, String title, String date) {
        this.id_message = id_message;
        this.objet = objet;
        this.content = content;
        this.title = title;
        this.date = date;
    }

    public String getId_message() {
        return id_message;
    }

    public void setId_message(String id_message) {
        this.id_message = id_message;
    }

    public String getObjet() {
        return objet;
    }

    public void setObjet(String objet) {
        this.objet = objet;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
