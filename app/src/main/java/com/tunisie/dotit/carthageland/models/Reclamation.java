package com.tunisie.dotit.carthageland.models;

/**
 * Created by eniso on 06/10/2017.
 */

public class Reclamation {

    private String id_reclamation;
    private String type;
    private String email;
    private String message;


    public Reclamation(String id_reclamation, String type, String email, String message) {
        this.id_reclamation = id_reclamation;
        this.type = type;
        this.email = email;
        this.message = message;

    }

    public String getId_reclamation() {
        return id_reclamation;
    }

    public void setId_reclamation(String id_reclamation) {
        this.id_reclamation = id_reclamation;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


}
