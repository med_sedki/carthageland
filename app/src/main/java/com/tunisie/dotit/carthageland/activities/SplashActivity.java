package com.tunisie.dotit.carthageland.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.tunisie.dotit.carthageland.R;
import com.tunisie.dotit.carthageland.Utils.Constants;
import com.tunisie.dotit.carthageland.global.AppUtils;
import com.tunisie.dotit.carthageland.models.Carte;
import com.tunisie.dotit.carthageland.models.Client;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;


public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        if (getPreferences().getUserId() != null) {
            getUserInfo();

        } else {
            Intent i = new Intent(SplashActivity.this, HomeActivity.class) ;
            i.putExtra("iSconnected", "false");
            startActivity(i);
            finish();
        }

        // close splash activity

    }

    public String parseJSONResponse(String jsonResponse, String url) {

        try {

            final JSONObject json = new JSONObject(jsonResponse);

            String code = json.getString("code");
            if (code.equals("200")) {


                JSONObject response = json.getJSONObject("response");
                Log.e("Response", response.toString());
                //JSONObject user = response.getJSONObject("user");

                JSONObject client = response.getJSONObject("client");
                final String firstname = response.getString("firstname");
                Log.e("TOKEN ", "***firstname*** " + firstname);
                String birthdate;
                if ((client.has("birthday")) && !client.isNull("birthday")) {
                    birthdate = client.getString("birthday").substring(0, 10);
                    Log.e("TOKEN ", "***birthday*** " + birthdate);
                } else {
                    birthdate = "";
                }
                final String lasttname = response.getString("lastname");
                Log.e("TOKEN ", "***lasttname*** " + lasttname);
                String id_user = response.getString("id");
                Log.e("TOKEN ", "***id_user*** " + id_user);
                final String email = response.getString("mail");
                Log.e("TOKEN ", "***email*** " + email);
                final String phone = response.getString("phone");
                Log.e("TOKEN ", "***phone*** " + phone);
                final String address = client.getString("address");
                Log.e("TOKEN ", "***address*** " + address);
                final String gender = response.getString("gender");
                Log.e("TOKEN ", "***gender*** " + gender);


                // }
                String points = client.getString("points");
                Log.e("TOKEN ", "***points*** " + points);
                String clientType = client.getJSONObject("clientType").getString("type");
                Log.e("TOKEN ", "***clientType*** " + clientType);
                Carte crt = null;

                try {
                    if ((client.has("carte")) && (!client.isNull("carte"))) {
                        JSONObject carte = client.getJSONObject("carte");
                        Log.e("TOKEN ", "***carte*** " + carte);
                        String num_carte = carte.getString("code");
                        Log.e("TOKEN ", "***num_carte*** " + num_carte);
                        String type_carte = carte.getJSONObject("type").getString("name");
                        Log.e("TOKEN ", "***type_carte*** " + type_carte);
                        crt = new Carte(num_carte, points, type_carte, id_user);
                        getPreferences().setCard(crt);

                        Intent i = new Intent(SplashActivity.this, HomeActivity.class) ;
                        i.putExtra("iSconnected", "true");
                        startActivity(i);
                        finish();
                    } else {
                        Intent i = new Intent(SplashActivity.this, HomeActivity.class) ;
                        i.putExtra("iSconnected", "false");
                        startActivity(i);
                        finish();
                    }

                } catch (JSONException e) {
                    Intent i = new Intent(SplashActivity.this, HomeActivity.class) ;
                    i.putExtra("iSconnected", "false");
                    startActivity(i);
                    finish();
                }




            }


            return code;

        } catch (JSONException ex) {

            ex.printStackTrace();
            startActivity(new Intent(SplashActivity.this, HomeActivity.class));

            return "250";
        }
    }

    public void getUserInfo() {


        try {
            RequestQueue queue = Volley.newRequestQueue(this);
            JSONObject jsonBody = new JSONObject();
            Log.e("PROFIL ", "***" + getPreferences().getUser() + "***");

            if (getPreferences().getUser().getId_member() != null) {

                jsonBody.put("id_user", getPreferences().getUser().getId_member());
            }

            final String requestBody = jsonBody.toString();

            StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.profilInfosUrl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e("PROFIL", "Response is " + response);
                    Log.i("VOLLEY", response);

                    parseJSONResponse(response, Constants.profilInfosUrl);

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEYY", error.toString());
                    Intent i = new Intent(SplashActivity.this, HomeActivity.class) ;
                    i.putExtra("iSconnected", "false");
                    startActivity(i);
                    finish();
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("X-Auth-Token", getPreferences().getToken());
                   // startActivity(new Intent(SplashActivity.this, HomeActivity.class));

                    return params;
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        Log.e("requestBody ", requestBody);
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }

                    /*@Override
                    protected Response<String> parseNetworkResponse(NetworkResponse response) {
                        String responseString = "";
                        if (response != null) {
                            responseString = String.valueOf(response);
                            Log.e(TAG, "ResponseString is " + response);
                            // can get more details such as response.headers
                        }
                        return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                    }*/
            };

            queue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
