package com.tunisie.dotit.carthageland.global;

/**
 * Created by eniso on 17/10/2017.
 */

public class Constants {

    //private static String baseUrl  = "http://192.168.1.126:80/CL/web/app.php/api/";
    private static String baseUrl  = "http://carthageland.dotit-corp.com/api/";
    public static String loginUrl = baseUrl + "login";
    public static String subscribeUrl = baseUrl + "register";
    public static String soldeWithEmailUrl = baseUrl + "soldeWithEmail";
    public static String promotionsUrl = baseUrl + "listeCadeauxOffres";
    public static String profilInfosUrl = baseUrl + "profilInformatins";
    public static String govUrl = baseUrl + "getGovernorates";
    public static String countriesUrl = baseUrl + "getAllCountries";
    public static String zipCodesUrl = baseUrl + "getZipCodeByGovernorate";
    public static String sendMessageUrl = baseUrl + "envoiMessage";
    public static String subjectsUrl = baseUrl + "listeSubjects";
    public static String historiqueUrl = baseUrl + "historiqueAchat";
    public static String historiqueVoucherUrl = baseUrl + "historiqueVoucher";
    public static String historiqueVBoutiqueUrl = baseUrl + "historiqueVoucherBoutique";
    public static String pointsConfigUrl = baseUrl + "getPointsConfig";
    public static String listeParrainageUrl = baseUrl + "listeParrainage";
    public static String messagesUrl = baseUrl + "boiteMessagerie";
    public static String assignCarteUrl = baseUrl + "assigneCarte";
    public static String parrainerUrl = baseUrl + "parrainerquelquUn";
    public static String editProfilUrl = baseUrl + "updateClient";
    public static String convPtsUrl = baseUrl + "voucherPointsConvert";
}
