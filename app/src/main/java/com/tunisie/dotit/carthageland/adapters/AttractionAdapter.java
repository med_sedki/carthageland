package com.tunisie.dotit.carthageland.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tunisie.dotit.carthageland.R;
import com.tunisie.dotit.carthageland.adapters.holder.AttractionHolder;
import com.tunisie.dotit.carthageland.adapters.holder.GiftsHolder;
import com.tunisie.dotit.carthageland.global.AttractionItemClickListener;
import com.tunisie.dotit.carthageland.global.VoucherItemClickListener;
import com.tunisie.dotit.carthageland.models.POI;
import com.tunisie.dotit.carthageland.models.Product;

import java.util.ArrayList;

/**
 * Created on 2/15/18.
 */

public class AttractionAdapter extends RecyclerView.Adapter<AttractionHolder> {

    private final Context mContext;
    private ArrayList<POI> mProducts;
    AttractionItemClickListener mAttratcionItemClickListener ;

    public AttractionAdapter( ArrayList<POI> mAttractionList , Context ctx , AttractionItemClickListener attractionItemClickListener ) {
        /*if (location.toString().equals(Constants.Parameters.CL_TUNIS)) {
            mProducts = AppUtils.getTunisRestaurants();
        } else if (location.toString().equals(Constants.Parameters.CL_HAMMAMET)) {
            mProducts = AppUtils.getHammametRestaurants() ;
        }*/
        mProducts = mAttractionList  ;
        this.mContext = ctx ;
        this.mAttratcionItemClickListener = attractionItemClickListener ;

    }

    @Override
    public AttractionHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_attraction, parent, false);
        return new AttractionHolder(view);
    }


    @Override
    public void onBindViewHolder( AttractionHolder holder, int position) {
        POI restaurant = mProducts.get(position);
        holder.bind(restaurant,mAttratcionItemClickListener);
    }

    @Override
    public int getItemCount() {
        return mProducts.size();
    }
}