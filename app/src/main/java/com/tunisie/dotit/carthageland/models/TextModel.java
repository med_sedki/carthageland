package com.tunisie.dotit.carthageland.models;

import com.google.gson.annotations.SerializedName;

public class TextModel {

    @SerializedName("message")
    private String message;
        @SerializedName("site")
    private String site ;


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }
}
