package com.tunisie.dotit.carthageland.global;

import com.tunisie.dotit.carthageland.models.Parc;

/**
 * Created on 2/16/18.
 */

public interface ParcItemClickListener {
    void onItemClicked(Parc parc);
}
