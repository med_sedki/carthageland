package com.tunisie.dotit.carthageland.adapters.holder;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tunisie.dotit.carthageland.BaseApplication;
import com.tunisie.dotit.carthageland.R;
import com.tunisie.dotit.carthageland.Utils.Constants;
import com.tunisie.dotit.carthageland.custom.CustomTextView;
import com.tunisie.dotit.carthageland.global.VoucherItemClickListener;
import com.tunisie.dotit.carthageland.models.POI;
import com.tunisie.dotit.carthageland.models.Product;
import com.tunisie.dotit.carthageland.models.Produit;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Created on 2/15/18.
 */

public class ProductsHolder extends BaseHolder {

    private final TextView mPriceTv;
    TextView mTitle, mDetails, mFidelityPoints;
    CircleImageView mProductImage;
    LinearLayout mVoucherLayout;

    public ProductsHolder(View itemView) {
        super(itemView);
        mTitle = itemView.findViewById(R.id.product_name);
        mDetails = itemView.findViewById(R.id.product_desc);
        mPriceTv = itemView.findViewById(R.id.product_price);
        mFidelityPoints = itemView.findViewById(R.id.fidelity_nbr);
        mVoucherLayout = itemView.findViewById(R.id.main_layout_voucher);
        mProductImage = itemView.findViewById(R.id.product_img);

    }

    public void bind(final Product menuItem, final VoucherItemClickListener voucherItemClickListener) {
        mTitle.setText(menuItem.getName());
      //  mDetails.setText(menuItem.getName());
        mPriceTv.setText(String.format(com.tunisie.dotit.carthageland.application.BaseApplication.getInstance().getString(R.string.tnd_price), menuItem.getPrice()));
        mFidelityPoints.setText(String.format(com.tunisie.dotit.carthageland.application.BaseApplication.getInstance().getString(R.string.points_label),String.valueOf(menuItem.getFidelityPriceNbr())));
        Picasso.with(com.tunisie.dotit.carthageland.application.BaseApplication.getInstance()).load(Constants.baseImageUrl + menuItem.getWebPath()+"/"+menuItem.getFilename().toString()).placeholder(R.drawable.placeholder).resize(200,200).centerCrop().into(mProductImage);
        mVoucherLayout.setOnClickListener(getItemClickListener(menuItem, voucherItemClickListener));

    }

    @NonNull
    private View.OnClickListener getItemClickListener(final Product menuItem, final VoucherItemClickListener mAccountMenuClickListener) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAccountMenuClickListener.onItemClicked(menuItem.getCode().toString());
            }
        };
    }

}
