package com.tunisie.dotit.carthageland.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class UseVoucherResponse implements Parcelable {

    @SerializedName("code")
    private String code;
    @SerializedName("message")
    private VoucherMessage message;
    @SerializedName("response")
    private String response;

    protected UseVoucherResponse(Parcel in) {
        code = in.readString();
        response = in.readString();
    }

    public static final Creator<UseVoucherResponse> CREATOR = new Creator<UseVoucherResponse>() {
        @Override
        public UseVoucherResponse createFromParcel(Parcel in) {
            return new UseVoucherResponse(in);
        }

        @Override
        public UseVoucherResponse[] newArray(int size) {
            return new UseVoucherResponse[size];
        }
    };

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public VoucherMessage getMessage() {
        return message;
    }

    public void setMessage(VoucherMessage message) {
        this.message = message;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(code);
        parcel.writeString(response);
    }
}
