package com.tunisie.dotit.carthageland.models;

import com.google.gson.annotations.SerializedName;

public class Country {

    @SerializedName("nomFrFr")
    private String name;
    @SerializedName("callingCode")
    private String code;
    @SerializedName("phoneLength")
    private String phone;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
