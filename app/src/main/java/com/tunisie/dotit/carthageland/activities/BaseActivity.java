package com.tunisie.dotit.carthageland.activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;

import com.tunisie.dotit.carthageland.R;
import com.tunisie.dotit.carthageland.Utils.Constants;
import com.tunisie.dotit.carthageland.application.BaseApplication;
import com.tunisie.dotit.carthageland.custom.CustomButton;
import com.tunisie.dotit.carthageland.custom.CustomProgressDialog;
import com.tunisie.dotit.carthageland.custom.CustomTextView;
import com.tunisie.dotit.carthageland.global.AppUtils;
import com.tunisie.dotit.carthageland.global.BackgroundManager;
import com.tunisie.dotit.carthageland.global.SharedPreferences;

import javax.inject.Inject;

import dagger.Lazy;


/**
 * Created on 2/4/18.
 */

public class BaseActivity extends AppCompatActivity {

    private static final String TAG = BaseActivity.class.getSimpleName();


    private Runnable mRestartRunnable;

    private CustomProgressDialog mProgressDialog;

    @Inject
    protected Lazy<SharedPreferences> mSharedPreferences;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BaseApplication.getInstance().getApplicationComponent().inject(this);

    }


    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
        bindView();
    }

    private void bindView() {
        onBindViewCompleted();
    }

    protected void onBindViewCompleted() {
        Log.d(TAG, "on Bind View Completed");
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                break;
        }
        return super.dispatchTouchEvent(event);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (mRestartRunnable != null) {
            Log.d(TAG, "start runnable activity");
            mRestartRunnable.run();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    public void quitApp() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
        Log.e("TAG","the home activity is finished here in this point fourth");
        System.exit(0);
    }

    /**
     * show subscribe ok dialog
     */

    public void showSimpleOkDialog(String message, Runnable okAction) {
        showSimpleOkDialog(message, 0, 0, R.string.ok, okAction);
    }

    public void showSimpleOkDialog(String message) {
        showSimpleOkDialog(message, 0, 0, R.string.ok, null);
    }

    public void showSimpleOkDialog(@StringRes int msgId) {
        showSimpleOkDialog(null, 0, msgId, R.string.ok, null);
    }

    public void showSimpleOkDialog(@StringRes int msgId, Runnable okAction) {
        showSimpleOkDialog(null, 0, msgId, R.string.ok, okAction);
    }

    public void showSimpleOkDialog(@StringRes int titleId, @StringRes int btnText, @StringRes int msgId) {
        showSimpleOkDialog(null, titleId, msgId, btnText, null);
    }

    public void showSimpleOkDialog(@StringRes int titleId, @StringRes int msgId, @StringRes int btnText, Runnable okAction) {
        showSimpleOkDialog(null, titleId, msgId, btnText, null);
    }

    public void showSimpleOkDialog(@StringRes int msgId, @StringRes int btnText) {
        showSimpleOkDialog(null, 0, msgId, btnText, null);
    }

    /**
     * show subscribe ok dialog
     *
     * @param msgId    message resources id
     * @param okAction action to do on click
     */
    public void showSimpleOkDialog(String message, @StringRes int titleId, @StringRes int msgId, @StringRes int btnText, final Runnable okAction) {
        if (!isFinishing()) {

            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.custom_error_dialog));
            final View dialogView = getLayoutInflater().inflate(R.layout.dialog_layout, null);
            dialogBuilder.setView(dialogView);

            final AlertDialog alertDialog = dialogBuilder.create();
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            alertDialog.setCancelable(okAction == null ? true : false);
            alertDialog.setCanceledOnTouchOutside(false);

            CustomTextView titleTextView = dialogView.findViewById(R.id.tv_title);
            if (titleId != 0) {
                titleTextView.setVisibility(View.VISIBLE);
                titleTextView.setText(getString(titleId));
            } else {
                titleTextView.setVisibility(View.GONE);
            }


            CustomTextView contentTextView = dialogView.findViewById(R.id.tv_content);
            if (message != null && !TextUtils.isEmpty(message)) {
                contentTextView.setText(message);
            } else {
                contentTextView.setText(getString(msgId));
            }

            CustomButton okBtn = dialogView.findViewById(R.id.btn_ok);
            okBtn.setText(btnText);
            okBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                    if (okAction != null) {
                        okAction.run();
                    }
                }
            });

            CustomButton cancelBtn = dialogView.findViewById(R.id.btn_cancel);
            cancelBtn.setVisibility(View.GONE);

            alertDialog.show();
        }
    }


    public void showSimpleOkDialog(@StringRes int titleId, @StringRes int msgId, @StringRes int btnText, String textData, final Runnable okAction) {
        if (!isFinishing()) {


            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
            final View dialogView = getLayoutInflater().inflate(R.layout.dialog_layout, null);
            dialogBuilder.setView(dialogView);

            final AlertDialog alertDialog = dialogBuilder.create();
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            alertDialog.setCancelable(okAction == null ? true : false);

            CustomTextView titleTextView = dialogView.findViewById(R.id.tv_title);
            if (titleId != 0) {
                titleTextView.setVisibility(View.VISIBLE);
                titleTextView.setText(getString(titleId));
            } else {
                titleTextView.setVisibility(View.GONE);
            }


            CustomTextView contentTextView = dialogView.findViewById(R.id.tv_content);

            contentTextView.setText(getString(msgId, textData));

            CustomButton okBtn = dialogView.findViewById(R.id.btn_ok);
            okBtn.setText(btnText);
            okBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                    if (okAction != null) {
                        okAction.run();
                    }
                }
            });

            CustomButton cancelBtn = dialogView.findViewById(R.id.btn_cancel);
            cancelBtn.setVisibility(View.GONE);

            alertDialog.show();
        }
    }

    /**
     * show subscribe ok dialog
     *
     * @param msgId    message resources id
     * @param okId     ok button resources id
     * @param cancelId cancel button resources id
     * @param okAction action to do on click ok
     */
    public void showChoseDialog(@StringRes int msgId, @StringRes int okId, @StringRes int cancelId, final Runnable okAction) {
        showChoseDialog(msgId, okId, cancelId, okAction, null);
    }

    public void showChoseDialog(@StringRes int titleId, @StringRes int msgId, @StringRes int okId, @StringRes int cancelId, final Runnable okAction) {
        showChoseDialog(titleId, msgId, okId, cancelId, okAction, null);
    }


    /**
     * show subscribe ok dialog
     *
     * @param msgId        message resources id
     * @param okId         ok button resources id
     * @param cancelId     cancel button resources id
     * @param okAction     action to do on click ok
     * @param cancelAction action to do on click cancel
     */
    public void showChoseDialog(@StringRes int msgId, @StringRes int okId, @StringRes int cancelId, final Runnable okAction, final Runnable cancelAction) {
        if (!isFinishing()) {

            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
            final View dialogView = getLayoutInflater().inflate(R.layout.dialog_layout, null);
            dialogBuilder.setView(dialogView);

            final AlertDialog alertDialog = dialogBuilder.create();
            alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            alertDialog.setCancelable(okAction == null ? true : false);


            CustomTextView titleTextView = dialogView.findViewById(R.id.tv_title);
            titleTextView.setVisibility(View.GONE);

            CustomTextView contentTextView = dialogView.findViewById(R.id.tv_content);
            contentTextView.setText(getString(msgId));

            CustomButton okBtn = dialogView.findViewById(R.id.btn_ok);
            okBtn.setVisibility(View.VISIBLE);
            okBtn.setText(okId);
            okBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                    if (okAction != null) {
                        okAction.run();
                    }
                }
            });

            CustomButton cancelBtn = dialogView.findViewById(R.id.btn_cancel);
            cancelBtn.setVisibility(View.VISIBLE);
            cancelBtn.setText(cancelId);
            cancelBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                    if (cancelAction != null) {
                        cancelAction.run();
                    }
                }
            });

            alertDialog.show();
        }
    }

    public void showChoseDialog(@StringRes int titleId, @StringRes int msgId, @StringRes int okId, @StringRes int cancelId, final Runnable okAction, final Runnable cancelAction) {
        if (!isFinishing()) {

            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
            final View dialogView = getLayoutInflater().inflate(R.layout.dialog_layout, null);
            dialogBuilder.setView(dialogView);

            final AlertDialog alertDialog = dialogBuilder.create();
            alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            alertDialog.setCancelable(okAction == null ? true : false);


            CustomTextView titleTextView = dialogView.findViewById(R.id.tv_title);
            titleTextView.setVisibility(View.VISIBLE);
            titleTextView.setText(titleId);

            CustomTextView contentTextView = dialogView.findViewById(R.id.tv_content);
            contentTextView.setText(getString(msgId));

            CustomButton okBtn = dialogView.findViewById(R.id.btn_ok);
            okBtn.setVisibility(View.VISIBLE);
            okBtn.setText(okId);
            okBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                    if (okAction != null) {
                        okAction.run();
                    }
                }
            });

            CustomButton cancelBtn = dialogView.findViewById(R.id.btn_cancel);
            cancelBtn.setVisibility(View.VISIBLE);
            cancelBtn.setText(cancelId);
            cancelBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                    if (cancelAction != null) {
                        cancelAction.run();
                    }

                }
            });

            alertDialog.show();
        }
    }

    /**
     * show unavailable Network Error
     */
    public void showErrorNetworkDialog() {

        showSimpleOkDialog(R.string.error_network, R.string.ok);
    }


    /**
     * show blocking progressBar on the root of the activity
     */
    public void showProgressBar() {

        if (!isFinishing()) {

            if (mProgressDialog == null) {
                mProgressDialog = new CustomProgressDialog(this, R.style.ProgressDialogStyle);
                mProgressDialog.setCancelable(false);

            }

            if (!mProgressDialog.isShowing()) {
                mProgressDialog.show();
            }

        }
    }

    /**
     * hide blocking progressBar
     */
    public void hideProgressBar() {

        if (!isFinishing()) {

            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
            }
        }
    }

    @NonNull
    public Runnable getUnauthorizedAction() {
        return new Runnable() {
            @Override
            public void run() {
                navigateToSignIn();
            }
        };
    }

    private void navigateToSignIn() {

        BaseApplication.getInstance().clearUserCache();

        //Intent intent = new Intent(BaseActivity.this, LoginActivity.class);
        //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        //startActivity(intent);
        //overridePendingTransition(R.anim.slide_in_top, R.anim.no);

    }


    /**
     * Start new activity when the app on foreground
     */
    public void startActivityWhenForeground(final Intent intent) {

        startActivityWhenForeground(intent, false);
    }


    /**
     * Start new activity when the app on foreground
     */
    public void startActivityWhenForeground(final Intent intent, final boolean shouldFinish) {

        mRestartRunnable = buildStartRunnable(intent, shouldFinish);
        if (!BackgroundManager.getInstance().isInBackground()) {
            Log.d(TAG, "start activity when foreground isApp on foreground true");
            mRestartRunnable.run();
        } else {
            Log.d(TAG, "start activity when foreground isApp on foreground false");
        }
    }


    /**
     * Start new activity when the app on foreground
     */
    public void startActivityWhenForeground(final Class activity) {

        startActivityWhenForeground(activity, false);

    }

    /**
     * Start new activity when the app on foreground
     *
     * @param shouldFinish current activity
     */

    public void startActivityWhenForeground(final Class activity, final boolean shouldFinish) {

        Intent intent = new Intent(BaseActivity.this, activity);
        startActivityWhenForeground(intent, shouldFinish);

    }


    @NonNull
    private Runnable buildStartRunnable(final Intent intent, final boolean shouldFinish) {
        return new Runnable() {
            @Override
            public void run() {

                Log.d(TAG, "run build start runnable");

                startActivity(intent);

                if (shouldFinish) finish();
                Log.e("TAG","the home activity is finished here in this point first");

                mRestartRunnable = null;
            }
        };
    }


    /**
     * Start new activity for result when the app on foreground
     */
    public void startActivityForResultWhenForeground(final Intent intent, int requestCode) {

        startActivityForResultWhenForeground(intent, false, requestCode);
    }


    /**
     * Start new activity for result when the app on foreground
     */
    public void startActivityForResultWhenForeground(final Intent intent, final boolean shouldFinish, int requestCode) {

        mRestartRunnable = buildStartForResultRunnable(intent, shouldFinish, requestCode);
        if (!BackgroundManager.getInstance().isInBackground()) {
            Log.d(TAG, "start activity when foreground isApp on foreground true");
            mRestartRunnable.run();
        } else {
            Log.d(TAG, "start activity when foreground isApp on foreground false");
        }
    }


    /**
     * Start new activity for result when the app on foreground
     */
    public void startActivityForResultWhenForeground(final Class activity, int requestCode) {

        startActivityForResultWhenForeground(activity, false, requestCode);

    }

    /**
     * Start new activity for result when the app on foreground
     *
     * @param shouldFinish current activity
     */

    public void startActivityForResultWhenForeground(final Class activity, final boolean shouldFinish, int requestCode) {

        Intent intent = new Intent(BaseActivity.this, activity);
        startActivityForResultWhenForeground(intent, shouldFinish, requestCode);

    }


    @NonNull
    private Runnable buildStartForResultRunnable(final Intent intent, final boolean shouldFinish, final int requestCode) {
        return new Runnable() {
            @Override
            public void run() {

                Log.d(TAG, "run build start runnable");

                startActivityForResult(intent, requestCode);

                if (shouldFinish) finish();
                Log.e("TAG","the home activity is finished here in this point second");

                mRestartRunnable = null;
            }
        };
    }

    /**
     * show error network
     */
    public boolean isNetworkAvailable() {
        return AppUtils.isNetworkAvailable(this);
    }

    /**
     * try to hide Keyboard from the focused screen
     */
    public void hideKeyboard() {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
            Log.d(TAG, "Could not hide keyboard, window unreachable. " + e.toString());
        }
    }

    @Override
    protected void onPause() {
        hideKeyboard();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        cancelProgress();
        super.onDestroy();
    }

    private void cancelProgress() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        pendingTransition();
    }

    @Override
    public void finish() {
        super.finish();
        Log.e("TAG","the home activity is finished here in this point third");
        if (!(this instanceof HomeActivity)) {
            pendingTransition();
        }
    }

    private void pendingTransition() {

        overridePendingTransition(R.anim.no, R.anim.slide_out_bottom);

    }

    /**
     * This method return a SharedPreferences instance
     *
     * @return A SharedPreferences
     */
    protected SharedPreferences getPreferences() {
        return mSharedPreferences.get();
    }


}
