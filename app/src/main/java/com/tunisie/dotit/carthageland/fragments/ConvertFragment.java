package com.tunisie.dotit.carthageland.fragments;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.tunisie.dotit.carthageland.R;
import com.tunisie.dotit.carthageland.Utils.Constants;
import com.tunisie.dotit.carthageland.activities.HomeActivity;
import com.tunisie.dotit.carthageland.application.BaseApplication;
import com.tunisie.dotit.carthageland.custom.CustomEditText;
import com.tunisie.dotit.carthageland.custom.CustomTextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class ConvertFragment extends BaseFragment {

    View view;
    CustomTextView nb_pts;
    CustomEditText nb_pts_conv;
    CustomTextView btn_conv;
    BaseApplication myApp;
    private SweetAlertDialog progressDoalog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_convert, container, false);
        myApp = ((BaseApplication) this.getActivity().getApplication());

        nb_pts = (CustomTextView) view.findViewById(R.id.nb_pts);
        nb_pts_conv = (CustomEditText) view.findViewById(R.id.nb_pts_conv);
        nb_pts.setText(getPreferences().getBalancePts() + " points");
        btn_conv = (CustomTextView) view.findViewById(R.id.btn_conv);
        HomeActivity.toolbar.setNavigationIcon(R.drawable.back_arrow_ic);
        HomeActivity.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getFragmentManager() != null) {
                    Fragment currentFragment = getFragmentManager().findFragmentById(R.id.flContent);
                    if (currentFragment instanceof ConvertFragment) {

                        navigateToSmilesFragment();
                    }
                } else {
                    HomeActivity.openDrawer();
                }

            }
        });
        btn_conv.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                View focusView = null;
                boolean cancel = false;
                if (TextUtils.isEmpty(nb_pts_conv.getText())) {
                    nb_pts_conv.setError(getString(R.string.error_field_required));
                    focusView = nb_pts_conv;
                    cancel = true;
                }
                if (cancel) {
                    // There was an error; don't attempt login and focus the first
                    // form field with an error.
                    focusView.requestFocus();
                } else {
                    int balancePts = Integer.parseInt(nb_pts_conv.getText().toString());
                    if (balancePts <= Integer.parseInt(getPreferences().getCard().getSolde_points())) {
                        convertPoints(balancePts);
                    } else {
                        progressDoalog = new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE);
                        progressDoalog.setTitleText("Erreur...");
                        progressDoalog.setContentText("Le solde saisi dépasse le solde des points disponible.");
                        progressDoalog.setConfirmText("OK");
                        progressDoalog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                        progressDoalog.show();

                    }
                }
            }
        });
        return view;
    }

    private void closeFragment() {
        getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
    }

    private void navigateToSmilesFragment() {
        Fragment fragment = null;
        try {
            closeFragment();
            fragment = SmilesFragment.class.newInstance();

        } catch (Exception e) {
            e.printStackTrace();
        }
        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).addToBackStack(null).commit();
    }

    public void convertPoints(int nb_pts) {
        progressDoalog = new SweetAlertDialog(this.getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        progressDoalog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressDoalog.setTitleText("Chargement");
        progressDoalog.setCancelable(true);
        progressDoalog.show();
        try {
            RequestQueue queue = Volley.newRequestQueue(this.getActivity());
            JSONObject jsonBody = new JSONObject();

            jsonBody.put("id_client", Integer.parseInt(getPreferences().getUserId()));
            jsonBody.put("pointsConvertir", nb_pts);

            final String requestBody = jsonBody.toString();

            StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.convPtsUrl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e("Smiles", "Response is " + response);
                    Log.i("VOLLEY", response);
                    parseJSONResponse(response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDoalog.setTitleText("Erreur!")
                            .setContentText("Une erreur est survenue, veuillez vérifier votre connexion Internet ou essayer plus tard.")
                            .setConfirmText("OK")
                            .changeAlertType(SweetAlertDialog.ERROR_TYPE);
                    Log.e("VOLLEYY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("X-Auth-Token", getPreferences().getToken());
                    return params;
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        Log.e("requestBody ", requestBody);
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }
                        /*@Override
                        protected Response<String> parseNetworkResponse(NetworkResponse response) {
                            String responseString = "";
                            if (response != null) {
                                responseString = String.valueOf(response);
                                Log.e(TAG, "ResponseString is " + response);
                                // can get more details such as response.headers
                            }
                            return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                        }*/
            };
            queue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String parseJSONResponse(String jsonResponse) {
        try {
            final JSONObject json = new JSONObject(jsonResponse);

            String code = json.getString("code");
            Log.e("Scan json", String.valueOf(code));

            if (code.equals("200")) {
                final String response = json.getString("response");
                progressDoalog.setTitleText("")
                        .setContentText("Votre voucher a été créé avec succès.")
                        .setConfirmText("OK")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                progressDoalog.dismissWithAnimation();
                                JSONObject response = null;
                                try {
                                    response = json.getJSONObject("response");
                                    String remainingPoints = response.getString("remainingPoints");
                                    nb_pts_conv.setText("");
                                    nb_pts.setText(remainingPoints + "points");
                                    getPreferences().setBalancePts(remainingPoints);
                                    nb_pts.setText(getPreferences().getBalancePts());


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                // int pts = Integer.parseInt(getPreferences().getCard().getSolde_points()) - Integer.parseInt(nb_pts_conv.getText().toString());
                                //  getPreferences().getCard().setSolde_points(pts + "");
                            }
                        })
                        .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
            } else {
                String msgErr = json.getJSONObject("message").getString("message");
                progressDoalog.setTitleText("Erreur!")
                        .setContentText(msgErr)
                        .setConfirmText("OK")
                        .changeAlertType(SweetAlertDialog.ERROR_TYPE);
                Log.e("msgErr ", msgErr);
            }
            return code;
        } catch (JSONException ex) {
            ex.printStackTrace();
            return "250";
        }
    }

    public void fragmentreplace(Class fragmentClass, String tag) throws IllegalAccessException, java.lang.InstantiationException {
        FragmentManager fragmentManager = getFragmentManager();
        Fragment fragment = (Fragment) fragmentClass.newInstance();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.flContent, fragment, tag);
        fragmentTransaction.addToBackStack("tag"); //this will add it to back stack
        fragmentTransaction.commit();
    }

}