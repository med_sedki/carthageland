package com.tunisie.dotit.carthageland.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.iid.FirebaseInstanceId;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.tunisie.dotit.carthageland.R;
import com.tunisie.dotit.carthageland.Utils.Constants;
import com.tunisie.dotit.carthageland.application.BaseApplication;
import com.tunisie.dotit.carthageland.custom.CustomEditText;
import com.tunisie.dotit.carthageland.custom.CustomProgressDialog;
import com.tunisie.dotit.carthageland.custom.CustomTextView;
import com.tunisie.dotit.carthageland.global.SharedPreferences;
import com.tunisie.dotit.carthageland.models.Carte;
import com.tunisie.dotit.carthageland.models.Client;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import javax.inject.Inject;

import dagger.Lazy;

public class LoginActivity extends BaseActivity implements View.OnClickListener {

    private static final String TAG = "Login";
    private SweetAlertDialog progressDoalog;
    public static Toolbar toolbar;
    private CustomEditText txt_email;
    private CustomEditText txt_password;
    CustomTextView mForgotPasswordTv;

    CustomTextView btn_login;
    CustomTextView btn_new_account;

    private String tokenDevice;
    private CustomProgressDialog mProgressDialog;
    @Inject
    protected Lazy<SharedPreferences> mSharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        txt_email = findViewById(R.id.txt_email);
        txt_password = findViewById(R.id.txt_password);
        mForgotPasswordTv = findViewById(R.id.btn_pwd);
        btn_login = findViewById(R.id.btn_login);
        btn_new_account = findViewById(R.id.btn_new_account);

        mForgotPasswordTv.setOnClickListener(this);
        btn_login.setOnClickListener(this);
        btn_new_account.setOnClickListener(this);

        LoginActivity.this.setTitle("Connexion");
        tokenDevice = FirebaseInstanceId.getInstance().getToken();
        Log.e(TAG, "token push " + tokenDevice);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationIcon(R.drawable.back_arrow_ic);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                getPreferences().setConnectedMode("false");
                intent.putExtra("iSconnected", "false ");
                finish();
                startActivity(intent);
            }
        });
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                return false;
            }
        });
    }

    public void login() {
        if ((TextUtils.isEmpty(txt_email.getText())) || (TextUtils.isEmpty(txt_password.getText()))) {
            final SweetAlertDialog errDialog = new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE);
            errDialog.setTitleText("Erreur...");
            errDialog.setContentText("Veuillez saisir tous les champs!");
            errDialog.show();
        } else {
            progressDoalog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
            progressDoalog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
            progressDoalog.setTitleText("Chargement");
            progressDoalog.setCancelable(true);
            progressDoalog.show();
            // Instantiate the RequestQueue.
            try {
                RequestQueue queue = Volley.newRequestQueue(this);
                JSONObject jsonBody = new JSONObject();

                jsonBody.put("login", txt_email.getText().toString());
                jsonBody.put("password", txt_password.getText().toString());
                jsonBody.put("notificationToken", tokenDevice);

                final String requestBody = jsonBody.toString();
                Log.e(TAG, "Constants.loginUrl " + Constants.loginUrl);

                StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.loginUrl, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e(TAG, "Response is " + response);
                        Log.i("V OLLEY", response);
                        parseJSONResponse(response);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDoalog.setTitleText("Erreur!")
                                .setContentText("Une erreur est survenue, veuillez vérifier votre connexion Internet ou essayer plus tard.")
                                .setConfirmText("OK")
                                .changeAlertType(SweetAlertDialog.ERROR_TYPE);
                        Log.e("VOLLEYY", error.toString());
                    }
                }) {
                    @Override
                    public String getBodyContentType() {
                        return "application/json; charset=utf-8";
                    }

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            Log.e("requestBody ", requestBody);
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }

                    /*@Override
                    protected Response<String> parseNetworkResponse(NetworkResponse response) {
                        String responseString = "";
                        if (response != null) {
                            responseString = String.valueOf(response);
                            Log.e(TAG, "ResponseString is " + response);
                            // can get more details such as response.headers
                        }
                        return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                    }*/
                };

                queue.add(stringRequest);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public String parseJSONResponse(String jsonResponse) {
        try {
            final JSONObject json = new JSONObject(jsonResponse);

            String code = json.getString("code");
            Log.e("Scan json", String.valueOf(code));

            if (code.equals("200")) {
                String id_user = "";
                String id_member = "";
                String birthdate = "";
                String firstname = "";
                String lastname = "";
                String email = "";
                String phone = "";
                String address = "";
                String points = "";
                String clientType = "";
                String num_carte = "";
                String type_carte = "";
                Carte crt = null;

                JSONObject response = json.getJSONObject("response");
                JSONObject user = response.getJSONObject("user");

                if ((user.has("id")) && (!user.isNull("id"))) {
                    id_member = user.getString("id");
                    Log.e("TOKEN ", "***id_member*** " + id_member);
                }

                if ((user.has("client")) && (!user.isNull("client"))) {

                    JSONObject client = user.getJSONObject("client");
                    if ((client.has("id")) && (!client.isNull("id"))) {
                        id_user = client.getString("id");
                        Log.e("TOKEN ", "***id_user*** " + id_user);
                    }
                    if ((client.has("birthday")) && (!client.isNull("birthday"))) {
                        birthdate = client.getString("birthday");
                        Log.e("TOKEN ", "***birthday*** " + birthdate);
                    }
                    firstname = client.getString("firstname");
                    Log.e("TOKEN ", "***firstname*** " + firstname);
                    lastname = client.getString("lastname");
                    Log.e("TOKEN ", "***lasttname*** " + lastname);
                    email = client.getString("mail");
                    Log.e("TOKEN ", "***email*** " + email);
                    phone = client.getString("phone");
                    Log.e("TOKEN ", "***phone*** " + phone);
                    address = client.getString("address");
                    Log.e("TOKEN ", "***address*** " + address);
                    points = client.getString("points");
                    Log.e("TOKEN ", "***points*** " + points);
                    clientType = client.getJSONObject("clientType").getString("type");
                    Log.e("TOKEN ", "***clientType*** " + clientType);
                    try {
                        if ((client.has("carte")) && (!client.isNull("carte"))) {
                            JSONObject carte = client.getJSONObject("carte");
                            Log.e("TOKEN ", "***carte*** " + carte);
                            num_carte = carte.getString("code");
                            Log.e("TOKEN ", "***num_carte*** " + num_carte);
                            type_carte = carte.getJSONObject("type").getString("name");
                            Log.e("TOKEN ", "***type_carte*** " + type_carte);
                            crt = new Carte(num_carte, points, type_carte, id_user);
                            getPreferences().setCard(crt);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                String token = response.getString("token");
                Client cl = new Client(id_user, id_member, lastname, firstname, email, birthdate, address, phone, crt);

                BaseApplication myApp = ((BaseApplication) this.getApplication());
                myApp.setEmail(email);
                myApp.setToken(token);
                myApp.setId_user(id_user);
                myApp.setClient(cl);
                myApp.setCarte(crt);
                getPreferences().saveActiveUser(cl);
                getPreferences().setUseId(id_user);
                getPreferences().setCard(crt);
                getPreferences().setToken(token);
                Log.e("TAG", "the token is " + token);
                getPreferences().setEmail(email);
                if (crt != null) {
                    getPreferences().setBalancePts(crt.getSolde_points());
                }

                progressDoalog.dismiss();
                Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                getPreferences().setConnectedMode("true");
                intent.putExtra("iSconnected", "true");
                finish();
                startActivity(intent);

                overridePendingTransition(R.anim.no, R.anim.slide_in_top);
            } else if (code.equals("400")) {
                progressDoalog.setTitleText("Erreur!")
                        .setContentText(json.getString("message"))
                        .setConfirmText("OK")
                        .changeAlertType(SweetAlertDialog.ERROR_TYPE);
                String msgErr = json.getString("message");
                Log.e("msgErr ", msgErr);

            } else {
                progressDoalog.setTitleText("Erreur!")
                        .setContentText("Veuillez vérifier votre login/mot de passe")
                        .setConfirmText("OK")
                        .changeAlertType(SweetAlertDialog.ERROR_TYPE);
                String msgErr = json.getString("message");
                Log.e("msgErr ", msgErr);
            }
            return code;
        } catch (JSONException ex) {
            ex.printStackTrace();
            return "250";
        }
    }

    public void showProgressBar() {
        if (!isFinishing()) {
            if (mProgressDialog == null) {
                mProgressDialog = new CustomProgressDialog(this, R.style.ProgressDialogStyle);
                mProgressDialog.setCancelable(false);
            }
            if (!mProgressDialog.isShowing()) {
                mProgressDialog.show();
            }
        }
    }

    /**
     * hide blocking progressBar
     */
    public void hideProgressBar() {
        if (!isFinishing()) {
            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
            }
        }
    }

    @Override
    public void onClick(View v) {
        if (v == mForgotPasswordTv) {
            startActivity(new Intent(LoginActivity.this, ForgetPwdActivity.class));
        } else if (v == btn_login) {
            login();
        } else if (v == btn_new_account) {
            Intent myIntent = new Intent(LoginActivity.this, RegisterActivity.class);
            startActivity(myIntent);
        }
    }
}