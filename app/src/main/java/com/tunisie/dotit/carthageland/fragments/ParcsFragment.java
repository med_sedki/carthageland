package com.tunisie.dotit.carthageland.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.tunisie.dotit.carthageland.R;
import com.tunisie.dotit.carthageland.Utils.Constants;
import com.tunisie.dotit.carthageland.activities.HomeActivity;
import com.tunisie.dotit.carthageland.activities.ParcDetailsActivity;
import com.tunisie.dotit.carthageland.adapters.ParcsAdapter;
import com.tunisie.dotit.carthageland.global.APIClient;
import com.tunisie.dotit.carthageland.global.APIInterface;
import com.tunisie.dotit.carthageland.global.ParcItemClickListener;
import com.tunisie.dotit.carthageland.models.Parc;
import com.tunisie.dotit.carthageland.models.ParcResponse;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by dotit on 4/12/18.
 */

public class ParcsFragment extends BaseFragment implements ParcItemClickListener {

    RecyclerView mParcsRecycler;
    View mView;
    public APIInterface mApiInerface;
    private ArrayList<Parc> mParcsList = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        mView = inflater.inflate(R.layout.parcs_fragment, container, false);

        HomeActivity.toolbar.setNavigationIcon(R.drawable.back_arrow_ic);
        HomeActivity.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getFragmentManager() != null) {
                    Fragment currentFragment = getFragmentManager().findFragmentById(R.id.flContent);
                    if (currentFragment instanceof ParcsFragment) {
                        navigateToHomeFragment();
                    }
                } else {
                    HomeActivity.openDrawer();
                }

            }
        });

        //  this.getActivity().setTitle("Parcs");
        mParcsRecycler = mView.findViewById(R.id.parcs_recycler);
        mParcsRecycler.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        if (getPreferences().getCarthageLandLocation().toString().equals(Constants.Parameters.CL_TUNIS)) {
            getParcsList(Constants.Sites.TUNIS_PARAM, null);
        } else if (getPreferences().getCarthageLandLocation().toString().equals(Constants.Parameters.CL_HAMMAMET)) {
            getParcsList(Constants.Sites.HHAMMAMET_PARAM, null);
        }
        Toolbar toolbarTop = (Toolbar) getActivity().findViewById(R.id.toolbar);
        TextView mTitle = (TextView) toolbarTop.findViewById(R.id.toolbar_title);
        mTitle.setText("Nos parcs");



        return mView;
    }

    public void getParcsList(final String site_praram, final String category_param) {
        mApiInerface = APIClient.getClient().create(APIInterface.class);

        showProgressBar();

        /**
         GET List Resources
         **/
        Call<ParcResponse> call = mApiInerface.getParcsList(getPreferences().getToken(), site_praram, category_param);
        call.enqueue(new Callback<ParcResponse>() {
            @Override
            public void onResponse(Call<ParcResponse> call, Response<ParcResponse> response) {


                //    Log.d("TAG", response.body().getResponse() + " response get poi ");
                Log.d("TAG", response + "");

                if (response.isSuccessful() && response.body() != null && response.body().getResponse() != null) {
                    if (!response.body().getResponse().isEmpty()) {
                        for (int i = 0; i < response.body().getResponse().size(); i++) {
                            Parc parc = response.body().getResponse().get(i);
                            Log.e("TAG", parc.getName().toString());
                            mParcsList.add(parc);

                        }
                        hideProgressBar();
                        hideProgressBar();
                        ParcItemClickListener parcItemClickListener = new ParcItemClickListener() {
                            @Override
                            public void onItemClicked(Parc parc) {
                                ParcDetailsActivity.SelectedParcCode = parc.getCode();
                                ParcDetailsActivity.SelectedAttractionName = parc.getName();
                                Intent myIntent = new Intent(getActivity(), ParcDetailsActivity.class);
                                startActivity(myIntent);
                            }
                        };
                        mParcsRecycler.setAdapter(new ParcsAdapter(mParcsList, getPreferences().getCarthageLandLocation(), getActivity(), parcItemClickListener));
                    } else {
                        hideProgressBar();
                        Toast.makeText(getActivity(), getString(R.string.error_empty_list), Toast.LENGTH_SHORT).show();
                    }
                } else {

                    Toast.makeText(getActivity(), getString(R.string.error_server), Toast.LENGTH_SHORT).show();
                    hideProgressBar();
                }

            }

            @Override
            public void onFailure(Call<ParcResponse> call, Throwable t) {
                hideProgressBar();
                Toast.makeText(getActivity(), getString(R.string.error_server), Toast.LENGTH_SHORT).show();
                call.cancel();
            }
        });
    }

    private void closeFragment() {
        getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();

    }

    private void navigateToHomeFragment() {
        Fragment fragment = null;
        try {
            closeFragment();
            //fragment = HomeFragment.class.newInstance();

        } catch (Exception e) {
            e.printStackTrace();
        }

        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        fragmentManager.popBackStack();
        //fragmentManager.beginTransaction().replace(R.id.flContent, fragment).addToBackStack(null).commit();
    }
//
//    public void navigateToHomeFragment(String tag) throws IllegalAccessException, java.lang.InstantiationException {
//        FragmentManager fragmentManager = getFragmentManager();
//        Fragment fragment = HomeFragment.class.newInstance();
//        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//        fragmentTransaction.replace(R.id.flContent, fragment, tag);
//        fragmentTransaction.addToBackStack(null); //this will add it to back stack
//        fragmentTransaction.commit();
//    }

    @Override
    public void onItemClicked(Parc parc) {

    }
}
