package com.tunisie.dotit.carthageland.models;

/**
 * Created by eniso on 06/10/2017.
 */

public class Promotion {
    private String id_promotion;
    private Produit produit;
    private String dateDebut;
    private String dateFin;
    private String prix;

    public Promotion(String id_promotion, Produit produit, String dateDebut, String dateFin, String prix) {
        this.id_promotion = id_promotion;
        this.produit = produit;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.prix = prix;
    }

    public String getId_promotion() {
        return id_promotion;
    }

    public void setId_promotion(String id_promotion) {
        this.id_promotion = id_promotion;
    }

    public Produit getProduit() {
        return produit;
    }

    public void setProduit(Produit produit) {
        this.produit = produit;
    }

    public String getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(String dateDebut) {
        this.dateDebut = dateDebut;
    }

    public String getDateFin() {
        return dateFin;
    }

    public void setDateFin(String dateFin) {
        this.dateFin = dateFin;
    }

    public String getPrix() {
        return prix;
    }

    public void setPrix(String prix) {
        this.prix = prix;
    }
}
