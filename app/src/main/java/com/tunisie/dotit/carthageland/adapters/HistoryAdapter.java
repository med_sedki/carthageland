package com.tunisie.dotit.carthageland.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.tunisie.dotit.carthageland.R;
import com.tunisie.dotit.carthageland.custom.CustomTextView;
import com.tunisie.dotit.carthageland.models.Historique;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class HistoryAdapter extends ArrayAdapter<Historique> {

    public HistoryAdapter(Context context, List<Historique> tweets) {
        super(context, 0, tweets);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_history,parent, false);
        }


        HistoryViewHolder viewHolder = (HistoryViewHolder) convertView.getTag();
        if(viewHolder == null){
            viewHolder = new HistoryViewHolder();
            CustomTextView code = convertView.findViewById(R.id.code);

            CustomTextView montant = convertView.findViewById(R.id.montant);

            CustomTextView date = convertView.findViewById(R.id.first_date);

            CustomTextView points = convertView.findViewById(R.id.points);
            viewHolder.date = date;
            viewHolder.code = code;
            viewHolder.montant = montant;
            viewHolder.points = points;
            convertView.setTag(viewHolder);
        }

        //getItem(position) va récupérer l'item [position] de la List<Tweet> tweets
        Historique historique = getItem(position);
        String CurrentString = historique.getId_historique().toString() ;
        String[] separated = CurrentString.split("\\+");
        SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
        Date hitsoryItemDate = null;
        try {
            hitsoryItemDate = myFormat.parse(separated[0]);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy HH:mm:ss");
        String newFormat = fmtOut.format(hitsoryItemDate);
        viewHolder.date.setText(newFormat);
        viewHolder.code.setText(historique.getId_historique());
        viewHolder.montant.setText(historique.getMontant());
        viewHolder.points.setText(historique.getPointsF() + " points");

        return convertView;
    }

    private class HistoryViewHolder{
        public CustomTextView date;
        public CustomTextView code;
        public CustomTextView montant;
        public CustomTextView points;

    }

}
