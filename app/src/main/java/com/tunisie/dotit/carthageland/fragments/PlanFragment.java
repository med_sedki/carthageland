package com.tunisie.dotit.carthageland.fragments;


import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.GroundOverlay;
import com.google.android.gms.maps.model.GroundOverlayOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.tunisie.dotit.carthageland.R;
import com.tunisie.dotit.carthageland.Utils.Constants;
import com.tunisie.dotit.carthageland.activities.HomeActivity;
import com.tunisie.dotit.carthageland.application.BaseApplication;
import com.tunisie.dotit.carthageland.custom.CustomTextView;
import com.tunisie.dotit.carthageland.global.APIInterface;
import com.tunisie.dotit.carthageland.models.POI;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class PlanFragment extends BaseFragment implements OnMapReadyCallback,
        GoogleMap.OnMapClickListener,
        GoogleMap.OnGroundOverlayClickListener,
        GoogleMap.OnMarkerClickListener,
        View.OnClickListener,
        GoogleMap.OnInfoWindowClickListener {

    private static ViewPager mPager;
    private TabLayout mTabLayout;
    TextView mTvTitle, mTvDescription;
    ImageView mMarkerImage;

    BaseApplication myApp;

    //  private static final LatLng NEWARK = new LatLng(40.714086, -74.228697);
    /*private static final LatLng NEWARK = new LatLng(36.368725, 10.532607);
    private static final LatLng NEWARK_LEFT = new LatLng(36.370305, 10.534751);
    private static final LatLng NEWARK_NEAR_LEFT = new LatLng(36.370236, 10.535367);
    private static final LatLng NEWARK_RIGHT = new LatLng(36.369136, 10.536489);
    private static final LatLng NEWARK_NEAR_RIGHT = new LatLng(36.368805, 10.535985);
    private static final LatLng NEWARK_NEAR_RIGHT1 = new LatLng(36.367986, 10.535348);
    private static final LatLng NEWARK_ALIBABA = new LatLng(36.368234, 10.534522);
    private static final LatLng NEWARK_STELLA = new LatLng(36.368674, 10.532939);
    //private static final LatLng NEWARK_BOTTOM_LEFT = new LatLng(36.368212, 10.534262);
    //private static final LatLng NEWARK_BOTTOM  = new LatLng(36.368451, 10.533215);
    //private static final LatLng NEWARK_LEFT_  = new LatLng(36.368674, 10.532939);

    private static final LatLng NEAR_NEWARK =
            new LatLng(NEWARK.latitude - 0.001, NEWARK.longitude - 0.025);*/
    private CustomTextView mAttractions, mToilets, mRestaurants, mStores, mServices;
    private static LatLng NEWARK;
    private static LatLng NEWARK_LEFT;
    private static LatLng NEWARK_NEAR_LEFT;
    private static LatLng NEWARK_RIGHT;
    private static LatLng NEWARK_NEAR_RIGHT;
    private static LatLng NEWARK_NEAR_RIGHT1;
    private static LatLng NEWARK_ALIBABA;
    private static LatLng NEWARK_STELLA;
    private static LatLng NEWARK_BOTTOM_LEFT;
    /*private static final LatLng NEWARK_BOTTOM  = new LatLng(36.831334, 10.223496);
    private static final LatLng NEWARK_LEFT_  = new LatLng(36.831609, 10.223104);*/

    private static LatLng NEAR_NEWARK;

    private ArrayList<Marker> mMarkersList = new ArrayList<>();

    private final List<BitmapDescriptor> mImages = new ArrayList<BitmapDescriptor>();

    private GroundOverlay mGroundOverlay;

    private GroundOverlay mGroundOverlayRotated;

    private SeekBar mTransparencyBar;

    private int mCurrentEntry = 0;

    private GoogleMap mGoogleMap;
    private LatLng mOriginalZoom;
    private BottomSheetBehavior mBottomSheetBehavior;
    private RelativeLayout mMapView;
    APIInterface mApiInerface;
    private ArrayList<POI> mPoisList = new ArrayList<>();
    private ImageView mPoiImage;
    private TextView mPoiTitle, mPoiDescription;
    private LatLngBounds CARTHAGELAND_HAMMAMET_BOUNDS;
    private LatLngBounds CARTHAGELAND_TUNIS_BOUNDS;
    private Bitmap tunisBitmap = null, hammametBitmap = null;
    private Target mTarget;

    // private static final LatLngBounds DUBAI_BOUNDS = new LatLngBounds(new LatLng(24.5908366068, 54.84375),
    //        new LatLng(25.3303729706, 55.6835174561));

    public PlanFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_plan, container, false);
        mAttractions = view.findViewById(R.id.attractions_tv);
        mToilets = view.findViewById(R.id.toilets_tv);
        mRestaurants = view.findViewById(R.id.restaurants_tv);
        mStores = view.findViewById(R.id.stores_tv);
        mServices = view.findViewById(R.id.services_tv);
        View bottomSheet = view.findViewById(R.id.bottom_sheet);
        mPoiImage = view.findViewById(R.id.poi_image);
        mPoiTitle = view.findViewById(R.id.poi_title);
        mPoiDescription = view.findViewById(R.id.poi_description);
        mMapView = view.findViewById(R.id.map_layout);

        mImages.clear();

        mBottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
        mBottomSheetBehavior.setHideable(true);
        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_COLLAPSED) {


                    mBottomSheetBehavior.setPeekHeight(mMapView.getHeight());
                }
            }

            @Override
            public void onSlide(View bottomSheet, float slideOffset) {
            }
        });

        mAttractions.setOnClickListener(this);
        mToilets.setOnClickListener(this);
        mRestaurants.setOnClickListener(this);
        mStores.setOnClickListener(this);
        mServices.setOnClickListener(this);

        SupportMapFragment mapFragment =
                (SupportMapFragment) this.getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        myApp = ((BaseApplication) this.getActivity().getApplication());
        HomeActivity.toolbar.setNavigationIcon(R.drawable.back_arrow_ic);
        HomeActivity.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getFragmentManager() != null) {
                    Fragment currentFragment = getFragmentManager().findFragmentById(R.id.flContent);
                    if (currentFragment instanceof PlanFragment) {
                        navigateToHomeFragment();
                        deleteCache(getActivity());
                        closeFragment();

                        if (mGroundOverlay != null) {

                            mGroundOverlay.remove();
                            mGoogleMap.clear();
                        }


                    }
                } else {
                    HomeActivity.openDrawer();
                    deleteCache(getActivity());
                    if (mGroundOverlay != null) {

                        mGroundOverlay.remove();
                        mGoogleMap.clear();
                    }
                }

            }
        });
        if (getPreferences().getCarthageLandLocation().equals("CLHammamet")) {

            // CARTHAGELAND_HAMMAMET_BOUNDS = new LatLngBounds(new LatLng(36.367655, 10.534639),
            //        new LatLng(36.370324, 10.535036));
            CARTHAGELAND_HAMMAMET_BOUNDS = new LatLngBounds(new LatLng(36.368116, 10.533275),
                    new LatLng(36.370580, 10.536348));

            // CARTHAGELAND_HAMMAMET_BOUNDS = new LatLngBounds( new LatLng(36.369217, 10.536028),
            //       new LatLng(36.368872, 10.532788));

            //     CARTHAGELAND_TUNIS_BOUNDS = new LatLngBounds( new LatLng(36.831691, 10.224648),
            //           new LatLng(36.832266, 10.223670));

            LatLngBounds DUBAI_BOUNDS = new LatLngBounds(new LatLng(24.5908366068, 54.84375),
                    new LatLng(36.368390, 10.534752));
            NEWARK = new LatLng(36.368869, 10.533096);
            NEWARK_LEFT = new LatLng(36.370605, 10.534652);
            NEWARK_NEAR_LEFT = new LatLng(36.370236, 10.535367);
            NEWARK_RIGHT = new LatLng(36.369136, 10.536489);
            NEWARK_NEAR_RIGHT = new LatLng(36.368805, 10.535985);
            NEWARK_NEAR_RIGHT1 = new LatLng(36.368194, 10.535579);
            NEWARK_ALIBABA = new LatLng(36.368201, 10.534688);
            NEWARK_STELLA = new LatLng(36.368541, 10.533252);

            /*NEWARK = new LatLng(36.368725, 10.532607);
            NEWARK_LEFT = new LatLng(36.370305, 10.534751);
            NEWARK_NEAR_LEFT = new LatLng(36.370236, 10.535367);
            NEWARK_RIGHT = new LatLng(36.370236, 10.535367);
            NEWARK_NEAR_RIGHT = new LatLng(36.368805, 10.535985);
            NEWARK_NEAR_RIGHT1 = new LatLng(36.367986, 10.535348);
            NEWARK_ALIBABA = new LatLng(36.368234, 10.534522);
            NEWARK_STELLA = new LatLng(36.368674, 10.532939);*/


        } else {

            CARTHAGELAND_HAMMAMET_BOUNDS = new LatLngBounds(new LatLng(36.368116, 10.533275),
                    new LatLng(36.370580, 10.536348));

            //CARTHAGELAND_HAMMAMET_BOUNDS = new LatLngBounds(new LatLng(36.367655, 10.534639),
            new LatLng(36.370324, 10.535036);
            // CARTHAGELAND_HAMMAMET_BOUNDS = new LatLngBounds( new LatLng(36.369217, 10.536028),
            //       new LatLng(36.368872, 10.532788));

            CARTHAGELAND_TUNIS_BOUNDS = new LatLngBounds(new LatLng(36.830886, 10.223740),
                    new LatLng(36.831955, 10.225627));
            NEWARK = new LatLng(36.831691, 10.224648);
            NEWARK_LEFT = new LatLng(36.832266, 10.223670);
            NEWARK_NEAR_LEFT = new LatLng(36.832259, 10.225219);
            NEWARK_RIGHT = new LatLng(36.832251, 10.225262);
            NEWARK_NEAR_RIGHT = new LatLng(36.832140, 10.225373);
            //NEWARK_NEAR_RIGHT1 = new LatLng(36.831136, 10.225757);
            //NEWARK_ALIBABA = new LatLng(36.831061, 10.223701);
            NEWARK_NEAR_RIGHT1 = new LatLng(36.831171, 10.225969);
            NEWARK_ALIBABA = new LatLng(36.831055, 10.223682);
            NEWARK_STELLA = new LatLng(36.831668, 10.223159);
            //NEWARK_BOTTOM_LEFT = new LatLng(36.831687, 10.223146);
        }
        NEAR_NEWARK = new LatLng(NEWARK.latitude - 0.001, NEWARK.longitude - 0.025);


        Log.e("Plan ", getPreferences().getCarthageLandLocation());
        Toolbar toolbarTop = (Toolbar) getActivity().findViewById(R.id.toolbar);
        TextView mTitle = (TextView) toolbarTop.findViewById(R.id.toolbar_title);
        mTitle.setText("");

        return view;
    }

    public static void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                Log.e("PLANFRAGMENT TAG", "App cache is cleared");
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

    private void closeFragment() {
        getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
        mImages.clear();

    }

    public void fragmentreplace(Class fragmentClass, String tag) throws IllegalAccessException, java.lang.InstantiationException {
        FragmentManager fragmentManager = getFragmentManager();
        Fragment fragment = (Fragment) fragmentClass.newInstance();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.flContent, fragment, tag);
        fragmentTransaction.addToBackStack(null); //this will add it to back stack
        fragmentTransaction.commit();
    }

    private void navigateToHomeFragment() {
        Fragment fragment = null;
        try {
            closeFragment();
            //fragment = HomeFragment.class.newInstance();

        } catch (Exception e) {
            e.printStackTrace();
        }

        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        //fragmentManager.beginTransaction().replace(R.id.flContent, fragment).addToBackStack("tag").commit();
        fragmentManager.popBackStack();
    }

    public void navigateToHomeFragment(String tag) throws IllegalAccessException, java.lang.InstantiationException {
        FragmentManager fragmentManager = getFragmentManager();
        Fragment fragment = HomeFragment.class.newInstance();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.flContent, fragment, tag);
        fragmentTransaction.addToBackStack(null); //this will add it to back stack
        fragmentTransaction.commit();
    }

    @Override
    public void onMapReady(GoogleMap map) {
        // Register a listener to respond to clicks on GroundOverlays.

        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(NEWARK_LEFT);
        builder.include(NEWARK_NEAR_LEFT);
        builder.include(NEWARK_RIGHT);
        builder.include(NEWARK_NEAR_RIGHT);
        builder.include(NEWARK_NEAR_RIGHT1);
        builder.include(NEWARK_ALIBABA);
        builder.include(NEWARK_STELLA);

        if (getPreferences().getCarthageLandLocation().equals("CLTunis")) {
            // builder.include(NEWARK_BOTTOM_LEFT);
        }
        LatLngBounds bounds = builder.build();

        LatLngBounds tmpBounds = builder.build();
        LatLng center = tmpBounds.getCenter();
        LatLng northEast = move(center, 709, 709);
        LatLng southWest = move(center, -709, -709);
        builder.include(southWest);
        builder.include(northEast);

        int padding = 0; // offset from edges of the ic_map in pixels
        CameraUpdate cu = CameraUpdateFactory.newLatLngZoom(bounds.getCenter(), 18);
        mOriginalZoom = tmpBounds.getCenter();
        map.setOnMapClickListener(this);
        map.setOnGroundOverlayClickListener(this);
        map.moveCamera(cu);

        mGoogleMap = map;
        mGoogleMap.setOnInfoWindowClickListener(this);
       /* mGoogleMap.setLatLngBoundsForCameraTarget(CARTHAGELAND_HAMMAMET_BOUNDS);
        mGoogleMap.setMinZoomPreference(18);
        mGoogleMap.setMaxZoomPreference(25);*/
        showSelectedMarkers(getResources().getString(R.string.attraction), mGoogleMap);
        //   ic_map.moveCamera(CameraUpdateFactory.newLatLngZoom(NEWARK, 20));
        /*map.addPolyline(new PolylineOptions()
                .add(NEWARK_LEFT,
                        NEWARK_NEAR_LEFT,NEWARK_RIGHT,NEWARK_NEAR_RIGHT,NEWARK_NEAR_RIGHT1,NEWARK_ALIBABA,NEWARK_LEFT)
                .width(15).color(Color.RED).geodesic(true));*/
        BitmapDescriptor imageDescriptor;


        if (getPreferences().getCarthageLandLocation().equals("CLTunis")) {

            for (int i = 0; i < getPreferences().getTunisPois().size(); i++) {
                ArrayList<POI> poiList = getPreferences().getTunisPois();
                if (poiList != null) {
                    {
                        POI poiItem = poiList.get(i);
                        float poiLatitude, poiLongitude;
                        poiLatitude = poiItem.getLat();
                        poiLongitude = poiItem.getLng();
                        mPoisList.add(poiItem);

                        switch (poiItem.getCategory().getName()) {
                            case Constants.Categories.attractions_filter: {
                                Marker marker = map.addMarker(new MarkerOptions()
                                        .position(new LatLng(poiLatitude, poiLongitude))
                                        .icon(BitmapDescriptorFactory.fromResource(R.mipmap.game_ic))
                                        .title(poiItem.getName().toString())
                                        .snippet(getString(R.string.attraction)));
                                mMarkersList.add(marker);
                                break;
                            }
                            case Constants.Categories.toilets_filter: {
                                Marker marker = map.addMarker(new MarkerOptions()
                                        .position(new LatLng(poiLatitude, poiLongitude))
                                        .icon(BitmapDescriptorFactory.fromResource(R.mipmap.wc_ic))
                                        .title(poiItem.getName().toString())
                                        .snippet(getString(R.string.toilets)));
                                mMarkersList.add(marker);
                                break;
                            }
                            case Constants.Categories.restaurants_filter: {
                                Marker marker = map.addMarker(new MarkerOptions()
                                        .position(new LatLng(poiLatitude, poiLongitude))
                                        .icon(BitmapDescriptorFactory.fromResource(R.mipmap.restaurant_ic))
                                        .title(poiItem.getName().toString())
                                        .snippet(getString(R.string.restaurants)));
                                mMarkersList.add(marker);
                                break;
                            }
                            case Constants.Categories.kiosque_filter: {
                                Marker marker = map.addMarker(new MarkerOptions()
                                        .position(new LatLng(poiLatitude, poiLongitude))
                                        .icon(BitmapDescriptorFactory.fromResource(R.mipmap.kiosk_ic))
                                        .title(poiItem.getName().toString())
                                        .snippet(getString(R.string.kiosques)));
                                mMarkersList.add(marker);
                                break;
                            }
                            case Constants.Categories.counters_filter: {
                                Marker marker = map.addMarker(new MarkerOptions()
                                        .position(new LatLng(poiLatitude, poiLongitude))
                                        .icon(BitmapDescriptorFactory.fromResource(R.mipmap.counter_ic))
                                        .title(poiItem.getName().toString())
                                        .snippet(getString(R.string.counters)));
                                mMarkersList.add(marker);
                                break;
                            }
                            case Constants.Categories.basics_filter: {
                                Marker marker = map.addMarker(new MarkerOptions()
                                        .position(new LatLng(poiLatitude, poiLongitude))
                                        .icon(BitmapDescriptorFactory.fromResource(R.mipmap.game_ic))
                                        .title(poiItem.getName().toString())
                                        .snippet(getString(R.string.basics)));
                                mMarkersList.add(marker);
                                break;
                            }
                            default:
                                Log.e("TAG", "the marker has not been created");
                                break;
                        }

                    }
                }
            }
           /* map.addPolyline(new PolylineOptions()
                    .add(NEWARK_LEFT,
                            NEWARK_NEAR_LEFT,NEWARK_RIGHT,NEWARK_NEAR_RIGHT,NEWARK_NEAR_RIGHT1,NEWARK_ALIBABA,NEWARK_STELLA,NEWARK_LEFT)
                    .width(15).color(Color.RED).geodesic(true));*/
            /*map.addMarker(new MarkerOptions()
                    .position(new LatLng(36.831768, 10.224689))
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.game_ic))
                    .title("Carthage Land")
                    .snippet("Population: 776733"));
            Marker marker1 = map.addMarker(new MarkerOptions()
                    .position(new LatLng(36.832123, 10.223888))
                    .title("Bumper cars")
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.game_ic))
                    .snippet(getString(R.string.attraction)));
            Marker marker2 = map.addMarker(new MarkerOptions()
                    .position(new LatLng(36.832103, 10.224187))
                    .title("Pirate Jet")
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.game_ic))
                    .snippet(getString(R.string.attraction)));
            Marker marker3 = map.addMarker(new MarkerOptions()
                    .position(new LatLng(36.832103, 10.224349))
                    .title("Trampoline Cage")
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.game_ic))
                    .snippet(getString(R.string.attraction)));
            Marker marker4 = map.addMarker(new MarkerOptions()
                    .position(new LatLng(36.832019, 10.224394))
                    .title("Choco cups")
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.game_ic))
                    .snippet(getString(R.string.attraction)));
            Marker marker5 = map.addMarker(new MarkerOptions()
                    .position(new LatLng(36.832207, 10.224685))
                    .title("Mixtrême")
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.game_ic))
                    .snippet(getString(R.string.attraction)));
            Marker marker6 = map.addMarker(new MarkerOptions()
                    .position(new LatLng(36.832009, 10.224777))
                    .title("Pirate Train")
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.game_ic))
                    .snippet(getString(R.string.attraction)));
            Marker marker7 = map.addMarker(new MarkerOptions()
                    .position(new LatLng(36.832208, 10.224833))
                    .title("Orca")
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.game_ic))
                    .snippet(getString(R.string.attraction)));
            Marker marker8 = map.addMarker(new MarkerOptions()
                    .position(new LatLng(36.832205, 10.224994))
                    .title("Colombus")
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.game_ic))
                    .snippet(getString(R.string.attraction)));
            Marker marker9 = map.addMarker(new MarkerOptions()
                    .position(new LatLng(36.832145, 10.225213))
                    .title("Mini Dance Party")
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.game_ic))
                    .snippet(getString(R.string.attraction)));
            Marker marker10 = map.addMarker(new MarkerOptions()
                    .position(new LatLng(36.831760, 10.225323))
                    .title("Drop & Twist")
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.game_ic))
                    .snippet(getString(R.string.attraction)));
            Marker marker11 = map.addMarker(new MarkerOptions()
                    .position(new LatLng(36.831926, 10.223758))
                    .title("Caroussel")
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.game_ic))
                    .snippet(getString(R.string.attraction)));
            Marker marker12 = map.addMarker(new MarkerOptions()
                    .position(new LatLng(36.831628, 10.223431))
                    .title("Fun Ball Battle")
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.game_ic))
                    .snippet(getString(R.string.attraction)));
            Marker marker13 = map.addMarker(new MarkerOptions()
                    .position(new LatLng(36.831340, 10.225675))
                    .title("Multi Slide & Kamikaze")
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.game_ic))
                    .snippet(getString(R.string.attraction)));
            Marker marker14 = map.addMarker(new MarkerOptions()
                    .position(new LatLng(36.831273, 10.225301))
                    .title("Rafting Slide")
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.game_ic))
                    .snippet(getString(R.string.attraction)));
            Marker marker15 = map.addMarker(new MarkerOptions()
                    .position(new LatLng(36.831671, 10.224587))
                    .title("Aqua Tower")
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.game_ic))
                    .snippet(getString(R.string.attraction)));
            Marker marker16 = map.addMarker(new MarkerOptions()
                    .position(new LatLng(36.831870, 10.224037))
                    .title("Lazy River")
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.game_ic))
                    .snippet(getString(R.string.attraction)));
            Marker marker17 = map.addMarker(new MarkerOptions()
                    .position(new LatLng(36.831324, 10.223933))
                    .title("Piscine à vagues")
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.game_ic))
                    .snippet(getString(R.string.attraction)));
            Marker marker18 = map.addMarker(new MarkerOptions()
                    .position(new LatLng(36.831311, 10.225425))
                    .title("Turbo lance")
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.game_ic))
                    .snippet(getString(R.string.attraction)));
            Marker marker19 = map.addMarker(new MarkerOptions()
                    .position(new LatLng(36.831199, 10.224441))
                    .title("Piscine familiale")
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.game_ic))
                    .snippet(getString(R.string.attraction)));
            Marker marker20 = map.addMarker(new MarkerOptions()
                    .position(new LatLng(36.831561, 10.225040))
                    .title("Space Hole")
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.game_ic))
                    .snippet(getString(R.string.attraction)));
            Marker marker21 = map.addMarker(new MarkerOptions()
                    .position(new LatLng(36.831316, 10.223762))
                    .title("Piscine ronde")
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.game_ic))
                    .snippet(getString(R.string.attraction)));
            Marker marker22 = map.addMarker(new MarkerOptions()
                    .position(new LatLng(36.831238, 10.225236))
                    .title("Big Hole")
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.game_ic))
                    .snippet(getString(R.string.attraction)));
            Marker marker23 = map.addMarker(new MarkerOptions()
                    .position(new LatLng(36.831729, 10.224337))
                    .title("Kids pools")
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.game_ic))
                    .snippet(getString(R.string.attraction)));
            Marker marker24 = map.addMarker(new MarkerOptions()
                    .position(new LatLng(36.831556, 10.223603))
                    .title("Compact slide & Aquatube")
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.game_ic))
                    .snippet(getString(R.string.attraction)));
            Marker marker25 = map.addMarker(new MarkerOptions()
                    .position(new LatLng(36.831642, 10.223532))
                    .title("Racer").
                            icon(BitmapDescriptorFactory.fromResource(R.mipmap.game_ic))
                    .snippet(getString(R.string.attraction)));
            Marker marker26 = map.addMarker(new MarkerOptions()
                    .position(new LatLng(36.831684, 10.223490))
                    .title("Dragero")
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.game_ic))
                    .snippet(getString(R.string.attraction)));
            Marker marker27 = map.addMarker(new MarkerOptions()
                    .position(new LatLng(36.831714, 10.223426))
                    .title("Wave Slide")
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.game_ic))
                    .snippet(getString(R.string.attraction)));

            Marker marker28 = map.addMarker(new MarkerOptions()
                    .position(new LatLng(36.832115, 10.223607))
                    .title("Entrée")
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.game_ic))
                    .snippet(getString(R.string.basics)));

            Marker marker29 = map.addMarker(new MarkerOptions()
                    .position(new LatLng(36.832069, 10.223741))
                    .title("Infirmerie")
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.medic_ic))
                    .snippet(getString(R.string.basics)));
            Marker marker30 = map.addMarker(new MarkerOptions()
                    .position(new LatLng(36.831869, 10.223644))
                    .title("Drug Store")
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.kiosk_ic))
                    .snippet(getString(R.string.kiosques)));
            Marker marker31 = map.addMarker(new MarkerOptions()
                    .position(new LatLng(36.831960, 10.225216))
                    .title("WC")
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.wc_ic))
                    .snippet(getString(R.string.toilets)));
            Marker marker32 = map.addMarker(new MarkerOptions()
                    .position(new LatLng(36.832078, 10.224097))
                    .title("WC")
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.wc_ic))
                    .snippet(getString(R.string.toilets)));
            Marker marker33 = map.addMarker(new MarkerOptions()
                    .position(new LatLng(36.831963, 10.223820))
                    .title("Kiosque glaces")
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.kiosk_ic))
                    .snippet(getString(R.string.kiosques)));
            Marker marker34 = map.addMarker(new MarkerOptions()
                    .position(new LatLng(36.831988, 10.223872))
                    .title("Buvette")
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.restaurant_ic))
                    .snippet(getString(R.string.restaurants)));
            Marker marker35 = map.addMarker(new MarkerOptions()
                    .position(new LatLng(36.831783, 10.223948))
                    .title("Buvette")
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.restaurant_ic))
                    .snippet(getString(R.string.restaurants)));
            Marker marker36 = map.addMarker(new MarkerOptions()
                    .position(new LatLng(36.831806, 10.224128))
                    .title("Buvette")
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.restaurant_ic))
                    .snippet(getString(R.string.restaurants)));
            Marker marker37 = map.addMarker(new MarkerOptions()
                    .position(new LatLng(36.832174, 10.223665))
                    .title("Guichet")
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.counter_ic))
                    .snippet(getString(R.string.counters)));
            Marker marker38 = map.addMarker(new MarkerOptions()
                    .position(new LatLng(36.831144, 10.225042))
                    .title("Guichet")
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.counter_ic))
                    .snippet(getString(R.string.counters)));
            Marker marker39 = map.addMarker(new MarkerOptions()
                    .position(new LatLng(36.831617, 10.225341))
                    .title("Restaurant")
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.counter_ic))
                    .snippet(getString(R.string.restaurants)));*/

            //addCLTunisMarkers(marker1, marker2, marker3, marker4, marker5, marker6, marker7, marker8, marker9, marker10, marker11, marker12, marker13, marker14, marker15, marker16, marker17, marker18, marker19, marker20, marker21, marker22, marker23, marker24, marker25, marker26, marker27, marker28, marker29, marker30, marker31, marker32, marker33, marker34, marker35, marker36, marker37, marker38, marker39);
            hideMarkers();
            showSelectedMarkers(getResources().getString(R.string.basics), null);
            showSelectedMarkers(getResources().getString(R.string.attraction), null);
            MapStyleOptions style = MapStyleOptions.loadRawResourceStyle(getActivity(), R.raw.style);
            mGoogleMap.setMapStyle(style);
            mGoogleMap.setOnMarkerClickListener(this);
            mGoogleMap.setInfoWindowAdapter(new MyInfoWindowAdapter());
            mGoogleMap.setLatLngBoundsForCameraTarget(CARTHAGELAND_TUNIS_BOUNDS);
            mImages.clear();
            try {

                // mImages.add(bitmapDescriptor);

                //   mImages.add(BitmapDescriptorFactory.fromResource(R.drawable.newark_prudential_sunny));
                //imageDescriptor = BitmapDescriptorFactory.fromResource(R.drawable.plan_cl_tunis_test_1);

                /*getTunisBitmapDimensions();
                tunisBitmap = decodeSampledBitmapFromResource(getResources(), R.drawable.plan_cl_tunis_test_1, 300, 300);
                BitmapDescriptor bitmapDescriptor = BitmapDescriptorFactory.fromBitmap(tunisBitmap);
                mGroundOverlay = map.addGroundOverlay(new GroundOverlayOptions()
                        .image(bitmapDescriptor)
                        .positionFromBounds(tmpBounds)

                        .clickable(true));*/
                loadTunisImage(getActivity(), map, bounds);
                // tunisBitmap.recycle();
                // tunisBitmap = null;
                mImages.clear();
            } catch (Exception e) {
                e.printStackTrace();
            }


        } else {
            /*map.addPolyline(new PolylineOptions()
                    .add(NEWARK_LEFT,
                            NEWARK_NEAR_LEFT, NEWARK_RIGHT, NEWARK_NEAR_RIGHT, NEWARK_NEAR_RIGHT1, NEWARK_ALIBABA, NEWARK_STELLA, NEWARK_LEFT)
                    .width(15).color(Color.RED).geodesic(true));*/
            setCLHammametMarkers(map, tmpBounds);
        }


        // Add a small, rotated overlay that is clickable by default
        // (set by the initial state of the checkbox.)


        // Add a large overlay at Newark on top of the smaller overlay.


        //   mGroundOverlay = ic_map.addGroundOverlay(new GroundOverlayOptions()
        //       .image(mImages.get(mCurrentEntry)).anchor(0, 1)
        //       .position(NEWARK, 8600f, 6500f));


        // Override the default content description on the view, for accessibility mode.
        // Ideally this string would be localised.
        map.setContentDescription("Google Map with ground overlay.");
    }

    private void getTunisBitmapDimensions() {

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        int imageHeight = options.outHeight;
        int imageWidth = options.outWidth;
        String imageType = options.outMimeType;
        Log.e("TAG", "the bitmap dimensions are " + imageWidth + " " + imageHeight + " " + imageType);
    }

    private void getHammemetBitmapDimensions() {

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        int imageHeight = options.outHeight;
        int imageWidth = options.outWidth;
        String imageType = options.outMimeType;
        Log.e("TAG", "the bitmap dimensions are " + imageWidth + " " + imageHeight + " " + imageType);
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                         int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }


    private void closefragment() {

        mImages.clear();
        getFragmentManager().popBackStack();
    }


    private static final double EARTHRADIUS = 6366198;

    /**
     * Create a new LatLng which lies toNorth meters north and toEast meters
     * east of startLL
     */
    private static LatLng move(LatLng startLL, double toNorth, double toEast) {
        double lonDiff = meterToLongitude(toEast, startLL.latitude);
        double latDiff = meterToLatitude(toNorth);
        return new LatLng(startLL.latitude + latDiff, startLL.longitude
                + lonDiff);
    }

    private static double meterToLongitude(double meterToEast, double latitude) {
        double latArc = Math.toRadians(latitude);
        double radius = Math.cos(latArc) * EARTHRADIUS;
        double rad = meterToEast / radius;
        return Math.toDegrees(rad);
    }

    private void setZoom(GoogleMap map) {
       /* Projection projection = map.getProjection();
         LatLng markerPosition = marker.getPosition();
        Point markerPoint = projection.toScreenLocation(markerPosition);
       Point targetPoint = new Point(markerPoint.x, markerPoint.y - getView().getHeight() / 2);
        LatLng targetPosition = projection.fromScreenLocation(targetPoint);
        map.animateCamera(CameraUpdateFactory.newLatLng(targetPosition), 1000, null);*/
    }

    private static double meterToLatitude(double meterToNorth) {
        double rad = meterToNorth / EARTHRADIUS;
        return Math.toDegrees(rad);
    }


    private void setCLHammametMarkers(GoogleMap map, LatLngBounds bounds) {


        for (int i = 0; i < getPreferences().getHammametPois().size(); i++) {
            ArrayList<POI> poiList = getPreferences().getHammametPois();
            if (poiList != null) {
                {
                    POI poiItem = poiList.get(i);
                    float poiLatitude, poiLongitude;
                    poiLatitude = poiItem.getLat();
                    poiLongitude = poiItem.getLng();
                    mPoisList.add(poiItem);

                    switch (poiItem.getCategory().getName()) {
                        case Constants.Categories.attractions_filter: {
                            Marker marker = map.addMarker(new MarkerOptions()
                                    .position(new LatLng(poiLatitude, poiLongitude))
                                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.game_ic))
                                    .title(poiItem.getName().toString())
                                    .snippet(getString(R.string.attraction)));
                            mMarkersList.add(marker);
                            break;
                        }
                        case Constants.Categories.toilets_filter: {
                            Marker marker = map.addMarker(new MarkerOptions()
                                    .position(new LatLng(poiLatitude, poiLongitude))
                                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.wc_ic))
                                    .title(poiItem.getName().toString())
                                    .snippet(getString(R.string.toilets)));
                            mMarkersList.add(marker);
                            break;
                        }
                        case Constants.Categories.restaurants_filter: {
                            Marker marker = map.addMarker(new MarkerOptions()
                                    .position(new LatLng(poiLatitude, poiLongitude))
                                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.restaurant_ic))
                                    .title(poiItem.getName().toString())
                                    .snippet(getString(R.string.restaurants)));
                            mMarkersList.add(marker);
                            break;
                        }
                        case Constants.Categories.kiosque_filter: {
                            Marker marker = map.addMarker(new MarkerOptions()
                                    .position(new LatLng(poiLatitude, poiLongitude))
                                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.kiosk_ic))
                                    .title(poiItem.getName().toString())
                                    .snippet(getString(R.string.kiosques)));
                            mMarkersList.add(marker);
                            break;
                        }
                        case Constants.Categories.counters_filter: {
                            Marker marker = map.addMarker(new MarkerOptions()
                                    .position(new LatLng(poiLatitude, poiLongitude))
                                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.counter_ic))
                                    .title(poiItem.getName().toString())
                                    .snippet(getString(R.string.counters)));
                            mMarkersList.add(marker);
                            break;
                        }
                        case Constants.Categories.basics_filter: {
                            Marker marker = map.addMarker(new MarkerOptions()
                                    .position(new LatLng(poiLatitude, poiLongitude))
                                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.game_ic))
                                    .title(poiItem.getName().toString())
                                    .snippet(getString(R.string.basics)));
                            mMarkersList.add(marker);
                            break;
                        }
                        default:
                            Log.e("TAG", "the marker has not been created");
                            break;
                    }

                }
            }
        }
        BitmapDescriptor imageDescriptor;
        /*map.addMarker(new MarkerOptions()
                .position(new LatLng(36.369423, 10.534972))

                .title("Carthage Land")
                .snippet("Population: 776733"));
        map.addMarker(new MarkerOptions()
                .position(new LatLng(36.368810, 10.534763))
                .title("Flume ride")
                .snippet(""));
        map.addMarker(new MarkerOptions()
                .position(new LatLng(36.368854, 10.535044))
                .title("Le lézard rouge")
                .snippet(""));
        map.addMarker(new MarkerOptions()
                .position(new LatLng(36.368443, 10.534537))
                .title("Carrousel")
                .snippet(""));
        map.addMarker(new MarkerOptions()
                .position(new LatLng(36.368583, 10.534808))
                .title("Safari africain")
                .snippet(""));
        map.addMarker(new MarkerOptions()
                .position(new LatLng(36.369181, 10.536116))
                .title("Le grand momoch")
                .snippet(""));
        map.addMarker(new MarkerOptions()
                .position(new LatLng(36.369181, 10.536116))
                .title("Le trésor de Carthage")
                .snippet(""));
        map.addMarker(new MarkerOptions()
                .position(new LatLng(36.369181, 10.536116))
                .title("Les jardins d'Amilcar")
                .snippet(""));
        map.addMarker(new MarkerOptions()
                .position(new LatLng(36.369181, 10.536116))
                .title("Le Dragon")
                .snippet(""));
        map.addMarker(new MarkerOptions()
                .position(new LatLng(36.368580, 10.535219))
                .title("La marche d'Hannibal: les éléphants")
                .snippet(""));
        map.addMarker(new MarkerOptions()
                .position(new LatLng(36.369181, 10.536116))
                .title("Le temple de Baal: bateau pirate")
                .snippet(""));
        map.addMarker(new MarkerOptions()
                .position(new LatLng(36.369181, 10.536116))
                .title("Le périple d'Hannon: la rivière rapide")
                .snippet(""));
        map.addMarker(new MarkerOptions()
                .position(new LatLng(36.369181, 10.536116))
                .title("La ville perdue")
                .snippet(""));
        map.addMarker(new MarkerOptions()
                .position(new LatLng(36.369181, 10.536116))
                .title("Fun Ball Battle")
                .snippet(""));
        map.addMarker(new MarkerOptions()
                .position(new LatLng(36.369181, 10.536116))
                .title("Jeux d'aventure")
                .snippet(""));
        map.addMarker(new MarkerOptions()
                .position(new LatLng(36.369181, 10.536116))
                .title("La danse des amulettes: Music Express")
                .snippet(""));
        map.addMarker(new MarkerOptions()
                .position(new LatLng(36.369181, 10.536116))
                .title("Mini zoo")
                .snippet(""));
        map.addMarker(new MarkerOptions()
                .position(new LatLng(36.369181, 10.536116))
                .title("Jeux gonflables")
                .snippet(""));
        map.addMarker(new MarkerOptions()
                .position(new LatLng(36.369181, 10.536116))
                .title("Mind Blaster")
                .snippet(""));
        map.addMarker(new MarkerOptions()
                .position(new LatLng(36.368666, 10.533309))
                .title("Piscine à vagues")
                .snippet(""));
        map.addMarker(new MarkerOptions()
                .position(new LatLng(36.368869, 10.532950))
                .title("Kid's pool")
                .snippet(""));
        map.addMarker(new MarkerOptions()
                .position(new LatLng(36.369124, 10.533443))
                .title("Carthage Tower")
                .snippet(""));
        map.addMarker(new MarkerOptions()
                .position(new LatLng(36.369147, 10.533909))
                .title("Body Slides")
                .snippet(""));
        map.addMarker(new MarkerOptions()
                .position(new LatLng(36.368976, 10.533092))
                .title("Multi Slides")
                .snippet(""));
        map.addMarker(new MarkerOptions()
                .position(new LatLng(36.369528, 10.533788))
                .title("Swimming pool")
                .snippet(""));*/

        hideMarkers();
        showSelectedMarkers(getResources().getString(R.string.attraction), null);

        showSelectedMarkers(getResources().getString(R.string.attraction), null);
        mGoogleMap.setOnMarkerClickListener(this);
        mGoogleMap.setLatLngBoundsForCameraTarget(CARTHAGELAND_HAMMAMET_BOUNDS);
        MapStyleOptions style = MapStyleOptions.loadRawResourceStyle(getActivity(), R.raw.style);
        mGoogleMap.setMapStyle(style);
        mGoogleMap.setInfoWindowAdapter(new MyInfoWindowAdapter());
        mImages.clear();


        try {
            getHammemetBitmapDimensions();

            // mImages.add(bitmapDescriptor);
            //   mImages.add(BitmapDescriptorFactory.fromResource(R.drawable.newark_prudential_sunny));
            //imageDescriptor = BitmapDescriptorFactory.fromResource(R.drawable.cl_hammamet_mp);
           /* hammametBitmap = decodeSampledBitmapFromResource(getResources(), R.drawable.cl_hammamet_mp, 300, 300);
            BitmapDescriptor bitmapDescriptor = BitmapDescriptorFactory.fromBitmap(hammametBitmap);
            mGroundOverlay = map.addGroundOverlay(new GroundOverlayOptions()
                    .image(bitmapDescriptor)
                    .positionFromBounds(bounds)
                    .clickable(true));*/
            loadHammametImage(getActivity(), map, bounds);

            // hammametBitmap.recycle();
            // hammametBitmap = null;
            mImages.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void loadHammametImage(Context context, final GoogleMap map, final LatLngBounds bounds) {


        mTarget = new Target() {
            @Override
            public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                //Do something
                // hammametBitmap = decodeSampledBitmapFromResource(getResources(), R.drawable.cl_hammamet_mp, 300, 300);
                BitmapDescriptor bitmapDescriptor = BitmapDescriptorFactory.fromBitmap(bitmap);
                // mImages.add(bitmapDescriptor);
                //   mImages.add(BitmapDescriptorFactory.fromResource(R.drawable.newark_prudential_sunny));
                //imageDescriptor = BitmapDescriptorFactory.fromResource(R.drawable.cl_hammamet_mp);
                mGroundOverlay = map.addGroundOverlay(new GroundOverlayOptions()
                        .image(bitmapDescriptor)
                        .positionFromBounds(bounds)
                        .clickable(true));
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        };

        Picasso.with(context)
                .load(R.drawable.cl_hammamet_mp).resize(500, 500).memoryPolicy(MemoryPolicy.NO_STORE)
                .into(mTarget);
    }

    void loadTunisImage(Context context, final GoogleMap map, final LatLngBounds bounds) {


        mTarget = new Target() {
            @Override
            public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                //Do something
                // tunisBitmap = decodeSampledBitmapFromResource(getResources(), R.drawable.plan_cl_tunis_test_1, 300, 300);
                BitmapDescriptor bitmapDescriptor = BitmapDescriptorFactory.fromBitmap(bitmap);
                // mImages.add(bitmapDescriptor);
                //   mImages.add(BitmapDescriptorFactory.fromResource(R.drawable.newark_prudential_sunny));
                //imageDescriptor = BitmapDescriptorFactory.fromResource(R.drawable.cl_hammamet_mp);
                mGroundOverlay = map.addGroundOverlay(new GroundOverlayOptions()
                        .image(bitmapDescriptor)
                        .positionFromBounds(bounds)
                        .clickable(true));
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        };

        Picasso.with(context)
                .load(R.drawable.plan_cl_tunis_test_1).resize(500, 500).memoryPolicy(MemoryPolicy.NO_STORE)
                .into(mTarget);
    }

    private void showSelectedMarkers(String markerCategory, @Nullable GoogleMap map) {

        if (mMarkersList != null) {
            for (int i = 0; i < mMarkersList.size(); i++) {
                if (markerCategory.equals(mMarkersList.get(i).getSnippet().toString())) {
                    mMarkersList.get(i).setVisible(true);
                } else {
                    mMarkersList.get(i).setVisible(false);
                }
            }
        }
    }

    private void hideMarkers() {

        if (mMarkersList != null)
            for (int i = 0; i < mMarkersList.size(); i++) {
                mMarkersList.get(i).setVisible(false);

            }
    }

    private void addCLTunisMarkers(Marker marker1, Marker marker2, Marker marker3, Marker marker4, Marker marker5, Marker marker6, Marker marker7, Marker marker8, Marker marker9, Marker marker10, Marker marker11, Marker marker12, Marker marker13, Marker marker14, Marker marker15, Marker marker16, Marker marker17, Marker marker18, Marker marker19, Marker marker20, Marker marker21, Marker marker22, Marker marker23, Marker marker24, Marker marker25, Marker marker26, Marker marker27, Marker marker28, Marker marker29, Marker marker30, Marker marker31, Marker marker32, Marker marker33, Marker marker34, Marker marker35, Marker marker36, Marker marker37, Marker marker38, Marker marker39) {
        mMarkersList.add(marker1);
        mMarkersList.add(marker2);
        mMarkersList.add(marker3);
        mMarkersList.add(marker4);
        mMarkersList.add(marker5);
        mMarkersList.add(marker6);
        mMarkersList.add(marker7);
        mMarkersList.add(marker8);
        mMarkersList.add(marker9);
        mMarkersList.add(marker10);
        mMarkersList.add(marker11);
        mMarkersList.add(marker12);
        mMarkersList.add(marker13);
        mMarkersList.add(marker14);
        mMarkersList.add(marker15);
        mMarkersList.add(marker16);
        mMarkersList.add(marker17);
        mMarkersList.add(marker18);
        mMarkersList.add(marker19);
        mMarkersList.add(marker20);
        mMarkersList.add(marker21);
        mMarkersList.add(marker22);
        mMarkersList.add(marker23);
        mMarkersList.add(marker24);
        mMarkersList.add(marker25);
        mMarkersList.add(marker26);
        mMarkersList.add(marker27);
        mMarkersList.add(marker28);
        mMarkersList.add(marker29);
        mMarkersList.add(marker30);
        mMarkersList.add(marker31);
        mMarkersList.add(marker32);
        mMarkersList.add(marker33);
        mMarkersList.add(marker34);
        mMarkersList.add(marker35);
        mMarkersList.add(marker36);
        mMarkersList.add(marker37);
        mMarkersList.add(marker38);
        mMarkersList.add(marker39);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.attractions_tv:
                showSelectedMarkers(getString(R.string.attraction), null);
                mGoogleMap.setInfoWindowAdapter(new MyInfoWindowAdapter());
                break;
            case R.id.toilets_tv:
                showSelectedMarkers(getString(R.string.toilets), null);
                mGoogleMap.setInfoWindowAdapter(new MyInfoWindowAdapter());
                break;
            case R.id.stores_tv:
                showSelectedMarkers(getString(R.string.kiosques), null);
                mGoogleMap.setInfoWindowAdapter(new MyInfoWindowAdapter());
                break;
            case R.id.restaurants_tv:
                showSelectedMarkers(getString(R.string.restaurants), null);
                mGoogleMap.setInfoWindowAdapter(new MyInfoWindowAdapter());
                break;
            case R.id.services_tv:
                showSelectedMarkers(getString(R.string.counters), null);
                mGoogleMap.setInfoWindowAdapter(new MyInfoWindowAdapter());
                break;
            default:
                break;
        }

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        LatLng latLng = marker.getPosition(); // whatever
        float zoom = 40;// whatever
        if (mGoogleMap != null) {
            // mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
            mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
            marker.showInfoWindow();

        }
        return true;
    }

    @Override
    public void onMapClick(LatLng latLng) {// whatever
        if (mGoogleMap != null) {
            // mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
            mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mOriginalZoom, 18));

        }
    }

    @Override
    public void onGroundOverlayClick(GroundOverlay groundOverlay) {
        if (mGoogleMap != null) {
            mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(groundOverlay.getPosition(), 18));

        }
    }

    @Override
    public void onInfoWindowClick(Marker marker) {

        if (mPoisList != null) {
            for (int i = 0; i < mPoisList.size(); i++) {
                if (!mPoisList.get(i).getCategory().getName().equals(Constants.Categories.restaurants_filter)) {
                    setBottomSheetData(marker, i);

                }


            }
        }

    }

    private void setBottomSheetData(Marker marker, int i) {
        if (marker.getTitle().toString().equals(mPoisList.get(i).getName().toString())) {
            if (mPoisList.get(i).getName() != null && mPoisList.get(i).getDescription() != null) {
                String poiName = mPoisList.get(i).getName();
                String poiDescription = mPoisList.get(i).getDescription();
                mPoiTitle.setText(poiName);
                mPoiDescription.setText(poiDescription);
                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
            if (mPoisList.get(i).getFilename() != null) {
                String poiImage = mPoisList.get(i).getFilename();
                Log.e("LOG","poiImage = "+poiImage);
                Picasso.with(getActivity())
                        .load(Constants.poiImmagesUrl + poiImage)
                        .placeholder(R.drawable.placeholder_attraction)
                        .centerInside().fit().into(mPoiImage);
            }

        }
    }

    class MyInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

        private final View myContentsView;

        MyInfoWindowAdapter() {
            myContentsView = initInfoWindowView();


            applyFonttoTextView(mTvTitle);
        }

        private View initInfoWindowView() {
            View myContentsView;
            myContentsView = getLayoutInflater().inflate(R.layout.custom_infowindow_layout, null);
            mTvTitle = (myContentsView.findViewById(R.id.marker_title));
            mTvDescription = (myContentsView.findViewById(R.id.marker_description));
            mMarkerImage = (myContentsView.findViewById(R.id.marker_image));
            return myContentsView;
        }

        @Override
        public View getInfoContents(Marker marker) {
            if (mPoisList != null) {
                for (int i = 0; i < mPoisList.size(); i++) {
                    setInfoWindowsData(marker, i);

                }
            }
            mTvTitle.setText(marker.getTitle());


            applyFonttoTextView(mTvTitle);
            return myContentsView;
        }

        @Override
        public View getInfoWindow(Marker marker) {
            // TODO Auto-generated method stub
            return null;
        }

    }

    private void setInfoWindowsData(Marker marker, int i) {
        if (marker.getTitle().toString().equals(mPoisList.get(i).getName().toString())) {
            if (mPoisList.get(i).getFilename() != null) {
                String poiImage = mPoisList.get(i).getFilename();
                setCustomDescriptionText(mPoisList.get(i).getDescription().toString());
                Picasso.with(getActivity())
                        .load(Constants.poiImmagesUrl + poiImage)
                        .placeholder(R.drawable.placeholder_attraction)
                        .into(mMarkerImage, new MarkerCallback(marker, Constants.poiImmagesUrl + poiImage, mMarkerImage));
            }

        }
    }


    public class MarkerCallback implements Callback {
        Marker marker = null;
        String URL;
        ImageView userPhoto;


        MarkerCallback(Marker marker, String URL, ImageView userPhoto) {
            this.marker = marker;
            this.URL = URL;
            this.userPhoto = userPhoto;
        }

        @Override
        public void onError() {
        }

        @Override
        public void onSuccess() {
            if (marker != null && marker.isInfoWindowShown()) {
                marker.hideInfoWindow();

                Picasso.with(getActivity())
                        .load(URL)
                        .into(userPhoto);

                marker.showInfoWindow();
            }
        }
    }

    private void setCustomDescriptionText(String description_label) {
        try {
            String sub_description = description_label.substring(0, 20);
            mTvDescription.setText(sub_description + getResources().getString(R.string.more_details));
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
            mTvDescription.setText(description_label + getResources().getString(R.string.more_details));
        }

    }

    private void applyFonttoTextView(TextView v) {
        Typeface externalFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Montserrat-Regular.ttf");


        v.setTypeface(externalFont);
    }
}
