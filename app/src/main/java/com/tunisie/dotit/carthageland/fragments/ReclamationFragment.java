package com.tunisie.dotit.carthageland.fragments;


import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.tunisie.dotit.carthageland.application.BaseApplication;
import com.tunisie.dotit.carthageland.Utils.Constants;
import com.tunisie.dotit.carthageland.R;
import com.tunisie.dotit.carthageland.adapters.SpinnerAdapter;
import com.tunisie.dotit.carthageland.custom.CustomEditText;
import com.tunisie.dotit.carthageland.models.Subject;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class ReclamationFragment extends BaseFragment {


    View view;
    private SweetAlertDialog progressDoalog;
    Spinner rec_subject;
    CustomEditText txt_title;
    CustomEditText txt_content;
    BaseApplication myApp;
    List<String> ids;
    String id_sub = "";
    Subject[] subjects;
    Spinner spinner;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_reclamation, container, false);

        Toolbar toolbarTop = (Toolbar) getActivity().findViewById(R.id.toolbar);
        TextView mTitle = (TextView) toolbarTop.findViewById(R.id.toolbar_title);
        mTitle.setText("Réclamation/Suggestion");

        myApp = ((BaseApplication)this.getActivity().getApplication());

        rec_subject = (Spinner) view.findViewById(R.id.sp_subject);
        txt_title = (CustomEditText) view.findViewById(R.id.txt_title);
        txt_content = (CustomEditText) view.findViewById(R.id.txt_content);

        view.findViewById(R.id.btn_facebook).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFacebook("https://www.facebook.com/CarthageLandOfficiel/", "CarthageLandOfficiel");
            }
        });

        view.findViewById(R.id.btn_instagram).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openInstagram("https://www.instagram.com/carthage.land/");

            }
        });

        view.findViewById(R.id.btn_send).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage();

            }
        });

        getSubjects();

        // Inflate the layout for this fragment
        return view;
    }

    public void openInstagram(String url) {

        String scheme = "http://instagram.com/_u/carthage.land";
        String path = "https://instagram.com/carthage.land";
        String nomPackageInfo ="com.instagram.android";
        Intent intentAiguilleur;
        try {
            this.getActivity().getPackageManager().getPackageInfo(nomPackageInfo, 0);
            intentAiguilleur = new Intent(Intent.ACTION_VIEW, Uri.parse(scheme));
        } catch (Exception e) {
            intentAiguilleur = new Intent(Intent.ACTION_VIEW, Uri.parse(path));
        }
        this.getActivity().startActivity(intentAiguilleur);

        /*Uri uri = Uri.parse(url);


        Intent i= new Intent(Intent.ACTION_VIEW,uri);

        i.setPackage("com.instagram.android");

        try {
            startActivity(i);
        } catch (ActivityNotFoundException e) {

            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://instagram.com/xxx")));
        }*/

    }

    public void openFacebook(String url, String fbc_id) {
        String facebookUrl = url;
        try {
            int versionCode = this.getActivity().getPackageManager().getPackageInfo("com.facebook.katana", 0).versionCode;
            if (versionCode >= 3002850) {
                Uri uri = Uri.parse("fb://facewebmodal/f?href=" + facebookUrl);
                startActivity(new Intent(Intent.ACTION_VIEW, uri));
            } else {
                Uri uri = Uri.parse("fb://page/"+fbc_id);
                startActivity(new Intent(Intent.ACTION_VIEW, uri));
            }
        } catch (PackageManager.NameNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(facebookUrl)));
        }
    }

    public void sendMessage() {
        if ((TextUtils.isEmpty(txt_title.getText())) || (TextUtils.isEmpty(txt_content.getText()))) {
            final SweetAlertDialog errDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE);
            errDialog.setTitleText("Erreur...");
            errDialog.setContentText("Veuillez saisir tous les champs!");
            errDialog.show();
        } else {
            progressDoalog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
            progressDoalog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
            progressDoalog.setTitleText("Chargement");
            progressDoalog.setCancelable(true);
            progressDoalog.show();
            // Instantiate the RequestQueue.
            try {
                RequestQueue queue = Volley.newRequestQueue(getActivity());
                JSONObject jsonBody = new JSONObject();

                Log.e("RECLAMATION", "ids "+ids);

                jsonBody.put("id_user", getPreferences().getUser().getId_client());
                jsonBody.put("titre", txt_title.getText().toString());
                jsonBody.put("sujet", id_sub);
                jsonBody.put("content", txt_content.getText().toString());

                final String requestBody = jsonBody.toString();

                StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.sendMessageUrl, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("RECLAMATION", "Response is " + response);
                        Log.i("VOLLEY", response);

                        parseJSONResponse(response);

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDoalog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
                        progressDoalog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
                        progressDoalog.setTitleText("Chargement");
                        progressDoalog.setCancelable(true);
                        progressDoalog.setTitleText("Erreur!")
                                .setContentText("Une erreur est survenue, veuillez vérifier votre connexion Internet ou essayer plus tard.")
                                .setConfirmText("OK")
                                .changeAlertType(SweetAlertDialog.ERROR_TYPE);
                        Log.e("VOLLEYY", error.toString());
                    }
                }) {
                    @Override
                    public String getBodyContentType() {
                        return "application/json; charset=utf-8";
                    }

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("X-Auth-Token", getPreferences().getToken());

                        return params;
                    }


                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            Log.e("requestBody ", requestBody);
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }

                    /*@Override
                    protected Response<String> parseNetworkResponse(NetworkResponse response) {
                        String responseString = "";
                        if (response != null) {
                            responseString = String.valueOf(response);
                            Log.e(TAG, "ResponseString is " + response);
                            // can get more details such as response.headers
                        }
                        return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                    }*/
                };

                queue.add(stringRequest);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void getSubjects() {
        Log.e("Register ", "get all governates");
        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(getActivity());

// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.subjectsUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            final JSONObject response_ = new JSONObject(response);

                            String code = response_.getString("code");
                            Log.e("Scan json", String.valueOf(code));


                            if (code.equals("200")) {
                                JSONArray json = response_.getJSONArray("response");
                                subjects = new Subject[json.length()];
                                for (int i = 0; i < json.length(); i++) {
                                    JSONObject row = json.getJSONObject(i);
                                    Log.e("subjects ", "row " + row.toString());
                                    subjects[i] = new Subject(row.getString("id"), row.getString("type"));
                                    Log.e("subjects ", "subjects[i] " + subjects[i]);
                                    //subjects.add(new Subject(row.getString("id"), row.getString("type")));
                                    initSubjects();
                                }
                            }
                        } catch (JSONException ex) {
                            ex.printStackTrace();
                        }

                        Log.e("Register ", "Response is" +response);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDoalog = new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE);
                progressDoalog.setTitleText("Erreur!")
                        .setContentText("Une erreur est survenue, veuillez vérifier votre connexion Internet ou essayer plus tard.")
                        .setConfirmText("OK")
                        .changeAlertType(SweetAlertDialog.ERROR_TYPE);
                Log.e("VOLLEYY", error.toString());
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("X-Auth-Token", getPreferences().getToken());

                return params;
            }

        };
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    public String parseJSONResponse(String jsonResponse) {

        try {

            final JSONObject json = new JSONObject(jsonResponse);

            String code = json.getString("code");
            Log.e("Scan json", String.valueOf(code));


            if (code.equals("200")) {

                progressDoalog.setTitleText("Succès!")
                        .setContentText("Votre demande a été envoyée avec succès, nous allons la traiter dans les meilleurs délais.")
                        .setConfirmText("OK")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                progressDoalog.dismissWithAnimation();
                                txt_content.setText("");
                                txt_title.setText("");
                                spinner.setSelection(0);

                            }
                        })
                        .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);


            } else {
                progressDoalog.setTitleText("Erreur!")
                        .setContentText("Veuillez vérifier votre login/mot de passe")
                        .setConfirmText("OK")
                        .changeAlertType(SweetAlertDialog.ERROR_TYPE);
                String msgErr = json.getString("message");
                Log.e("msgErr ", msgErr);

            }
            return code;

        } catch (JSONException ex) {

            ex.printStackTrace();
            return "250";
        }
    }

    public void initSubjects() {

        // Spinner element
        spinner = (Spinner) view.findViewById(R.id.sp_subject);

        if( subjects!=null)
        {
            Log.e("Reclamation", "subjects "+subjects);
            // Creating adapter for spinner
            SpinnerAdapter dataAdapter = new SpinnerAdapter(getActivity(), android.R.layout.simple_spinner_item, subjects); /*{
            public View getView(int position, View convertView, ViewGroup parent) {


                TextView v = (TextView) super.getView(position, convertView, parent);
                Subject sb = subjects.get(position);
                v.setText(sb.getName_subject());
                applyFontToSpinner((TextView) v);

                return v;


            }*/


            /*public View getDropDownView(int position, View convertView, ViewGroup parent) {


                View v = super.getDropDownView(position, convertView, parent);

                applyFontToSpinner((TextView) v);

                return v;


            }
        };*/

            // Drop down layout style - list view with radio button
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            // attaching data adapter to spinner
            spinner.setAdapter(dataAdapter);


            // Spinner click listener
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                public void onItemSelected(AdapterView<?> parent, View view,
                                           int position, long id) {

                    // On selecting a spinner item
                    String item = parent.getItemAtPosition(position).toString();
                    Subject sub = (Subject) parent.getItemAtPosition(position);
                    id_sub = sub.getId_subject();

                    // Showing selected spinner item
                    // Toast.makeText(parent.getContext(), "Selected: " + item, Toast.LENGTH_LONG).show();

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }


    }

    private void applyFontToSpinner(TextView v) {
        Typeface externalFont=Typeface.createFromAsset(getActivity().getAssets(), "fonts/Montserrat-Regular.ttf");


        v.setTypeface(externalFont);
    }


}
