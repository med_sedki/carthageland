package com.tunisie.dotit.carthageland.models;

/**
 * Created by eniso on 06/10/2017.
 */

public class Carte {

    private int id_carte;
    private String num_carte;
    private String solde_points;
    private String type;
    private String client;


    public Carte(String num_carte, String solde_points, String type, String client) {
        this.num_carte = num_carte;
        this.solde_points = solde_points;
        this.type = type;
        this.client = client;
    }

    public int getId_carte() {
        return id_carte;
    }

    public void setId_carte(int id_carte) {
        this.id_carte = id_carte;
    }

    public String getNum_carte() {
        return num_carte;
    }

    public void setNum_carte(String num_carte) {
        this.num_carte = num_carte;
    }

    public String getSolde_points() {
        return solde_points;
    }

    public void setSolde_points(String solde_points) {
        this.solde_points = solde_points;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }
}

