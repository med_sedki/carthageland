package com.tunisie.dotit.carthageland.global;

import com.tunisie.dotit.carthageland.models.Product;

/**
 * Created on 2/16/18.
 */

public interface VoucherItemClickListener {
    void onItemClicked(String code);
}
