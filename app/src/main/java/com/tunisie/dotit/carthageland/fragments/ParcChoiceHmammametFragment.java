package com.tunisie.dotit.carthageland.fragments;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextSwitcher;
import android.widget.Toast;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.tunisie.dotit.carthageland.R;
import com.tunisie.dotit.carthageland.Utils.Constants;
import com.tunisie.dotit.carthageland.activities.LoginActivity;
import com.tunisie.dotit.carthageland.adapters.CarouselPagerAdapter;
import com.tunisie.dotit.carthageland.adapters.CoverFlowAdapter;
import com.tunisie.dotit.carthageland.adapters.GetImageAdapter;
import com.tunisie.dotit.carthageland.application.BaseApplication;
import com.tunisie.dotit.carthageland.custom.CustomButton;
import com.tunisie.dotit.carthageland.custom.CustomTextView;
import com.tunisie.dotit.carthageland.global.APIClient;
import com.tunisie.dotit.carthageland.global.APIInterface;
import com.tunisie.dotit.carthageland.models.HomeTextResponse;
import com.tunisie.dotit.carthageland.models.SliderPhotoResponse;
import com.yarolegovich.discretescrollview.DiscreteScrollView;
import com.yarolegovich.discretescrollview.InfiniteScrollAdapter;
import com.yarolegovich.discretescrollview.Orientation;
import com.yarolegovich.discretescrollview.transform.ScaleTransformer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import it.moondroid.coverflow.components.ui.containers.FeatureCoverFlow;
import retrofit2.Call;
import retrofit2.Callback;


/**
 * A simple {@link Fragment} subclass.
 */
public class ParcChoiceHmammametFragment extends BaseFragment implements DiscreteScrollView.OnClickListener, BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener {

    ViewPager viewPager;
    private DiscreteScrollView itemPicker;
    int images[] = {R.drawable.carthage_land_1, R.drawable.carthage_land_2, R.drawable.carthage_land_3, R.drawable.carthage_land_4};
    private InfiniteScrollAdapter infiniteAdapter;
    CarouselPagerAdapter myCustomPagerAdapter;
    private CustomTextView mShowNewsTv;
    private ImageView mHideNewsTv;
    private LinearLayout mNewsLayout, mParcslay, mPlanLay, mRestaurantsLay, mOffersLay, mOnlineLay;
    private SliderLayout mDemoSlider;

    //    HashMap<String, Integer> mUrlMaps = new HashMap<String, Integer>();
//    private List<Integer> mImageslist = new ArrayList();
    //----
    HashMap<String, String> mUrlMaps = new HashMap<String, String>();
    private ArrayList<String> mImageslist = new ArrayList();
    //----

    private CustomTextView mPlanIcon;
    private APIInterface mApiInerface;
    private CustomButton mAccesAccountBtn;
    private LinearLayout mFirstLayout, mSecondLayout, mSecondaryRestoLayout;
    int mCurrentPage = 0;
    Timer mTime;
    final long DELAY_MS = 500;//delay in milliseconds before task is to be executed
    final long PERIOD_MS = 3000; // time in milliseconds between successive task executions.

    View view;
    Fragment fragment = null;

    private FeatureCoverFlow mCoverFlow;
    private CoverFlowAdapter mAdapter;
    //private int[] mDataHammamet = new int[]{R.drawable.cl_hammamet_1, R.drawable.cl_hammamet_2, R.drawable.cl_hammamet_1, R.drawable.carthage_land_2};
    //private int[] mDataTunis = new int[]{R.drawable.cl_tunis1, R.drawable.cl_tunis2, R.drawable.cl_tunis3, R.drawable.cl_tunis4};
    private TextSwitcher mTitle;
    BaseApplication myApp;
    private CustomTextView mFirstText, mSecondText;

    CustomTextView btn_attr, btn_restau, btn_infos;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_parc_choice, container, false);
        /*viewPager = (ViewPager) view.findViewById(R.id.viewPager);

        myCustomPagerAdapter = new CarouselPagerAdapter(this.getActivity(), images);
        viewPager.setAdapter(myCustomPagerAdapter);

        TabLayout tabLayout = (TabLayout) view.findViewById(R.id.tabDots);
        tabLayout.setupWithViewPager(viewPager, true);*/
        Log.e("mDataaa ==== ", this.getActivity().getTitle().toString());
        setRetainInstance(true);

        itemPicker = (DiscreteScrollView) view.findViewById(R.id.item_picker);
        mNewsLayout = view.findViewById(R.id.news_layout);
        mShowNewsTv = view.findViewById(R.id.show_text_tv);
        mHideNewsTv = view.findViewById(R.id.iv_close_subscribe);
        mPlanLay = view.findViewById(R.id.layout_plan);
        mRestaurantsLay = view.findViewById(R.id.layout_restaurants);
        mOffersLay = view.findViewById(R.id.layout_of);
        mParcslay = view.findViewById(R.id.layout_parc);
        mOnlineLay = view.findViewById(R.id.layout_online);
        mFirstLayout = view.findViewById(R.id.first_main_lay);
        mSecondLayout = view.findViewById(R.id.second_main_lay);
        mSecondaryRestoLayout = view.findViewById(R.id.layout_restaurants_secondary);
        mPlanIcon = view.findViewById(R.id.txt_plan);
        mFirstText = view.findViewById(R.id.first_text);
        mSecondText = view.findViewById(R.id.second_text);
        mAccesAccountBtn = view.findViewById(R.id.acces_acount_btn);

        itemPicker.setOnClickListener(this);
        mHideNewsTv.setOnClickListener(this);
        mShowNewsTv.setOnClickListener(this);
        mParcslay.setOnClickListener(this);
        mRestaurantsLay.setOnClickListener(this);
        mOffersLay.setOnClickListener(this);
        mOnlineLay.setOnClickListener(this);
        mPlanLay.setOnClickListener(this);
        mAccesAccountBtn.setOnClickListener(this);
        mSecondaryRestoLayout.setOnClickListener(this);

        mPlanIcon.setOnClickListener(this);

        itemPicker.setOrientation(Orientation.HORIZONTAL);
        //itemPicker.addOnItemChangedListener(this.getActivity());

        // infiniteAdapter = InfiniteScrollAdapter.wrap(new ShopAdapter(mDataHammamet));
        itemPicker.setAdapter(infiniteAdapter);
        //itemPicker.setItemTransitionTimeMillis(DiscreteScrollViewOptions.getTransitionTime());
        itemPicker.setItemTransformer(new ScaleTransformer.Builder()
                .setMinScale(0.7f)
                .build());

        mDemoSlider = (SliderLayout) view.findViewById(R.id.slider);
        //setImageSlider();

        //setmImageslist();
        getImagesList();
        setViewPager();
        configurePublicMode();

        getNewsText(Constants.Sites.HHAMMAMET_PARAM);

       /* mUrlMaps.put("Hannibal", "http://static2.hypable.com/wp-content/uploads/2013/12/hannibal-season-2-release-date.jpg");
        mUrlMaps.put("Big Bang Theory", "http://tvfiles.alphacoders.com/100/hdclearart-10.png");
        mUrlMaps.put("House of Cards", "http://cdn3.nflximg.net/images/3093/2043093.jpg");
        mUrlMaps.put("Game of Thrones", "http://images.boomsbeat.com/data/images/full/19640/game-of-thrones-season-4-jpg.jpg");*/

      /*  HashMap<String,Integer> file_maps = new HashMap<String, Integer>();
        file_maps.put("Hannibal",R.drawable.hannibal);
        file_maps.put("Big Bang Theory",R.drawable.bigbang);
        file_maps.put("House of Cards",R.drawable.house);
        file_maps.put("Game of Thrones", R.drawable.game_of_thrones);*/

       /* mTitle = (TextSwitcher) view.findViewById(R.id.title);
        mTitle.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                LayoutInflater inflater = LayoutInflater.from(ParcChoiceHmammametFragment.this.getActivity());
                TextView textView = (TextView) inflater.inflate(R.layout.item_title, null);
                return textView;
            }
        });
        Animation in = AnimationUtils.loadAnimation(this.getActivity(), R.anim.slide_in_top);
        Animation out = AnimationUtils.loadAnimation(this.getActivity(), R.anim.slide_out_bottom);
        mTitle.setInAnimation(in);
        mTitle.setOutAnimation(out);

        mAdapter = new CoverFlowAdapter(this.getActivity());
        mAdapter.setData(mData);
        mCoverFlow = (FeatureCoverFlow) view.findViewById(R.id.coverflow);
        mCoverFlow.setAdapter(mAdapter);*/
        myApp = ((BaseApplication) this.getActivity().getApplication());
        Log.e("Plan__ ", getPreferences().getCarthageLandLocation());


        btn_attr = view.findViewById(R.id.txt_attractions);
        btn_attr.setOnClickListener(this);

        btn_restau = view.findViewById(R.id.txt_restau);
        btn_restau.setOnClickListener(this);

        btn_infos = view.findViewById(R.id.offers_txt);
        btn_infos.setOnClickListener(this);

        // Inflate the layout for this fragment
        return view;
    }

/*    void setmImageslist() {
        mImageslist.add((R.drawable.clhammamet_1));
        mImageslist.add((R.drawable.clhammamet_2));
        mImageslist.add((R.drawable.clhammamet_3));
        mImageslist.add((R.drawable.clhammamet_4));
    }*/

    public void getImagesList() {
        mApiInerface = APIClient.getClient().create(APIInterface.class);

        Call<SliderPhotoResponse> call = mApiInerface.getSliders();
        call.enqueue(new Callback<SliderPhotoResponse>() {
            @Override
            public void onResponse(Call<SliderPhotoResponse> call, retrofit2.Response<SliderPhotoResponse> response) {

                Log.d("__TAG__", response + "");

                if (response.isSuccessful() && response.body() != null && response.body().getResponse() != null) {
                    if (!response.body().getResponse().isEmpty()) {

                        for (int i = 0; i < response.body().getResponse().size(); i++) {

                            String photo = null;
                            if (response.body().getResponse().get(i).getSite().equals("Hammamet")) {

                                if (response.body() != null) {
                                    photo = response.body().getResponse().get(i).getFilename();
                                }

                                Log.e("photo", "photo" + i + " : " + Constants.baseImageUrl + response.body().getResponse().get(i).getWebPath() + "/" + photo);
                                mImageslist.add(Constants.baseImageUrl + response.body().getResponse().get(i).getWebPath() + "/" + photo);
                            }

                        }

                    } else {
                        Toast.makeText(getActivity(), getString(R.string.error_empty_list), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getActivity(), getString(R.string.error_server), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SliderPhotoResponse> call, Throwable t) {
                call.cancel();
            }
        });

    }

    /*private void setImageSlider() {

        mUrlMaps.put("Hammamet 1", R.drawable.carthage_land_1);
        mUrlMaps.put("Hammamet 2", R.drawable.carthage_land_2);
        mUrlMaps.put("Hammamet 3", R.drawable.carthage_land_3);
        mUrlMaps.put("Hammamet 4", R.drawable.carthage_land_4);

        for (String name : mUrlMaps.keySet()) {
            TextSliderView textSliderView = new TextSliderView(getActivity());
            // initialize a SliderLayout
            textSliderView
                    .description("")
                    .image(mUrlMaps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(this);

            //add your extra information
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra", name);

            mDemoSlider.addSlider(textSliderView);
        }
        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Default);
        mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        mDemoSlider.setCustomAnimation(new DescriptionAnimation());
        mDemoSlider.setDuration(3000);
        mDemoSlider.setIndicatorVisibility(PagerIndicator.IndicatorVisibility.Invisible);
        mDemoSlider.addOnPageChangeListener(this);
        ListView l = (ListView) view.findViewById(R.id.transformers);
        l.setAdapter(new TransformerAdapter(getActivity()));
        l.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // mDemoSlider.setPresetTransformer(((TextView) view).getText().toString());
                // Toast.makeText(getActivity(), ((TextView) view).getText().toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }*/

    private void setViewPager() {
        final ViewPager mViewPager = view.findViewById(R.id.viewPageAndroid);
        GetImageAdapter adapterView = new GetImageAdapter(getActivity(), mImageslist);
        mViewPager.setAdapter(adapterView);
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (mCurrentPage == mViewPager.getAdapter().getCount() - 1) {
                    mCurrentPage = 0;
                }
                mViewPager.getAdapter().notifyDataSetChanged();
                mViewPager.setCurrentItem(mCurrentPage++, true);
            }
        };

        mTime = new Timer(); // This will create a new Thread
        mTime.schedule(new TimerTask() { // task to be scheduled

            @Override
            public void run() {
                handler.post(Update);
            }
        }, DELAY_MS, PERIOD_MS);
    }


    private void configurePublicMode() {
        if (getPreferences().getConnectedMode().equals("false")) {
            mOffersLay.setVisibility(View.INVISIBLE);
            mFirstLayout.setWeightSum(2);
            mSecondLayout.setWeightSum(2);
            mRestaurantsLay.setVisibility(View.GONE);
            mSecondaryRestoLayout.setVisibility(View.VISIBLE);
            mRestaurantsLay.setVisibility(View.INVISIBLE);
        } else {
            mAccesAccountBtn.setVisibility(View.GONE);
            mOffersLay.setOnClickListener(this);
            mFirstLayout.setWeightSum(3);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.gravity = Gravity.CENTER;
            layoutParams.setMargins(30, 5, 30, 0);
            mSecondLayout.setLayoutParams(layoutParams);
            mSecondLayout.setWeightSum(2);
            mSecondaryRestoLayout.setVisibility(View.GONE);
            mOffersLay.setVisibility(View.VISIBLE);
            mRestaurantsLay.setVisibility(View.VISIBLE);
        }
    }

    public void getNewsText(String code) {
        mApiInerface = APIClient.getClient().create(APIInterface.class);

        /**
         GET List Resources
         **/
        Call<HomeTextResponse> call = mApiInerface.getNewsText(getPreferences().getToken(), code);
        call.enqueue(new Callback<HomeTextResponse>() {
            @Override
            public void onResponse(Call<HomeTextResponse> call, retrofit2.Response<HomeTextResponse> response) {

                //  Log.d("TAG", response.body().getResponse() + " response get poi ");
                Log.d("TAG", response + "");
                if (response.isSuccessful() && response.body() != null && response.body().getResponse() != null) {
                    mFirstText.setText(response.body().getResponse().getMessage());
                    mSecondText.setText(response.body().getResponse().getSite());
                } else {
                    Toast.makeText(getActivity(), getString(R.string.error_server), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<HomeTextResponse> call, Throwable t) {
                hideProgressBar();
                Toast.makeText(getActivity(), getString(R.string.error_server), Toast.LENGTH_SHORT).show();
                call.cancel();
            }
        });
    }

    public void fragmentreplace(Class fragmentClass, String tag) throws IllegalAccessException, java.lang.InstantiationException {
        FragmentManager fragmentManager = getFragmentManager();
        Fragment fragment = (Fragment) fragmentClass.newInstance();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.flContent, fragment, tag);
        fragmentTransaction.addToBackStack(null); //this will add it to back stack
        fragmentTransaction.commit();
    }

    public void goToPlan(Class fragmentClass) {
        Fragment fragment = null;
        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Insert the fragment by replacing any existing fragment
        if (fragment != null) {
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.flContent, fragment);
            fragmentTransaction.addToBackStack(null); //this will add it to back stack
            fragmentTransaction.commit();
        }
    }

    private void closeFragment() {
        getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.item_picker:
                /*if (getPreferences().getCarthageLandLocation().toString().equals(Constants.Parameters.CL_TUNIS)) {
                    infiniteAdapter = InfiniteScrollAdapter.wrap(new ShopAdapter(mDataTunis));
                    itemPicker.setAdapter(infiniteAdapter);
                    //itemPicker.setItemTransitionTimeMillis(DiscreteScrollViewOptions.getTransitionTime());
                    itemPicker.setItemTransformer(new ScaleTransformer.Builder()
                            .setMinScale(0.7f)
                            .build());
                } else if (getPreferences().getCarthageLandLocation().toString().equals(Constants.Parameters.CL_HAMMAMET)) {
                    infiniteAdapter = InfiniteScrollAdapter.wrap(new ShopAdapter(mDataHammamet));
                    itemPicker.setAdapter(infiniteAdapter);
                    //itemPicker.setItemTransitionTimeMillis(DiscreteScrollViewOptions.getTransitionTime());
                    itemPicker.setItemTransformer(new ScaleTransformer.Builder()
                            .setMinScale(0.7f)
                            .build());
                }*/
                break;

            case R.id.show_text_tv:
                mNewsLayout.setVisibility(View.VISIBLE);
                mShowNewsTv.setVisibility(View.GONE);
                break;

            case R.id.iv_close_subscribe:
                mNewsLayout.setVisibility(View.GONE);
                mShowNewsTv.setVisibility(View.VISIBLE);
                break;

            case R.id.layout_of:
                goToPlan(PromotionsFragment.class);
                break;

            case R.id.layout_online:
                //String url = getString(R.string.cv_hammamet_website);
                String url = Constants.cvHammametXebsiteUrl;
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
                break;

            case R.id.layout_parc:
                goToPlan(ParcsFragment.class);
                break;

            case R.id.layout_restaurants:
                goToPlan(RestaurantsFragment.class);
                break;

            case R.id.layout_restaurants_secondary:
                goToPlan(RestaurantsFragment.class);
                break;

            case R.id.layout_plan:
                goToPlan(PlanFragment.class);
                break;

            case R.id.acces_acount_btn:
                startActivity(new Intent(getActivity(), LoginActivity.class));
                getActivity().finish();
                break;


            case R.id.txt_plan:
                goToPlan(PlanFragment.class);
                break;

            case R.id.txt_attractions:
                goToPlan(ParcsFragment.class);
                break;

            case R.id.txt_restau:
                goToPlan(RestaurantsFragment.class);
                break;

            case R.id.offers_txt:
                goToPlan(PromotionsFragment.class);
                break;
        }
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}