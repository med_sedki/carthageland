package com.tunisie.dotit.carthageland.fragments;


import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ibotta.android.support.pickerdialogs.SupportedDatePickerDialog;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.tunisie.dotit.carthageland.R;
import com.tunisie.dotit.carthageland.Utils.Constants;
import com.tunisie.dotit.carthageland.activities.HomeActivity;
import com.tunisie.dotit.carthageland.application.BaseApplication;
import com.tunisie.dotit.carthageland.custom.CustomEditText;
import com.tunisie.dotit.carthageland.custom.CustomTextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddMemberFragment extends BaseFragment implements View.OnClickListener, AdapterView.OnItemSelectedListener, SupportedDatePickerDialog.OnDateSetListener {

    private SweetAlertDialog progressDoalog;
    BaseApplication myApp;

    Spinner spinnerG;
    Spinner spinnerS;

    String TAG = AddMemberFragment.class.getSimpleName();
    View view;

    CustomEditText txt_birthday;
    CustomEditText txt_name;
    CustomEditText txt_name2;
    CustomEditText txt_email;

    CustomTextView btn_add;

    String name;
    String name2;
    String email;
    String birthday;

    private int mYear, mMonth, mDay;

    public static final Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile(
            "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                    "\\@" +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                    "(" +
                    "\\." +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                    ")+"
    );

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_add_member, container, false);
        Toolbar toolbarTop = (Toolbar) getActivity().findViewById(R.id.toolbar);
        TextView mTitle = (TextView) toolbarTop.findViewById(R.id.toolbar_title);
        mTitle.setText("Ajouter un membre");

        myApp = ((BaseApplication) this.getActivity().getApplication());
        HomeActivity.toolbar.setNavigationIcon(R.drawable.back_arrow_ic);
        HomeActivity.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getFragmentManager() != null) {
                    Fragment currentFragment = getFragmentManager().findFragmentById(R.id.flContent);
                    if (currentFragment instanceof AddMemberFragment) {
                        navigateToFamilyFragment();
                    }
                } else {
                    HomeActivity.openDrawer();
                }
            }
        });
        HomeActivity.toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                getActivity().finish();
                return true;
            }
        });
        txt_name = view.findViewById(R.id.txt_name);
        txt_name2 = view.findViewById(R.id.txt_name2);
        txt_email = view.findViewById(R.id.txt_email);
        txt_birthday = view.findViewById(R.id.txt_birthday);
        btn_add = view.findViewById(R.id.btn_add);

        txt_birthday.setOnClickListener(this);
        btn_add.setOnClickListener(this);
        initGenders();
        initStatus();

        return view;
    }

    public void initGenders() {
        spinnerG = (Spinner) view.findViewById(R.id.gender);
        spinnerG.setOnItemSelectedListener(this);

        // Spinner Drop down elements
        List<String> genders = new ArrayList<String>();
        genders.add("Homme");
        genders.add("Femme");

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_spinner_item, genders) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                return v;
            }
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                applyFontToSpinner((TextView) v);
                return v;
            }
        };

        spinnerG.setPrompt("Vous êtes");

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinnerG.setAdapter(dataAdapter);
    }

    public void initStatus() {
        spinnerS = (Spinner) view.findViewById(R.id.status);
        spinnerS.setOnItemSelectedListener(this);

        // Spinner Drop down elements
        List<String> status = new ArrayList<String>();
        status.add("Epouse");
        status.add("Epoux");
        status.add("Père");
        status.add("Mère");
        status.add("Fils");
        status.add("Fille");
        status.add("Frère");
        status.add("Soeur");

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_spinner_item, status) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                return v;
            }

            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                applyFontToSpinner((TextView) v);
                return v;
            }
        };
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinnerS.setAdapter(dataAdapter);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        // On selecting a spinner item
        String item = parent.getItemAtPosition(position).toString();

        // Showing selected spinner item
        //Toast.makeText(parent.getContext(), "Selected: " + item, Toast.LENGTH_LONG).show();
    }

    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub
    }

    private void applyFontToSpinner(TextView v) {
        Typeface externalFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Montserrat-Regular.ttf");

        v.setTypeface(externalFont);
    }

    public void addMember() {

        View focusView = null;
        boolean cancel = false;

        name = txt_name.getText().toString();
        name2 = txt_name2.getText().toString();
        email = txt_email.getText().toString();
        birthday = txt_birthday.getText().toString();
        if (TextUtils.isEmpty(name)) {
            txt_name.setError(getString(R.string.error_field_required));
            focusView = txt_name;
            cancel = true;
        } else if (isTextValid(name)) {
            txt_name.setError(getString(R.string.error_invalid_nom));
            focusView = txt_name;
            cancel = true;
        }

        if (TextUtils.isEmpty(name2)) {
            txt_name2.setError(getString(R.string.error_field_required));
            focusView = txt_name2;
            cancel = true;
        } else if (isTextValid(name2)) {
            txt_name2.setError(getString(R.string.error_invalid_nom));
            focusView = txt_name2;
            cancel = true;
        }

        if (TextUtils.isEmpty(email)) {
            txt_email.setError(getString(R.string.error_field_required));
            focusView = txt_email;
            cancel = true;
        } else if (!isMailValid(email)) {
            txt_email.setError(getString(R.string.error_invalid_mail));
            focusView = txt_email;
            cancel = true;
        }

        if (TextUtils.isEmpty(birthday)) {
            txt_birthday.setError(getString(R.string.error_field_required));
            focusView = txt_birthday;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            sendRequest(Constants.setClientUsers);
        }
    }

    public void sendRequest(final String url) {
        progressDoalog = new SweetAlertDialog(this.getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        progressDoalog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressDoalog.setTitleText("Chargement");
        progressDoalog.setCancelable(true);
        progressDoalog.show();

        try {
            RequestQueue queue = Volley.newRequestQueue(this.getActivity());
            JSONObject jsonBody = new JSONObject();

            jsonBody.put("id_client", getPreferences().getUser().getId_client());
            jsonBody.put("firstname", txt_name.getText().toString());
            jsonBody.put("lastname", txt_name2.getText().toString());
            jsonBody.put("gender", spinnerG.getSelectedItem().toString());
            jsonBody.put("birthDay", txt_birthday.getText().toString());
            jsonBody.put("lienParente", spinnerS.getSelectedItem().toString());
            jsonBody.put("mail", txt_email.getText().toString());

            final String requestBody = jsonBody.toString();
            Log.e(TAG, "jsonBody " + requestBody);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    parseJSONResponse(response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDoalog.setTitleText("Erreur!")
                            .setContentText(getString(R.string.fb_connect_canceled))
                            .setConfirmText("OK")
                            .changeAlertType(SweetAlertDialog.ERROR_TYPE);
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("X-Auth-Token", getPreferences().getToken());

                    return params;
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }
            };
            queue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public boolean isTextValid(String text) {
        return Character.isDigit(text.charAt(0));
    }

    public boolean isMailValid(String email) {
        return EMAIL_ADDRESS_PATTERN.matcher(email).matches();
    }

    public void showCalendar(View v) {
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        SupportedDatePickerDialog dpd =
                new SupportedDatePickerDialog(getContext(), R.style.SpinnerDatePickerDialogTheme, this, mYear, mMonth, mDay);
        dpd.getDatePicker().setMaxDate(c.getTimeInMillis());
        dpd.show();
    }

    @Override
    public void onDateSet(DatePicker datePicker, int mYear, int mMonth, int mDay) {
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, mYear);
        c.set(Calendar.MONTH, mMonth);
        c.set(Calendar.DAY_OF_MONTH, mDay);
        txt_birthday.setText(mDay + "-" + (mMonth + 1) + "-" + mYear);
    }

    public String parseJSONResponse(String jsonResponse) {

        try {

            final JSONObject json = new JSONObject(jsonResponse);

            String code = json.getString("code");
            Log.e(TAG, String.valueOf(code));
            String message = json.getString("message");

            if (code.equals("200")) {
                Log.e(TAG, "Response is " + jsonResponse);
                Log.i("VOLLEY", jsonResponse);
                progressDoalog.setTitleText("Succès")
                        .setContentText("Le membre a été ajouté avec succès.")
                        .setConfirmText("OK")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                progressDoalog.dismiss();
                                try {
                                    fragmentreplace(FamilyFragment.class, "");
                                } catch (IllegalAccessException e) {
                                    e.printStackTrace();
                                } catch (java.lang.InstantiationException e) {
                                    e.printStackTrace();
                                }
                            }
                        })
                        .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                txt_name.setText("");
                txt_name2.setText("");
                txt_email.setText("");
                Log.e("TAG", jsonResponse);

            } else if (code.equals("400")) {
                Log.e("TAG", jsonResponse);
                progressDoalog.setTitleText("Erreur!")
                        .setContentText("Cet email est déjà utilisé, veuillez essayer avec une autre adresse mail.")
                        .setConfirmText("OK")
                        .changeAlertType(SweetAlertDialog.ERROR_TYPE);
            } else {
                Log.e("TAG", jsonResponse);
                progressDoalog.setTitleText("Erreur!")
                        .setContentText(getString(R.string.fb_connect_canceled))
                        .setConfirmText("OK")
                        .changeAlertType(SweetAlertDialog.ERROR_TYPE);
                String msgErr = json.getString("message");
                Log.e("msgErr ", msgErr);
            }
            return code;
        } catch (JSONException ex) {
            ex.printStackTrace();
            return "250";
        }
    }

    private void closeFragment() {
        getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
    }

    private void navigateToFamilyFragment() {
        Fragment fragment = null;
        try {
            closeFragment();
            fragment = ProfilFragment.class.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).addToBackStack("tag").commit();
    }

    public void fragmentreplace(Class fragmentClass, String tag) throws IllegalAccessException, java.lang.InstantiationException {
        FragmentManager fragmentManager = getFragmentManager();
        Fragment fragment = (Fragment) fragmentClass.newInstance();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.flContent, fragment, tag);
        fragmentTransaction.addToBackStack(null); //this will add it to back stack
        fragmentTransaction.commit();
    }

    @Override
    public void onClick(View v) {
        if (v == txt_birthday) {
            showCalendar(v);
        } else if (v == btn_add) {
            addMember();
        }
    }
}
