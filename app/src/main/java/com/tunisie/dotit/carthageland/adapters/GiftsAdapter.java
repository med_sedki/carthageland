package com.tunisie.dotit.carthageland.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tunisie.dotit.carthageland.R;
import com.tunisie.dotit.carthageland.adapters.holder.GiftsHolder;
import com.tunisie.dotit.carthageland.adapters.holder.ProductsHolder;
import com.tunisie.dotit.carthageland.global.VoucherItemClickListener;
import com.tunisie.dotit.carthageland.models.Product;

import java.util.ArrayList;

/**
 * Created on 2/15/18.
 */

public class GiftsAdapter extends RecyclerView.Adapter<GiftsHolder> {

    private final Context mContext;
    private ArrayList<Product> mProducts;
    private VoucherItemClickListener mVoucherItemListener ;

    public GiftsAdapter(ArrayList<Product> productsList , Context ctx  , VoucherItemClickListener voucherItemClickListener) {
        /*if (location.toString().equals(Constants.Parameters.CL_TUNIS)) {
            mProducts = AppUtils.getTunisRestaurants();
        } else if (location.toString().equals(Constants.Parameters.CL_HAMMAMET)) {
            mProducts = AppUtils.getHammametRestaurants() ;
        }*/
        mProducts = productsList ;
        this.mContext = ctx ;
        this.mVoucherItemListener = voucherItemClickListener ;
    }

    @Override
    public GiftsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.promo, parent, false);
        return new GiftsHolder(view);
    }


    @Override
    public void onBindViewHolder( GiftsHolder holder, int position) {
        Product restaurant = mProducts.get(position);
        holder.bind(restaurant,mVoucherItemListener);
    }

    @Override
    public int getItemCount() {
        return mProducts.size();
    }
}