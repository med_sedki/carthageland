package com.tunisie.dotit.carthageland.models;

import java.util.ArrayList;

/**
 * Created by eniso on 06/10/2017.
 */

public class HistoriqueConversion extends Historique {

    private String NbPointsCumul;
    private ArrayList<Achat> achats;

    /*public HistoriqueConversion(String id_historique, String date, String montant, String nbPointsCumul, ArrayList<Achat> achats) {
        super(id_historique, date, montant);
        //NbPointsCumul = nbPointsCumul;
        //this.achats = achats;
    }*/

    public String getNbPointsCumul() {
        return NbPointsCumul;
    }

    public void setNbPointsCumul(String nbPointsCumul) {
        NbPointsCumul = nbPointsCumul;
    }

    public ArrayList<Achat> getAchats() {
        return achats;
    }

    public void setAchats(ArrayList<Achat> achats) {
        this.achats = achats;
    }
}
