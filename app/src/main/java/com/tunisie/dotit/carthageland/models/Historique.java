package com.tunisie.dotit.carthageland.models;

/**
 * Created by eniso on 06/10/2017.
 */

public class Historique {

    private String id_historique;
    private String creationDate;
    //private String availabilityDate;
    private String montant;
    private String pointsF;
    private String type;
    private String name;

    public Historique() {
    }

    //public Historique(String id_historique, String creationDate, String availabilityDate, String montant, String pointsF, String type, String name) {
    public Historique(String id_historique, String creationDate, String montant, String pointsF, String type, String name) {
        this.id_historique = id_historique;
        this.creationDate = creationDate;
        this.montant = montant;
        //this.availabilityDate = availabilityDate;
        this.pointsF = pointsF;
        this.type = type;
        this.name = name;
    }

    public String getId_historique() {
        return id_historique;
    }

    public void setId_historique(String id_historique) {
        this.id_historique = id_historique;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String date) {
        this.creationDate = date;
    }

    /*public String getAvailabilityDate() {
        return availabilityDate;
    }

    public void setAvailabilityDate(String availabilityDate) {
        this.availabilityDate = availabilityDate;
    }*/

    public String getMontant() {
        return montant;
    }

    public void setMontant(String montant) {
        this.montant = montant;
    }

    public String getPointsF() {
        return pointsF;
    }

    public void setPointsF(String pointsF) {
        this.pointsF = pointsF;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
