package com.tunisie.dotit.carthageland.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by dotit on 4/4/18.
 */

public class LogOutResponse {
    @SerializedName("code")
    private String mCode;
    @SerializedName("message")
    private String mMessage;
    @SerializedName("response")
    private String mResponse;


    public String getResponse() {
        return mResponse;
    }

    public void setResponse(String mResponse) {
        this.mResponse = mResponse;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String mMessage) {
        this.mMessage = mMessage;
    }

    public String getCode() {
        return mCode;
    }

    public void setCode(String mCode) {
        this.mCode = mCode;
    }
}
