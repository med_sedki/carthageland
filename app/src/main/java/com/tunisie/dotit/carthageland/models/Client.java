package com.tunisie.dotit.carthageland.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by eniso on 06/10/2017.
 */

public class Client implements Parcelable {

    private String id_client;
    private String id_member;
    private String nom;
    private String prenom;
    private String email;
    private String dateDeNaissance;
    private String adresse;
    private String numTel;
    private String id_parente;
    private String lienDeParente;
    private Carte carte;


    public Client(String id_client, String id_member, String nom, String prenom, String email, String dateDeNaissance, String adresse, String numTel, Carte carte) {
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.dateDeNaissance = dateDeNaissance;
        this.adresse = adresse;
        this.carte = carte;
        this.numTel = numTel;
        this.id_client = id_client;
        this.id_member = id_member;
    }

    protected Client(Parcel in) {
        id_client = in.readString();
        id_member = in.readString();
        nom = in.readString();
        prenom = in.readString();
        email = in.readString();
        dateDeNaissance = in.readString();
        adresse = in.readString();
        numTel = in.readString();
        id_parente = in.readString();
        lienDeParente = in.readString();
    }

    public static final Creator<Client> CREATOR = new Creator<Client>() {
        @Override
        public Client createFromParcel(Parcel in) {
            return new Client(in);
        }

        @Override
        public Client[] newArray(int size) {
            return new Client[size];
        }
    };

    public String getNom() {

        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getId_client() {
        return id_client;
    }

    public void setId_client(String id_client) {
        this.id_client = id_client;
    }

    public String getDateDeNaissance() {
        return dateDeNaissance;
    }

    public void setDateDeNaissance(String dateDeNaissance) {
        this.dateDeNaissance = dateDeNaissance;
    }

    public String getNumTel() {
        return numTel;
    }

    public void setNumTel(String numTel) {
        this.numTel = numTel;
    }

    public String getId_parente() {
        return id_parente;
    }

    public void setId_parente(String id_parente) {
        this.id_parente = id_parente;
    }

    public String getLienDeParente() {
        return lienDeParente;
    }

    public void setLienDeParente(String lienDeParente) {
        this.lienDeParente = lienDeParente;
    }

    public Carte getCarte() {
        return carte;
    }

    public void setCarte(Carte carte) {
        this.carte = carte;
    }

    public String getId_member() {
        return id_member;
    }

    public void setId_member(String id_member) {
        this.id_member = id_member;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id_client);
        parcel.writeString(id_member);
        parcel.writeString(nom);
        parcel.writeString(prenom);
        parcel.writeString(email);
        parcel.writeString(dateDeNaissance);
        parcel.writeString(adresse);
        parcel.writeString(numTel);
        parcel.writeString(id_parente);
        parcel.writeString(lienDeParente);
    }
}
