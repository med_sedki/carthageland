package com.tunisie.dotit.carthageland.fragments;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.tunisie.dotit.carthageland.activities.HomeActivity;
import com.tunisie.dotit.carthageland.application.BaseApplication;
import com.tunisie.dotit.carthageland.Utils.Constants;
import com.tunisie.dotit.carthageland.R;
import com.tunisie.dotit.carthageland.custom.CustomTextView;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class SmilesFragment extends BaseFragment {


    BaseApplication myApp;
    private SweetAlertDialog progressDoalog;
    TextView nb_points;
    TextView btn_histo;
    TextView conv_pts;
    View view;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_smiles, container, false);

        //this.getActivity().setTitle("Mes points");
        Toolbar toolbarTop = (Toolbar) getActivity().findViewById(R.id.toolbar);
        TextView mTitle = (TextView) toolbarTop.findViewById(R.id.toolbar_title);
        mTitle.setText("Mes points");

        myApp = ((BaseApplication) this.getActivity().getApplication());

        HomeActivity.toolbar.setNavigationIcon(R.drawable.back_arrow_ic);
        HomeActivity.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getFragmentManager() != null) {
                    HomeActivity.openDrawer();
                }

            }
        });

        nb_points = view.findViewById(R.id.nbpoints);

        btn_histo = view.findViewById(R.id.btn_histo);

        HomeActivity.toolbar.setNavigationIcon(R.drawable.drawer_menu_ic);

        btn_histo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                goTo(HistoryFragment.class);

            }
        });

        conv_pts = view.findViewById(R.id.conv_pts);

        conv_pts.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                goTo(ConvertFragment.class);

            }
        });

        getNbPoints(getPreferences().getEmail());
        // Inflate the layout for this fragment
        return view;
    }

    public int getNbPoints(String email) {
        progressDoalog = new SweetAlertDialog(this.getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        progressDoalog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressDoalog.setTitleText("Chargement");
        progressDoalog.setCancelable(true);
        progressDoalog.show();
        try {
            RequestQueue queue = Volley.newRequestQueue(this.getActivity());
            JSONObject jsonBody = new JSONObject();

            jsonBody.put("email", email);

            final String requestBody = jsonBody.toString();

            StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.soldeWithEmailUrl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e("Smiles", "Response is " + response);
                    Log.i("VOLLEY", response);

                    parseJSONResponse(response);

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDoalog.setTitleText("Erreur!")
                            .setContentText("Une erreur est survenue, veuillez vérifier votre connexion Internet ou essayer plus tard.")
                            .setConfirmText("OK")
                            .changeAlertType(SweetAlertDialog.ERROR_TYPE);
                    Log.e("VOLLEYY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("X-Auth-Token", getPreferences().getToken());

                    return params;
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        Log.e("requestBody ", requestBody);
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }

                    /*@Override
                    protected Response<String> parseNetworkResponse(NetworkResponse response) {
                        String responseString = "";
                        if (response != null) {
                            responseString = String.valueOf(response);
                            Log.e(TAG, "ResponseString is " + response);
                            // can get more details such as response.headers
                        }
                        return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                    }*/
            };

            queue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public void goTo(Class fragmentClass) {

        Fragment fragment = null;
        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = this.getActivity().getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).addToBackStack(null).commit();
    }

    public void fragmentreplace(Class fragmentClass, String tag) throws IllegalAccessException, java.lang.InstantiationException {
        FragmentManager fragmentManager = getFragmentManager();
        Fragment fragment = (Fragment) fragmentClass.newInstance();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.flContent, fragment, tag);
        fragmentTransaction.addToBackStack("tag"); //this will add it to back stack
        fragmentTransaction.commit();
    }

    public String parseJSONResponse(String jsonResponse) {

        try {

            final JSONObject json = new JSONObject(jsonResponse);

            String code = json.getString("code");
            Log.e("Scan json", String.valueOf(code));


            if (code.equals("200")) {
                String response = json.getString("response");
                progressDoalog.dismiss();
                nb_points.setText(response);


            } else {
                progressDoalog.setTitleText("Erreur!")
                        .setContentText("Une erreur est survenue, veuillez essayer plus tard.")
                        .setConfirmText("OK")
                        .changeAlertType(SweetAlertDialog.ERROR_TYPE);
                String msgErr = json.getString("message");
                Log.e("msgErr ", msgErr);

            }
            return code;

        } catch (JSONException ex) {

            ex.printStackTrace();
            return "250";
        }
    }

}
