package com.tunisie.dotit.carthageland.global;

/**
 * Created on 2/4/18.
 */

public class BackgroundManager {

    private static BackgroundManager mInstance;
    private int mState;


    public static BackgroundManager getInstance() {
        if (mInstance == null) {
            mInstance = new BackgroundManager();
        }
        return mInstance;
    }

    private BackgroundManager() {
    }

    public void onStart() {
        mState++;
    }

    public void onStop() {
        mState--;
    }

    public boolean isInBackground() {
        return mState == 0;
    }

}
