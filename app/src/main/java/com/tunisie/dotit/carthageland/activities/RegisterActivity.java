package com.tunisie.dotit.carthageland.activities;

import android.app.DatePickerDialog;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ibotta.android.support.pickerdialogs.SupportedDatePickerDialog;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.tunisie.dotit.carthageland.R;
import com.tunisie.dotit.carthageland.Utils.Constants;
import com.tunisie.dotit.carthageland.custom.CustomButton;
import com.tunisie.dotit.carthageland.custom.CustomEditText;
import com.tunisie.dotit.carthageland.custom.CustomTextView;
import com.tunisie.dotit.carthageland.models.Country;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.regex.Pattern;

public class RegisterActivity extends BaseActivity implements
        View.OnClickListener, AdapterView.OnItemSelectedListener, SupportedDatePickerDialog.OnDateSetListener {


    private static final String TAG = "Register";
    public static Toolbar toolbar;
    private DatePickerDialog datePickerDialog;
    CustomEditText txt_birthday;
    CustomEditText txt_name;
    CustomEditText txt_name2;
    CustomEditText txt_phone;
    CustomEditText txt_email;
    CustomEditText txt_address;

    CustomEditText txt_gov;
    CustomEditText txt_zip;

    CustomTextView txt_member;
    CustomEditText txt_nbr_enfant;
    CustomEditText txt_pwd;
    CustomEditText txt_pwdconf;
    CheckBox check_box_cgu;
    CheckBox check_box_mailing;
    ImageView addButton;
    ImageView btn_return;
    CustomButton btn_subscribe;
    LinearLayout mainLinearLayout;
    ScrollView scrollView;
    private SweetAlertDialog progressDoalog;
    private int mYear, mMonth, mDay;

    public static final Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile(
            "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                    "\\@" +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                    "(" +
                    "\\." +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                    ")+"
    );

    String name;
    String name2;
    String phone;
    String email;
    String address;

    String govTxt;
    String zipTxt;

    String birthday;
    String nbrEnfant;
    String password;
    String passwordC;

    Boolean check1;
    Boolean check2 = false;

    Spinner spinnerG;
    Spinner spinner;
    Spinner spinnerZ;
    Spinner spinnerC;

    ArrayAdapter<String> dataAdapterC;
    ArrayAdapter<String> dataAdapterG;
    ArrayAdapter<String> dataAdapterZ;

    List<String> countries;
    List<String> mCountriesCodes;
    ArrayList<Country> mCountries = new ArrayList<Country>();
    List<String> governates;
    List<String> zipCodes;
    private String mCompletePhoneFormatText = "";
    private String mSelectedCountryCode = "";

    Integer age = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        // getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        txt_birthday = findViewById(R.id.txt_birthday);
        txt_birthday.setOnClickListener(this);

        txt_member = findViewById(R.id.txt_member);

        addButton = findViewById(R.id.btn_add);
        addButton.setOnClickListener(this);

        // btn_return = (ImageView) findViewById(R.id.btn_return);

        mainLinearLayout = findViewById(R.id.mainLinearLayout);
        scrollView = findViewById(R.id.scrollView);
        txt_name = findViewById(R.id.txt_name);
        txt_name2 = findViewById(R.id.txt_name2);
        txt_phone = findViewById(R.id.txt_phone);
        txt_email = findViewById(R.id.txt_email);
        txt_address = findViewById(R.id.txt_address);

        txt_gov = findViewById(R.id.txt_gov);
        txt_zip = findViewById(R.id.txt_zip);

        txt_nbr_enfant = findViewById(R.id.txt_nbr_enfant);
        txt_pwd = findViewById(R.id.txt_pwd);
        txt_pwdconf = findViewById(R.id.txt_pwdconf);
        check_box_cgu = findViewById(R.id.check_box_cgu);
        check_box_mailing = findViewById(R.id.check_box_mailing);

        btn_subscribe = findViewById(R.id.btn_subscribe);
        btn_subscribe.setOnClickListener(this);

        check_box_cgu.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    btn_subscribe.setBackgroundColor(getResources().getColor(R.color.blue));
                    btn_subscribe.setEnabled(true);
                    Log.d("Visibility ", "ok");
                } else if (!isChecked) {
                    btn_subscribe.setBackgroundColor(getResources().getColor(R.color.gray));
                    btn_subscribe.setEnabled(false);
                    Log.d("Deleted click ", "nok");
                }
            }
        });

        governates = new ArrayList<String>();
        countries = new ArrayList<String>();
        mCountriesCodes = new ArrayList<String>();
        zipCodes = new ArrayList<String>();

        initGenders();
        initCountries();
        initGovernorates();
        initZipCode();
        getCountries();

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Toolbar toolbarTop = (Toolbar) findViewById(R.id.toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationIcon(R.drawable.back_arrow_ic);
        View bottomSheet = findViewById(R.id.bottom_sheet);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                return false;
            }
        });

        txt_gov.setVisibility(View.GONE);
        txt_zip.setVisibility(View.GONE);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        // On selecting a spinner item
        String item = parent.getItemAtPosition(position).toString();

        // Showing selected spinner item
        //Toast.makeText(parent.getContext(), "Selected: " + item, Toast.LENGTH_LONG).show();
    }

    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub
    }

    public void initGenders() {
        // Spinner element
        spinnerG = (Spinner) findViewById(R.id.gender);
        spinnerG.setOnItemSelectedListener(this);

        // Spinner Drop down elements
        List<String> genders = new ArrayList<String>();
        genders.add("Homme");
        genders.add("Femme");

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapterG = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, genders) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                applyFontToSpinner((TextView) v);
                return v;
            }

            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                applyFontToSpinner((TextView) v);
                return v;
            }
        };
        spinnerG.setPrompt("Vous êtes");

        // Drop down layout style - list view with radio button
        dataAdapterG.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinnerG.setAdapter(dataAdapterG);
    }

    public void initGovernorates() {
        // Spinner element
        spinner = (Spinner) findViewById(R.id.gov);

        // Creating adapter for spinner
        dataAdapterG = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, governates) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                applyFontToSpinner((TextView) v);
                return v;
            }

            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                applyFontToSpinner((TextView) v);
                return v;
            }
        };

        // Drop down layout style - list view with radio button
        dataAdapterG.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapterG);
        if (governates.size() > 0) {
            getZipCodesForGov(governates.get(0));
            getZipCodesForGovAlg(governates.get(0));
        }

        // Spinner click listener
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                zipCodes.clear();
                dataAdapterZ.notifyDataSetChanged();
                if (governates.size() > 0) {
                    getZipCodesForGov(spinner.getSelectedItem().toString());
                    getZipCodesForGovAlg(spinner.getSelectedItem().toString());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
    }

    public void initCountries() {
        // Spinner element
        spinnerC = findViewById(R.id.country);
        spinnerC.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                governates.clear();
                zipCodes.clear();
                for (int i = 0; i < mCountries.size(); i++) {
                    if (mCountries.get(i).getName().toString().equals(spinnerC.getSelectedItem().toString())) {
                        txt_phone.setText(mCountries.get(i).getCode());
                    }
                }
                if (spinnerC.getSelectedItem().toString().equals("Tunisie")) {
                    spinner.setVisibility(View.VISIBLE);
                    spinnerZ.setVisibility(View.VISIBLE);

                    txt_gov.setVisibility(View.GONE);
                    txt_zip.setVisibility(View.GONE);

                    getGovernates();
                } else if (spinnerC.getSelectedItem().toString().equals("Algérie")) {
                    spinner.setVisibility(View.VISIBLE);
                    spinnerZ.setVisibility(View.VISIBLE);

                    txt_gov.setVisibility(View.GONE);
                    txt_zip.setVisibility(View.GONE);

                    getGovernatesAlg();
                } else {
                    //text rempli manuel
                    spinner.setVisibility(View.GONE);
                    spinnerZ.setVisibility(View.GONE);

                    txt_gov.setVisibility(View.VISIBLE);
                    txt_zip.setVisibility(View.VISIBLE);
                }
                dataAdapterG.notifyDataSetChanged();
                dataAdapterZ.notifyDataSetChanged();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        // Creating adapter for spinner
        dataAdapterC = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, countries) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                applyFontToSpinner((TextView) v);
                return v;
            }

            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                applyFontToSpinner((TextView) v);
                return v;
            }
        };

        // Drop down layout style - list view with radio button
        dataAdapterC.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinnerC.setAdapter(dataAdapterC);
    }

    public void initZipCode() {
        // Spinner element
        spinnerZ = (Spinner) findViewById(R.id.zipcode);
        spinnerZ.setOnItemSelectedListener(this);

        // Creating adapter for spinner
        dataAdapterZ = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, zipCodes) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                applyFontToSpinner((TextView) v);
                return v;
            }

            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                applyFontToSpinner((TextView) v);
                return v;
            }
        };

        // Drop down layout style - list view with radio button
        dataAdapterZ.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinnerZ.setAdapter(dataAdapterZ);
    }

    public boolean isTextValid(String text) {
        return Character.isDigit(text.charAt(0));
    }

    public boolean isMailValid(String email) {
        return EMAIL_ADDRESS_PATTERN.matcher(email).matches();
    }

    @Override
    public void onClick(final View v) {
        if (v == addButton) {
            addMembersLayout();
        } else if (v == btn_subscribe) {
            subscribe();
        } else if (v == btn_return) {
            goBack();
        } else if (v == txt_birthday) {
            showCalendarFor(v);
        }
    }

    public void subscribe() {
        progressDoalog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        progressDoalog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressDoalog.setTitleText("Chargement");
        progressDoalog.setCancelable(true);
        progressDoalog.show();

        View focusView = null;
        boolean cancel = false;

        name = txt_name.getText().toString();
        name2 = txt_name2.getText().toString();
        phone = txt_phone.getText().toString();
        email = txt_email.getText().toString();
        address = txt_address.getText().toString();

        govTxt = txt_gov.getText().toString();
        zipTxt = txt_zip.getText().toString();

        birthday = txt_birthday.getText().toString();
        mCompletePhoneFormatText = mSelectedCountryCode + txt_phone.getText().toString();
        Log.e("TAG", "the phone text is " + mCompletePhoneFormatText);
        nbrEnfant = txt_nbr_enfant.getText().toString();
        password = txt_pwd.getText().toString();
        passwordC = txt_pwdconf.getText().toString();
        check1 = Boolean.valueOf(check_box_cgu.getText().toString());
        check2 = Boolean.valueOf(check_box_mailing.getText().toString());

        check_box_mailing.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    check2 = true;
                } else if (!isChecked) {
                    check2 = false;
                }
            }
        });

        if (TextUtils.isEmpty(name)) {
            txt_name.setError(getString(R.string.error_field_required));
            focusView = txt_name;
            cancel = true;
            progressDoalog.dismiss();

        } else if (isTextValid(name)) {
            txt_name.setError(getString(R.string.error_invalid_nom));
            focusView = txt_name;
            cancel = true;
            progressDoalog.dismiss();
        }

        if (TextUtils.isEmpty(name2)) {
            txt_name2.setError(getString(R.string.error_field_required));
            focusView = txt_name2;
            cancel = true;
            progressDoalog.dismiss();
        } else if (isTextValid(name2)) {
            txt_name2.setError(getString(R.string.error_invalid_nom));
            focusView = txt_name2;
            cancel = true;
            progressDoalog.dismiss();
        }

        if (!(governates.size() > 0) && TextUtils.isEmpty(govTxt)) {
            txt_gov.setError(getString(R.string.error_field_required));
            focusView = txt_gov;
            cancel = true;
            progressDoalog.dismiss();
        }
        if (!(zipCodes.size() > 0) && TextUtils.isEmpty(zipTxt)) {
            txt_zip.setError(getString(R.string.error_field_required));
            focusView = txt_zip;
            cancel = true;
            progressDoalog.dismiss();
        }

        for (int i = 0; i < mCountries.size(); i++) {
            if (mCountries.get(i).getName().equals(spinnerC.getSelectedItem().toString())) {

                Integer l = Integer.valueOf(mCountries.get(i).getPhone());

                if (TextUtils.isEmpty(phone)) {
                    txt_phone.setError(getString(R.string.error_field_required));
                    focusView = txt_phone;
                    cancel = true;
                    progressDoalog.dismiss();
                } else if (txt_phone.length() != l) {

                    if (l == 0 && txt_phone.length() != 0) {

                    } else {
                        txt_phone.setError("le numero de telephone doit etre " + mCountries.get(i).getPhone());
                        focusView = txt_phone;
                        cancel = true;
                        progressDoalog.dismiss();
                        Log.e(TAG, "lenght text is " + txt_phone.length());
                    }
                }
            }
        }

        if (TextUtils.isEmpty(email)) {
            txt_email.setError(getString(R.string.error_field_required));
            focusView = txt_email;
            cancel = true;
            progressDoalog.dismiss();
        } else if (!isMailValid(email)) {
            txt_email.setError(getString(R.string.error_invalid_mail));
            focusView = txt_email;
            cancel = true;
            progressDoalog.dismiss();
        }

        if (TextUtils.isEmpty(address)) {
            txt_address.setError(getString(R.string.error_field_required));
            focusView = txt_address;
            cancel = true;
            progressDoalog.dismiss();
        } else if (isTextValid(address)) {
            txt_address.setError(getString(R.string.error_invalid_nom));
            focusView = txt_address;
            cancel = true;
            progressDoalog.dismiss();
        }

        if (TextUtils.isEmpty(nbrEnfant)) {
            txt_nbr_enfant.setError(getString(R.string.error_field_required));
            focusView = txt_nbr_enfant;
            cancel = true;
            progressDoalog.dismiss();
        }

        if (TextUtils.isEmpty(birthday)) {
            txt_birthday.setError(getString(R.string.error_field_required));
            focusView = txt_birthday;
            cancel = true;
            progressDoalog.dismiss();
        }

        if (age < 18) {
            txt_birthday.setError("Age minimum est 18");
            focusView = txt_birthday;
            cancel = true;
            //progressDoalog.dismiss();
            progressDoalog.setTitleText("Erreur!")
                    .setContentText("Age minimum est 18")
                    .setConfirmText("OK")
                    .changeAlertType(SweetAlertDialog.ERROR_TYPE);
        }

        if (TextUtils.isEmpty(password)) {
            txt_pwd.setError(getString(R.string.error_field_required));
            focusView = txt_pwd;
            cancel = true;
            progressDoalog.dismiss();
        }

        if (TextUtils.isEmpty(passwordC)) {
            txt_pwdconf.setError(getString(R.string.error_field_required));
            focusView = txt_pwdconf;
            cancel = true;
            progressDoalog.dismiss();
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
            Log.e(TAG, "cancel is " + cancel);
        } else {
            if (!txt_pwdconf.getText().toString().equals(txt_pwd.getText().toString())) {
                progressDoalog.setTitleText("Erreur!")
                        .setContentText("Les mots de passe saisis ne sont pas identiques.")
                        .setConfirmText("OK")
                        .changeAlertType(SweetAlertDialog.ERROR_TYPE);
            } else {
                // Instantiate the RequestQueue.
                sendRequest(Constants.subscribeUrl);
            }
        }
    }

    public void sendRequest(String url) {
        try {
            RequestQueue queue = Volley.newRequestQueue(this);
            JSONObject jsonBody = new JSONObject();

            jsonBody.put("firstname", txt_name.getText().toString());
            jsonBody.put("lastname", txt_name2.getText().toString());
            jsonBody.put("mail", txt_email.getText().toString());
            jsonBody.put("gender", spinnerG.getSelectedItem().toString());
            jsonBody.put("address", txt_address.getText().toString());
            mCompletePhoneFormatText = mSelectedCountryCode + txt_phone.getText().toString();
            jsonBody.put("phone", txt_phone.getText().toString());
            jsonBody.put("nbrEnfants", txt_nbr_enfant.getText().toString());
            jsonBody.put("plainPassword", txt_pwd.getText().toString());
            jsonBody.put("birthDay", txt_birthday.getText().toString());

            if (governates.size() > 0) {
                jsonBody.put("governorate", spinner.getSelectedItem().toString());
            } else if (!(governates.size() > 0)) {
                jsonBody.put("governorate", txt_gov.getText().toString());
            }

            if (zipCodes.size() > 0) {
                jsonBody.put("zipCode", spinnerZ.getSelectedItem().toString());
            } else if (!(zipCodes.size() > 0)) {
                jsonBody.put("zipCode", txt_zip.getText().toString());
            }

            jsonBody.put("nationality", spinnerC.getSelectedItem().toString());
            jsonBody.put("clientType", "");
            final String requestBody = jsonBody.toString();

            ///check mailing :
            if (check_box_mailing.isChecked()) {
                check2 = true;
                jsonBody.put("checkMailing", check2);
                Log.e(TAG, "check2 is " + check2);
            } else {
                check2 = false;
                jsonBody.put("checkMailing", check2);
                Log.e(TAG, "check2 is " + check2);
            }

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e(TAG, "Response is " + response);
                    Log.i("VOLLEY", response);
                    parseJSONResponse(response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDoalog.setTitleText("Erreur!")
                            .setContentText("Une erreur est survenue, veuillez essayer plus tard.")
                            .setConfirmText("OK")
                            .changeAlertType(SweetAlertDialog.ERROR_TYPE);
                    Log.e("VOLLEY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }
            };

            queue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void goBack() {
        finish();
    }

    public void addMembersLayout() {

        LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT, 3);
        p.setMargins(0, 0, 0, 0);

        final LinearLayout firstlayout = new LinearLayout(this);
        firstlayout.setOrientation(LinearLayout.HORIZONTAL);
        firstlayout.setWeightSum(3);
        LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT, txt_name.getHeight(), 1.4f);
        params1.setMargins(0, 0, 0, 10);

        final CustomEditText txt_mbr = new CustomEditText(this);
        txt_mbr.setHint("Nom ");
        txt_mbr.setTextSize(14);
        txt_mbr.setPadding(0, 0, 0, 0);
        txt_mbr.setBackgroundResource(R.drawable.textlines);
        firstlayout.addView(txt_mbr, params1);

        LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT, txt_name.getHeight(), 1.5f);
        params2.setMargins(20, 0, 0, 10);

        final CustomEditText txt_date = new CustomEditText(this);
        txt_date.setHint("Date de naissance");
        txt_date.setTextSize(14);
        txt_date.setPadding(0, 0, 0, 0);
        txt_date.setCursorVisible(false);

        txt_date.setFocusable(false);
        txt_date.setBackgroundResource(R.drawable.textlines);
        txt_date.setLayoutParams(params2);
        firstlayout.addView(txt_date);
        txt_date.setOnClickListener(new View.OnClickListener() {
            public void onClick(final View v) {
                showCalendarFor(v);
            }
        });

        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT, 0.05f);
        param.setMargins(5, 0, 0, 0);
        param.gravity = Gravity.CENTER_VERTICAL;
        final ImageView btn_moins = new ImageView(this);
        btn_moins.setBackgroundResource(R.mipmap.ic_minus);

        btn_moins.setOnClickListener(new View.OnClickListener() {
            public void onClick(final View v) {

                firstlayout.removeView(txt_mbr);
                firstlayout.removeView(txt_date);
                firstlayout.removeView(btn_moins);
                txt_mbr.setVisibility(View.GONE);
                txt_date.setVisibility(View.GONE);
                btn_moins.setVisibility(View.GONE);
            }
        });

        firstlayout.addView(btn_moins, param);

        mainLinearLayout.addView(firstlayout, p);

        scrollView.post(new Runnable() {
            @Override
            public void run() {
                scrollView.scrollTo(0, scrollView.getBottom());
            }
        });
    }

    public void showCalendarFor(View v) {
        final Calendar c = Calendar.getInstance();
        //mYear = c.get(Calendar.YEAR);
        mYear = 2001;
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        SupportedDatePickerDialog dpd =
                new SupportedDatePickerDialog(this, R.style.SpinnerDatePickerDialogTheme, this, mYear, mMonth, mDay);
        dpd.getDatePicker().setMaxDate(c.getTimeInMillis());
        dpd.show();
    }

    @Override
    public void onDateSet(DatePicker datePicker, int mYear, int mMonth, int mDay) {
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, mYear);
        c.set(Calendar.MONTH, mMonth);
        c.set(Calendar.DAY_OF_MONTH, mDay);
        txt_birthday.setText(mDay + "-" + (mMonth + 1) + "-" + mYear);

        age = getAge(mYear, mMonth, mDay);
        Log.e("Res Age", "" + age);
    }

    public int getAge(int year, int month, int day) {
        GregorianCalendar cal = new GregorianCalendar();
        int y, m, d, age;
        y = cal.get(Calendar.YEAR);
        m = cal.get(Calendar.MONTH);
        d = cal.get(Calendar.DAY_OF_MONTH);
        cal.set(year, month, day);
        age = y - cal.get(Calendar.YEAR);
        if ((m < cal.get(Calendar.MONTH))
                || ((m == cal.get(Calendar.MONTH)) && (d < cal
                .get(Calendar.DAY_OF_MONTH)))) {
            --age;
        }
        if (age < 0)
            throw new IllegalArgumentException("Age < 0");
        return age;
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public String parseJSONResponse(String jsonResponse) {
        try {
            final JSONObject json = new JSONObject(jsonResponse);

            String code = json.getString("code");
            Log.e("Scan json", String.valueOf(code));

            if (code.equals("201")) {
                JSONObject response = json.getJSONObject("result");
                JSONObject user = response.getJSONObject("client");
                String firstname = user.getString("firstname");
                String lasttname = user.getString("lastname");

                progressDoalog.setTitleText("Succès!")
                        .setContentText("Veuillez consulter votre boite mail pour confirmer votre inscription.")
                        .setConfirmText("OK")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                progressDoalog.dismiss();
                                //Intent myIntent = new Intent(RegisterActivity.this, HomeActivity.class);
                                //startActivity(myIntent);
                                goBack();
                            }
                        })
                        .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);

            } else if (code.equals("400")) {
                progressDoalog.setTitleText("Erreur!")
                        .setContentText("Cet email est déjà utilisé, veuillez essayer avec une autre adresse mail.")
                        .setConfirmText("OK")
                        .changeAlertType(SweetAlertDialog.ERROR_TYPE);
                String msgErr = json.getString("message");
                Log.e("msgErr ", msgErr);

            } else {
                progressDoalog.setTitleText("Erreur!")
                        .setContentText("Une erreur est survenue, veuillez essayer plus tard.")
                        .setConfirmText("OK")
                        .changeAlertType(SweetAlertDialog.ERROR_TYPE);
                String msgErr = json.getString("message");
                Log.e("msgErr ", msgErr);
            }
            return code;
        } catch (JSONException ex) {
            ex.printStackTrace();
            return "250";
        }
    }

    private void applyFontToSpinner(TextView v) {
        Typeface externalFont = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");
        v.setTypeface(externalFont);
    }

    public void getGovernates() {
        Log.e("Register ", "get all governates");
        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(this);

// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constants.govUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Register ", "governates " + response);
                        try {
                            JSONArray json = new JSONArray(response);
                            for (int i = 0; i < json.length(); i++) {
                                JSONObject row = json.getJSONObject(i);
                                Log.e("governates ", "Response is" + row.getString("governorate"));
                                governates.add(row.getString("governorate"));

                            }
                            dataAdapterG.notifyDataSetChanged();
                        } catch (JSONException ex) {
                            ex.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Register ", "Error is" + error.toString());
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    public void getGovernatesAlg() {
        Log.e("Register ", "get all governates alg");
        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(this);

// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constants.govUrlAlg,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Register ", "governates alg" + response);
                        try {
                            JSONArray json = new JSONArray(response);
                            for (int i = 0; i < json.length(); i++) {
                                JSONObject row = json.getJSONObject(i);
                                Log.e("governates ", "Response is" + row.getString("nom"));
                                governates.add(row.getString("nom"));

                            }
                            dataAdapterG.notifyDataSetChanged();
                        } catch (JSONException ex) {
                            ex.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Register ", "Error is" + error.toString());
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    public void getZipCodesForGov(String gov) {
        Log.e("Register ", "get all zipCodes " + gov);
        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(this);

// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constants.zipCodesUrl + "?governorate=" + gov,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Register ", "zipCodes " + response);
                        try {
                            JSONArray json = new JSONArray(response);
                            for (int i = 0; i < json.length(); i++) {
                                JSONObject row = json.getJSONObject(i);
                                Log.e("zipCodes ", "Response is" + row.getString("zipCode"));
                                zipCodes.add(row.getString("zipCode"));

                            }
                            dataAdapterZ.notifyDataSetChanged();

                        } catch (JSONException ex) {
                            ex.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Register ", "Error is" + error.toString());
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    public void getZipCodesForGovAlg(String gov) {
        Log.e("Register ", "get all zipCodes " + gov);
        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(this);

// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constants.zipCodesUrlAlg + "?governorate=" + gov,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Register ", "zipCodesAlg " + response);
                        try {
                            JSONArray json = new JSONArray(response);
                            for (int i = 0; i < json.length(); i++) {
                                JSONObject row = json.getJSONObject(i);
                                Log.e("zipCodesAlg ", "Response is" + row.getString("codePostal"));
                                zipCodes.add(row.getString("codePostal"));

                            }
                            dataAdapterZ.notifyDataSetChanged();

                        } catch (JSONException ex) {
                            ex.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Register ", "Error is" + error.toString());
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    public void getCountries() {
        Log.e("Register ", "get all countries");
        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(this);

// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constants.countriesUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Register ", "Response is" + response);
                        try {
                            JSONArray json = new JSONArray(response);
                            for (int i = 0; i < json.length(); i++) {
                                JSONObject row = json.getJSONObject(i);
                                Log.e("country ", "Response is" + row.getString("nomFrFr"));
                                Log.e("country ", "Response is" + row.getString("callingCode"));
                                Log.e("country ", "Response is" + row.getString("phoneLength"));
                                Country countryItem = new Country();
                                countryItem.setName(row.getString("nomFrFr"));
                                countryItem.setCode(row.getString("callingCode"));
                                countryItem.setPhone(row.getString("phoneLength"));
                                mCountries.add(countryItem);
                                countries.add(row.getString("nomFrFr"));
                                mCountriesCodes.add(row.getString("callingCode"));

                            }

                            Log.e("TAG", "the country list size is " + mCountries.size());

                            dataAdapterC.notifyDataSetChanged();

                        } catch (JSONException ex) {
                            ex.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Register ", "Error is" + error.toString());
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

}