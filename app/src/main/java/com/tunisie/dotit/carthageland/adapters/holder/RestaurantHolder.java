package com.tunisie.dotit.carthageland.adapters.holder;

import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.tunisie.dotit.carthageland.R;
import com.tunisie.dotit.carthageland.Utils.Constants;
import com.tunisie.dotit.carthageland.custom.CustomTextView;
import com.tunisie.dotit.carthageland.models.POI;
import com.tunisie.dotit.carthageland.models.Restaurant;
import com.squareup.picasso.Picasso;


/**
 * Created on 2/15/18.
 */

public class RestaurantHolder extends BaseHolder {

    TextView mTitle, mDetails;
    public ImageView mImageView;

    public RestaurantHolder(View itemView) {
        super(itemView);
        mTitle = itemView.findViewById(R.id.tv_restaurant_title);
        mDetails = itemView.findViewById(R.id.tv_restaurant_details);
        mImageView = itemView.findViewById(R.id.iv_restaurant);
    }

    public void bind(final POI menuItem) {
        mTitle.setText(menuItem.getName());
        //mDetails.setText(menuItem.getDescription());
        if (menuItem.getFilename() != null) {

            Picasso.with(com.tunisie.dotit.carthageland.application.BaseApplication.getInstance()).load(Constants.poiImmagesUrl + menuItem.getFilename()).placeholder(R.drawable.placeholder).resize(400,400).centerCrop().into(mImageView);
        }
    }

}
