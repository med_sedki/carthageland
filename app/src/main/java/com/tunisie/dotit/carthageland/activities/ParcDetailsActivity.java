package com.tunisie.dotit.carthageland.activities;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.squareup.picasso.Picasso;
import com.tunisie.dotit.carthageland.R;
import com.tunisie.dotit.carthageland.Utils.Constants;
import com.tunisie.dotit.carthageland.adapters.AttractionAdapter;
import com.tunisie.dotit.carthageland.adapters.ImageAdapter;
import com.tunisie.dotit.carthageland.adapters.TransformerAdapter;
import com.tunisie.dotit.carthageland.custom.CustomTypefaceSpan;
import com.tunisie.dotit.carthageland.global.APIClient;
import com.tunisie.dotit.carthageland.global.APIInterface;
import com.tunisie.dotit.carthageland.global.AppUtils;
import com.tunisie.dotit.carthageland.global.AttractionItemClickListener;
import com.tunisie.dotit.carthageland.models.POI;
import com.tunisie.dotit.carthageland.models.ParcPhotoResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;

public class ParcDetailsActivity extends BaseActivity implements BaseSliderView.OnSliderClickListener, AttractionItemClickListener, ViewPagerEx.OnPageChangeListener {

    private static DrawerLayout mDrawer;
    public static Toolbar toolbar;
    public static String SelectedAttractionName;
    private APIInterface mApiInterface;
    TextView mTitle, mDetails;
    public ImageView mImageView;
    LinearLayout mMainLayout;
    private SliderLayout mDemoSlider;
    public static String SelectedParcCode;
    private ImageView mPoiImage;
    private TextView mPoiTitle, mPoiDescription;
    private APIInterface mApiInerface;
    HashMap<String, String> mUrlMaps = new HashMap<String, String>();
    private RecyclerView mAttractionRecycler;
    private ArrayList<String> mImageslist = new ArrayList();
    private BottomSheetBehavior mBottomSheetBehavior;
    int mCurrentPage = 0;
    Timer mTime;
    final long DELAY_MS = 500;//delay in milliseconds before task is to be executed
    final long PERIOD_MS = 3000; // time in milliseconds between successive task executions.
    private ArrayList<POI> mAttractionsList = new ArrayList<>();
    int width, height;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parc_detail);
        if (getPreferences().getConnectedMode().equals("true")) {
            Log.e("TAG", "the client id is " + getPreferences().getUser().getId_client() + "the token is " + getPreferences().getToken());
        }

        DisplayMetrics displayMetrics = this.getResources().getDisplayMetrics();
        width = displayMetrics.widthPixels;
        height = displayMetrics.heightPixels;

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Toolbar toolbarTop = (Toolbar) findViewById(R.id.toolbar);
        mTitle = (TextView) toolbarTop.findViewById(R.id.toolbar_title);
        mTitle.setText(SelectedAttractionName);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationIcon(R.drawable.back_arrow_ic);
        View bottomSheet = findViewById(R.id.bottom_sheet);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                return false;
            }
        });

        mTitle = findViewById(R.id.parc_title);
        // mImageView = findViewById(R.id.iv_restaurant);
        mDetails = findViewById(R.id.parc_description);
        mMainLayout = findViewById(R.id.main_parc_layout);
        mAttractionRecycler = findViewById(R.id.attractions_recycler);

        getProductsList(SelectedParcCode);
        mPoiImage = findViewById(R.id.poi_image_description);
        mPoiTitle = findViewById(R.id.poi_title);
        mPoiDescription = findViewById(R.id.poi_description);
        mDemoSlider = findViewById(R.id.slider);

        mBottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
        mBottomSheetBehavior.setHideable(true);
        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
                    mBottomSheetBehavior.setPeekHeight(height - (toolbar.getHeight() + 60));
                }
            }

            @Override
            public void onSlide(View bottomSheet, float slideOffset) {
            }
        });


        /*mTitle.setText(menuItem.getName());
        mDetails.setText(menuItem.getDescription());
        if (menuItem.getFilename() != null) {

            Picasso.with(com.tunisie.dotit.carthageland.application.BaseApplication.getInstance()).load(Constants.parcsImagesUrl + menuItem.getFilename()).placeholder(R.drawable.placeholder_attraction).into(mImageView);
        }*/


        /*getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(false);
        drawerToggle.setDrawerIndicatorEnabled(false);
        drawerToggle.setHomeAsUpIndicator(R.drawable.ic_custom_drawer_icon);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawer, null,
                0, 0);*/


       /* mUrlMaps.put("Hannibal", "http://static2.hypable.com/wp-content/uploads/2013/12/hannibal-season-2-release-date.jpg");
        mUrlMaps.put("Big Bang Theory", "http://tvfiles.alphacoders.com/100/hdclearart-10.png");
        mUrlMaps.put("House of Cards", "http://cdn3.nflximg.net/images/3093/2043093.jpg");
        mUrlMaps.put("Game of Thrones", "http://images.boomsbeat.com/data/images/full/19640/game-of-thrones-season-4-jpg.jpg");*/

      /*  HashMap<String,Integer> file_maps = new HashMap<String, Integer>();
        file_maps.put("Hannibal",R.drawable.hannibal);
        file_maps.put("Big Bang Theory",R.drawable.bigbang);
        file_maps.put("House of Cards",R.drawable.house);
        file_maps.put("Game of Thrones", R.drawable.game_of_thrones);*/


    }

    private void setBottomSheetData(POI poi, int i) {

        String poiName = poi.getName();
        String poiDescription = poi.getDescription();
        mPoiTitle.setText(poiName);
        mPoiDescription.setText(poiDescription);
        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

        if (poi.getFilename() != null) {
            String poiImage = poi.getFilename();
            Picasso.with(ParcDetailsActivity.this).load(Constants.poiImmagesUrl + poiImage).placeholder(R.drawable.placeholder).centerInside().fit().into(mPoiImage);
        }
    }

    private void setImageSlider() {
        for (String name : mUrlMaps.keySet()) {
            TextSliderView textSliderView = new TextSliderView(this);
            // initialize a SliderLayout
            textSliderView
                    .description(name)
                    .image(mUrlMaps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.CenterCrop)
                    .setOnSliderClickListener(this);

            //add your extra information
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra", name);

            mDemoSlider.addSlider(textSliderView);
        }
        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Default);
        mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        mDemoSlider.setCustomAnimation(new DescriptionAnimation());
        mDemoSlider.setDuration(3000);
        mDemoSlider.addOnPageChangeListener(this);
        ListView l = (ListView) findViewById(R.id.transformers);
        l.setAdapter(new TransformerAdapter(this));
        l.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mDemoSlider.setPresetTransformer(((TextView) view).getText().toString());
                Toast.makeText(ParcDetailsActivity.this, ((TextView) view).getText().toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void applyFontToMenuItem(MenuItem mi) {
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("", font), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }

    public void getProductsList(String code) {
        mApiInerface = APIClient.getClient().create(APIInterface.class);

        showProgressBar();

        /**
         GET List Resources
         **/
        Call<ParcPhotoResponse> call = mApiInerface.getParcPhotos(getPreferences().getToken(), code, "ATTRACTION");
        call.enqueue(new Callback<ParcPhotoResponse>() {
            @Override
            public void onResponse(Call<ParcPhotoResponse> call, retrofit2.Response<ParcPhotoResponse> response) {

                //  Log.d("TAG", response.body().getResponse() + " response get poi ");
                Log.d("TAG", response + "");
                if (response.isSuccessful() && response.body() != null && response.body().getResponse() != null) {
                    if (!response.body().getResponse().isEmpty()) {
                        for (int i = 0; i < response.body().getResponse().get(0).getPois().size(); i++) {

                            String photo = null;
                            if (response.body() != null) {
                                photo = response.body().getResponse().get(0).getPois().get(i).getFilename();
                            }
                            Log.e("TAG", photo);
                            mUrlMaps.put("photo" + i, Constants.poiImmagesUrl + photo);
                            mImageslist.add(Constants.poiImmagesUrl + photo);
                            mAttractionsList.add(response.body().getResponse().get(0).getPois().get(i));
                            if (response.body().getResponse().get(0).getName() != null) {
                                mTitle.setText(response.body().getResponse().get(0).getName());
                                if (response.body().getResponse().get(0).getPois().size() > 1) {
                                    setAttractionsList();
                                }
                            }
                            if (response.body().getResponse().get(0).getDescription() != null) {
                                mDetails.setText(response.body().getResponse().get(0).getDescription());
                                mDetails.setVisibility(View.VISIBLE);
                            }
                        }
                        hideProgressBar();
                        //setImageSlider();

                        setViewPager();
                        SelectedParcCode = "";
                        SelectedAttractionName = "";
                    } else {
                        hideProgressBar();
                        Toast.makeText(ParcDetailsActivity.this, getString(R.string.error_empty_list), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(ParcDetailsActivity.this, getString(R.string.error_server), Toast.LENGTH_SHORT).show();
                    hideProgressBar();
                }
            }

            @Override
            public void onFailure(Call<ParcPhotoResponse> call, Throwable t) {
                hideProgressBar();
                call.cancel();
            }
        });
    }

    private void setAttractionsList() {
        AttractionItemClickListener attractionItemClickListener = new AttractionItemClickListener() {
            @Override
            public void onItemClicked(POI parc) {
                setBottomSheetData(parc, 0);
            }
        };
        mAttractionRecycler.setLayoutManager(new LinearLayoutManager(ParcDetailsActivity.this, LinearLayoutManager.VERTICAL, false));

        mAttractionRecycler.setAdapter(new AttractionAdapter(mAttractionsList, ParcDetailsActivity.this, attractionItemClickListener));
        mAttractionRecycler.setVisibility(View.VISIBLE);
    }


    private void setViewPager() {
        final ViewPager mViewPager = findViewById(R.id.viewPageAndroid);
        ImageAdapter adapterView = new ImageAdapter(ParcDetailsActivity.this, mImageslist);
        mViewPager.setAdapter(adapterView);
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (mCurrentPage == mViewPager.getAdapter().getCount() - 1) {
                    mCurrentPage = 0;
                }
                mViewPager.setCurrentItem(mCurrentPage++, true);
            }
        };
        mTime = new Timer(); // This will create a new Thread
        mTime.schedule(new TimerTask() { // task to be scheduled

            @Override
            public void run() {
                handler.post(Update);
            }
        }, DELAY_MS, PERIOD_MS);
    }

    @Override
    protected void onPause() {
        hideKeyboard();
        super.onPause();
    }

    public void hideKeyboard() {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
            Log.d("TAG", "Could not hide keyboard, window unreachable. " + e.toString());
        }
    }


    public boolean isNetworkAvailable() {
        return AppUtils.isNetworkAvailable(this);
    }


    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onItemClicked(POI parc) {
        setBottomSheetData(parc, 0);
    }
}