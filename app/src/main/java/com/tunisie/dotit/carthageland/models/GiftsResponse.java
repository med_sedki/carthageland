package com.tunisie.dotit.carthageland.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GiftsResponse {
    @SerializedName("code")
    private Integer code;
    @SerializedName("message")
    private String message;
    @SerializedName("response")
    private List<Product> response;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public List<Product> getOfferList() {
        return response;
    }

    public void setOfferList(List<Product> pois) {
        this.response = pois;
    }

}
