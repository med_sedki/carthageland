package com.tunisie.dotit.carthageland.activities;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabSelectListener;
import com.tunisie.dotit.carthageland.R;
import com.tunisie.dotit.carthageland.Utils.Constants;
import com.tunisie.dotit.carthageland.application.BaseApplication;
import com.tunisie.dotit.carthageland.custom.CustomTypefaceSpan;
import com.tunisie.dotit.carthageland.fragments.BarCodeFragment;
import com.tunisie.dotit.carthageland.fragments.FamilyFragment;
import com.tunisie.dotit.carthageland.fragments.GiftsFragment;
import com.tunisie.dotit.carthageland.fragments.HistoryFragment;
import com.tunisie.dotit.carthageland.fragments.HomeFragment;
import com.tunisie.dotit.carthageland.fragments.InfosFragment;
import com.tunisie.dotit.carthageland.fragments.InviteFragment;
import com.tunisie.dotit.carthageland.fragments.NotificationsFragment;
import com.tunisie.dotit.carthageland.fragments.ParcChoiceHmammametFragment;
import com.tunisie.dotit.carthageland.fragments.ParcChoiceTunisFragment;
import com.tunisie.dotit.carthageland.fragments.ParcsFragment;
import com.tunisie.dotit.carthageland.fragments.PlanFragment;
import com.tunisie.dotit.carthageland.fragments.ProfilFragment;
import com.tunisie.dotit.carthageland.fragments.ReclamationFragment;
import com.tunisie.dotit.carthageland.fragments.RestaurantsFragment;
import com.tunisie.dotit.carthageland.fragments.SettingsFragment;
import com.tunisie.dotit.carthageland.fragments.SmilesFragment;
import com.tunisie.dotit.carthageland.global.APIClient;
import com.tunisie.dotit.carthageland.global.APIInterface;
import com.tunisie.dotit.carthageland.global.AppUtils;
import com.tunisie.dotit.carthageland.models.LogOutResponse;
import com.tunisie.dotit.carthageland.models.PoiResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends BaseActivity implements View.OnClickListener {

    private static DrawerLayout mDrawer;
    public static Toolbar toolbar;
    private NavigationView nvDrawer;
    private TextView btn_notif;
    private TextView profile_name;
    TextView mTitle;
    private NavigationView mNavigationView;
    private BottomBar mBottomBar;
    APIInterface mApiInerface;

    Fragment fragment = null;

    // Make sure to be using android.support.v7.app.ActionBarDrawerToggle version.
    // The android.support.v4.app.ActionBarDrawerToggle has been deprecated.
    private ActionBarDrawerToggle drawerToggle;
    BaseApplication myApp;
    ActionBarDrawerToggle mDrawerToggle;
    public String iSconnected = "";
    private TextView mMessagesIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        Intent intent = getIntent();
        iSconnected = intent.getStringExtra("iSconnected");
        if (iSconnected.equals("false")) {
            getPreferences().setConnectedMode("false");
        } else if (iSconnected.equals("true")) {
            getPreferences().setConnectedMode("true");
            Log.e("TAG", "the client id is " + getPreferences().getUser().getId_client() + "the token is " + getPreferences().getToken());
        }

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Toolbar toolbarTop = findViewById(R.id.toolbar);
        mTitle = toolbarTop.findViewById(R.id.toolbar_title);
        mMessagesIcon = toolbarTop.findViewById(R.id.btn_notif);
        mTitle.setText("Carthage Land");
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                return false;
            }
        });

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawer, null,
                0, 0);
        mNavigationView = findViewById(R.id.nvView);
        View headerView = mNavigationView.inflateHeaderView(R.layout.nav_header);
        profile_name = headerView.findViewById(R.id.profile_name);

        btn_notif = findViewById(R.id.btn_notif);
        btn_notif.setOnClickListener(this);

        // Find our drawer view
        mDrawer = findViewById(R.id.drawer_layout);
        nvDrawer = findViewById(R.id.nvView);
        setupDrawerContent(nvDrawer);

        setConnectedMode();

        if (isNetworkAvailable()) {
            if (getPreferences().getHammametPois() == null && getPreferences().getTunisPois() == null) {
                Log.e("TAG", "the pois are lodaed now ");
                getPOIList(Constants.Sites.HHAMMAMET_PARAM);
                getPOIList(Constants.Sites.TUNIS_PARAM);
            } else {
                Log.e("TAG", "the pois are already loaded ");
            }
        } else {
            finish();
            Log.e("TAG", "the home activity is finished here in this home point");
        }


        Menu m = nvDrawer.getMenu();
        for (int i = 0; i < m.size(); i++) {
            MenuItem mi = m.getItem(i);

            //for aapplying a font to subMenu ...
            SubMenu subMenu = mi.getSubMenu();
            if (subMenu != null && subMenu.size() > 0) {
                for (int j = 0; j < subMenu.size(); j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem);
                }
            }
            //the method we have create in activity
            applyFontToMenuItem(mi);
        }

        goToPlan(HomeFragment.class);

        mBottomBar = findViewById(R.id.bottomBar);
        mBottomBar.getCurrentTab().setSelected(false);
        mBottomBar.getCurrentTab().setActivated(false);
        mBottomBar.getCurrentTab().setVisibility(View.GONE);
        mBottomBar.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelected(@IdRes int tabId) {

                Fragment fragment = null;
                Class fragmentClass = null;
                switch (tabId) {
                    case R.id.none:
                        getPreferences().setCarthageLandLocation("CLHammamet");
                        //fragmentClass = PlanFragment.class;
                        break;
                    case R.id.tab_infos_pratiques:
                        fragmentClass = InfosFragment.class;
                        setAllItemsUnchecked();
                        break;
                    case R.id.tab_smiles:
                        fragmentClass = SmilesFragment.class;
                        setAllItemsUnchecked();
                        break;
                    case R.id.tab_barcode:
                        fragmentClass = BarCodeFragment.class;
                        setAllItemsUnchecked();
                        break;
                    default:
                        fragmentClass = HomeFragment.class;
                        break;
                }

                if (fragmentClass != null) {
                    goToPlan(fragmentClass);
                }

                mDrawer.closeDrawers();

            }
        });
        configureConnectedMode();
    }

    private void setConnectedMode() {
        if (iSconnected.equals("false")) {

            getPreferences().setConnectedMode("false");
            toolbar.setNavigationIcon(null);
            //drawerToggle.setDrawerIndicatorEnabled(false);
            mDrawerToggle.setDrawerIndicatorEnabled(false);
            toolbar.setOverflowIcon(null);
            Menu navigationMenu = mNavigationView.getMenu();
            navigationMenu.findItem(R.id.nav_logout).setVisible(false);

            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, 0, 0);
            drawer.setDrawerListener(toggle);
            toggle.syncState();
            toggle.setDrawerIndicatorEnabled(false);


        } else if (iSconnected.equals("true")) {
            getPreferences().setConnectedMode("true");
            Log.e("TAG", "the client id is " + getPreferences().getUser().getId_client() + "the token is " + getPreferences().getToken());
            profile_name.setText(getPreferences().getUser().getNom() + " " + getPreferences().getUser().getPrenom());
        }
    }

    public static void openDrawer() {
        mDrawer.openDrawer(GravityCompat.START);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // The action bar ic_home/up action should open or close the drawer.
        switch (item.getItemId()) {
            case android.R.id.home:
                if (getPreferences().getConnectedMode().equals("true")) {
                    mDrawer.openDrawer(GravityCompat.START);

                }

                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        selectDrawerItem(menuItem);
                        return true;
                    }
                });
    }

    private void configureConnectedMode() {
        if (getPreferences().getConnectedMode().equals("false")) {
            //mDrawer.setVisibility(View.INVISIBLE);
            mBottomBar.setVisibility(View.GONE);
            mNavigationView.setVisibility(View.GONE);
            mMessagesIcon.setVisibility(View.INVISIBLE);
            toolbar.setNavigationIcon(null);
            final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, 0, 0);
            drawer.setDrawerListener(toggle);
            toggle.syncState();
            toggle.setDrawerIndicatorEnabled(false);
            toggle.getDrawerArrowDrawable().setVisible(false, false);

            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            getSupportActionBar().setHomeAsUpIndicator(null);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }
    }

    public void getPOIList(final String site_praram) {
        mApiInerface = APIClient.getClient().create(APIInterface.class);

        showProgressBar();
        /**
         GET List Resources
         **/
        Call<PoiResponse> call = mApiInerface.getPoiList(getPreferences().getToken(), site_praram);
        call.enqueue(new Callback<PoiResponse>() {
            @Override
            public void onResponse(Call<PoiResponse> call, Response<PoiResponse> response) {

                if (response.isSuccessful() && response.body() != null && response.body().getResponse() != null) {
                    if (!response.body().getResponse().isEmpty()) {
                        Log.d("TAG", response.body().getResponse() + " response get poi ");
                        Log.d("TAG", response + "");
                        if (site_praram.equals(Constants.Sites.HHAMMAMET_PARAM)) {

                            getPreferences().saveHammametPois(response.body().getResponse());
                            for (int i = 0; i < getPreferences().getHammametPois().size(); i++) {
                                Log.e("TAG", getPreferences().getHammametPois().get(i).getCategory().getName().toString());

                            }
                        } else {
                            getPreferences().saveTunisPois(response.body().getResponse());
                            for (int i = 0; i < getPreferences().getTunisPois().size(); i++) {
                                Log.e("TAG", getPreferences().getTunisPois().get(i).getCategory().getName().toString());

                            }
                        }
                    } else {
                        hideProgressBar();
                        Toast.makeText(HomeActivity.this, getString(R.string.error_empty_list), Toast.LENGTH_SHORT).show();
                    }
                } else {

                    Toast.makeText(HomeActivity.this, getString(R.string.error_server), Toast.LENGTH_SHORT).show();
                }

                hideProgressBar();
            }

            @Override
            public void onFailure(Call<PoiResponse> call, Throwable t) {
                call.cancel();
                hideProgressBar();
            }
        });
        toolbar.setNavigationIcon(null);
    }

    public void selectDrawerItem(MenuItem menuItem) {
        // Create a new fragment and specify the fragment to show based on nav item clicked
        Fragment fragment = null;
        Class fragmentClass;
        switch (menuItem.getItemId()) {
            case R.id.nav_home:
                fragmentClass = HomeFragment.class;
                if (getPreferences().getConnectedMode().equals("true")) {
                    toolbar.setNavigationIcon(R.drawable.drawer_menu_ic);
                }
                mBottomBar.selectTabAtPosition(0);
                break;
            case R.id.nav_reclamation:
                fragmentClass = ReclamationFragment.class;
                mBottomBar.selectTabAtPosition(0);
                break;
            /*case R.id.nav_photos:
                fragmentClass = PhotosFragment.class;
                break;*/
            case R.id.nav_invite:
                fragmentClass = InviteFragment.class;
                mBottomBar.selectTabAtPosition(0);
                break;
            /*case R.id.gifts_list:
                fragmentClass = GiftsFragment.class;
                mBottomBar.selectTabAtPosition(0);
                break;
            case R.id.nav_share:
                fragmentClass = ShareFragment.class;
                break;*/
            case R.id.nav_profil:
                fragmentClass = ProfilFragment.class;
                mBottomBar.selectTabAtPosition(0);
                break;
            case R.id.nav_settings:
                fragmentClass = SettingsFragment.class;
                mBottomBar.selectTabAtPosition(0);
                break;
            case R.id.nav_logout:
                showDialog("Déconnexion", "Êtes-vous sûr de vouloir vous déconnecter ?");
                fragmentClass = null;
                break;
            default:
                fragmentClass = HomeFragment.class;
                break;
        }

        goToPlan(fragmentClass);

        // Highlight the selected item has been done by NavigationView
        menuItem.setChecked(true);
        // Set action bar title
        //setTitle(menuItem.getTitle());
        // Close the navigation drawer
        mDrawer.closeDrawers();
    }

    public void showDialog(String title, CharSequence message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        if (title != null) builder.setTitle(title);

        builder.setMessage(message);
        builder.setPositiveButton("Déconnecter", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                quitAction();
            }

            ;
        });
        builder.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    private void quitAction() {
        BaseApplication.getInstance().clearUserCache();
        Intent intent = new Intent(HomeActivity.this, HomeActivity.class);
        getPreferences().setConnectedMode("false");
        logOut();
        intent.putExtra("iSconnected", "false");
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_top, R.anim.no);
    }

    public void logOut() {
        mApiInerface = APIClient.getClient().create(APIInterface.class);


        /**
         GET List Resources
         **/
        Call<LogOutResponse> call = mApiInerface.logOut(getPreferences().getToken());
        call.enqueue(new Callback<LogOutResponse>() {
            @Override
            public void onResponse(Call<LogOutResponse> call, retrofit2.Response<LogOutResponse> response) {


                //  Log.d("TAG", response.body().getResponse() + " response get poi ");
                Log.e("TAG", "products " + response + "");
                if (response.isSuccessful() && response.body() != null) {


                    Log.e("TAG", " the log out API is successful");
                }

            }

            @Override
            public void onFailure(Call<LogOutResponse> call, Throwable t) {

                call.cancel();
            }
        });
    }

    public void goToPlan(Class fragmentClass) {
        Fragment fragment = null;
        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Insert the fragment by replacing any existing fragment
        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.flContent, fragment).addToBackStack(null).commit();
            FragmentManager secondFragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.flContent, fragment);
            fragmentTransaction.addToBackStack(null); //this will add it to back stack
            fragmentTransaction.commit();

        }
        if (fragmentClass == HomeFragment.class) {
            // setTitle("Accueil");

            if (getPreferences().getConnectedMode().equals("true")) {

                toolbar.setNavigationIcon(R.drawable.drawer_menu_ic);
            }
        }
    }

    private void applyFontToMenuItem(MenuItem mi) {
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/avenirnextpro_mediumcn.otf");
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("", font), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }

    @Override
    protected void onPause() {
        hideKeyboard();
        super.onPause();
    }

    public void hideKeyboard() {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
            Log.d("TAG", "Could not hide keyboard, window unreachable. " + e.toString());
        }
    }


    public boolean isNetworkAvailable() {
        return AppUtils.isNetworkAvailable(this);
    }

    @Override
    public void onBackPressed() {
       /* DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            FragmentManager manager = getSupportFragmentManager();
            if (manager.getBackStackEntryCount() > 0) {
               // super.onBackPressed();
                if (manager.findFragmentById(R.id.flContent) != null) {
                    Fragment currentFragment = manager.findFragmentById(R.id.flContent);
                    Log.e("TAG", "the current fragment is " + currentFragment.getClass().toString());
                    setBackPressedEvent(currentFragment);
                  //  super.onBackPressed();
                } else {
                    finish();
                } //setBackPressedEvent(currentFragment);
            } else {
                finish();
            }
        }*/

        /* if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            getSupportFragmentManager().popBackStack();
            mBottomBar.selectTabAtPosition(0);
            FragmentManager manager = getSupportFragmentManager();
            if (manager.getBackStackEntryCount() > 0) {
                // super.onBackPressed();
                if (manager.findFragmentById(R.id.flContent) != null) {
                    Fragment currentFragment = manager.findFragmentById(R.id.flContent);
                    Log.e("TAG", "the current fragment is " + currentFragment.getClass().toString());
                    if (currentFragment instanceof PlanFragment) {
                        goToPlan(HomeFragment.class);
                        //setTitle("Accueil");
                    }
                    //  super.onBackPressed();
                } else {
                    finish();
                } //setBackPressedEvent(currentFragment);
            }
        } else {
            finish();
        }*/

        /*mBottomBar.selectTabAtPosition(0);

        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            FragmentManager manager = getSupportFragmentManager();
            Fragment currentFragment = manager.findFragmentById(R.id.flContent);
            Log.e("TAG", "the current fragment is " + currentFragment.getClass().toString());
            if (currentFragment instanceof HomeFragment) {
                finish();
                Log.e("TAG", "the home activity is finished here in this home point");

                //setTitle("Accueil");
            } else if (currentFragment instanceof ParcChoiceTunisFragment) {
                //finish();
                //goToPlan(HomeFragment.class);
                goToPlan(RegisterActivity.class);

            } else {
                getSupportFragmentManager().popBackStack();
            }

            setAllItemsUnchecked();
            //mBottomBar.selectTabAtPosition(0);
            super.onBackPressed();
        } else {
            finish();
            Log.e("TAG", "the home activity is finished here in this home point");
        }*/
        super.onBackPressed();
    }

    private void setBackPressedEvent(Fragment currentFragment) {
        if (currentFragment instanceof HomeFragment) {
            mNavigationView.getMenu().getItem(0).setChecked(true);
            mBottomBar.selectTabAtPosition(0);
            //setTitle("Accueil");
        } else if (currentFragment instanceof ProfilFragment) {
            mNavigationView.getMenu().getItem(1).setChecked(true);
            mBottomBar.selectTabAtPosition(0);
            //setTitle("Profil");
        } else if (currentFragment instanceof ReclamationFragment) {
            mNavigationView.getMenu().getItem(2).setChecked(true);
            mBottomBar.selectTabAtPosition(0);
            //setTitle("Réclamation");
        } else if (currentFragment instanceof InviteFragment) {
            mNavigationView.getMenu().getItem(3).setChecked(true);
            mBottomBar.selectTabAtPosition(0);
            //setTitle("Inviter des amis");
        } else if (currentFragment instanceof GiftsFragment) {
            mNavigationView.getMenu().getItem(4).setChecked(true);
            mBottomBar.selectTabAtPosition(0);
            //setTitle("Réclamation");
        } else if (currentFragment instanceof FamilyFragment) {
            mNavigationView.getMenu().getItem(5).setChecked(true);
            mBottomBar.selectTabAtPosition(0);
            //setTitle("Mon Espace famillial");
        } else if (currentFragment instanceof SettingsFragment) {
            mNavigationView.getMenu().getItem(6).setChecked(true);
            mBottomBar.selectTabAtPosition(0);
            //setTitle("Paramètres");
        } else if (currentFragment instanceof HistoryFragment) {
            setAllItemsUnchecked();
            mBottomBar.selectTabAtPosition(0);
            //setTitle("Historique des achats");
        } else if (currentFragment instanceof ParcChoiceHmammametFragment) {
            setAllItemsUnchecked();
            mBottomBar.selectTabAtPosition(0);
            //setTitle("Accueil");
        } else if (currentFragment instanceof RestaurantsFragment) {
            setAllItemsUnchecked();
            mBottomBar.selectTabAtPosition(0);
            //setTitle("Restaurants");
        } else if (currentFragment instanceof ParcsFragment) {
            setAllItemsUnchecked();
            mBottomBar.selectTabAtPosition(0);
            //setTitle("Parcs");
        } else if (currentFragment instanceof PlanFragment) {
            setAllItemsUnchecked();
            mBottomBar.selectTabAtPosition(0);
            //setTitle("Carte Carthage Land");
        }

        /*else if (currentFragment instanceof ParcsFragment) {
            setAllItemsUnchecked();
            mBottomBar.selectTabAtPosition(0);
            //setTitle("Parcs");
            currentFragment = new HomeFragment() ;
            goToPlan(currentFragment.getClass());
        }  else if (currentFragment instanceof ParcChoiceHmammametFragment) {
            mNavigationView.getMenu().getItem(0).setChecked(true);
            mBottomBar.selectTabAtPosition(0);
            setTitle("Accueil");
        }   else if (currentFragment instanceof RestaurantsFragment) {
            setAllItemsUnchecked();
            //mBottomBar.selectTabAtPosition(0);
            setTitle("Restaurants");
            currentFragment = new HomeFragment() ;
            goToPlan(currentFragment.getClass());
        } */

        else if (currentFragment instanceof BarCodeFragment) {
            mBottomBar.selectTabAtPosition(0);
            setAllItemsUnchecked();
        } else if (currentFragment instanceof SmilesFragment) {
            mBottomBar.selectTabAtPosition(0);
            setAllItemsUnchecked();
        } else if (currentFragment instanceof InfosFragment) {
            mBottomBar.selectTabAtPosition(0);
            setAllItemsUnchecked();
        } else if (currentFragment == null) {
            finish();
        }
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        } else {
            this.finish();
            Log.e("TAG", "the home activity is finished here in this home point");
        }
    }

    private void setAllItemsUnchecked() {
        int size = mNavigationView.getMenu().size();
        for (int i = 0; i < size; i++) {
            mNavigationView.getMenu().getItem(i).setChecked(false);
        }
    }

    @Override
    public void onClick(View v) {
        if (v == btn_notif) {
            goToPlan(NotificationsFragment.class);
        }
    }
}