package com.tunisie.dotit.carthageland.fragments;


import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.tunisie.dotit.carthageland.R;
import com.tunisie.dotit.carthageland.Utils.Constants;
import com.tunisie.dotit.carthageland.activities.HomeActivity;
import com.tunisie.dotit.carthageland.application.BaseApplication;
import com.tunisie.dotit.carthageland.custom.CustomTextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class FamilyFragment extends BaseFragment implements View.OnClickListener {

    View view;
    LinearLayout _linearL;
    ImageView add_mbr;
    private SweetAlertDialog progressDoalog;
    BaseApplication myApp;

    String TAG = FamilyFragment.class.getSimpleName();

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_family, container, false);
        _linearL = view.findViewById(R.id.members);
        add_mbr = view.findViewById(R.id.add_mbr);

        HomeActivity.toolbar.setNavigationIcon(R.drawable.back_arrow_ic);
        HomeActivity.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getFragmentManager() != null) {
                    HomeActivity.openDrawer();
                }
            }
        });

        // ((HomeActivity) getActivity()).getSupportActionBar().setIcon(R.drawable.back_arrow_ic);
        add_mbr.setOnClickListener(this);

        myApp = ((BaseApplication) this.getActivity().getApplication());

        //this.getActivity().setTitle("Mon espace familial");
        android.support.v7.widget.Toolbar toolbarTop = (android.support.v7.widget.Toolbar) getActivity().findViewById(R.id.toolbar);
        TextView mTitle = (TextView) toolbarTop.findViewById(R.id.toolbar_title);
        mTitle.setText("Mon espace familial");
        getUsers();

        return view;
    }

    public void fragmentreplace(Class fragmentClass, String tag) throws IllegalAccessException, java.lang.InstantiationException {
        FragmentManager fragmentManager = getFragmentManager();
        Fragment fragment = (Fragment) fragmentClass.newInstance();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.flContent, fragment, tag);
        fragmentTransaction.addToBackStack("tag"); //this will add it to back stack
        fragmentTransaction.commit();
    }

    private void closeFragment() {
        getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
    }

    public void getUsers() {

        progressDoalog = new SweetAlertDialog(this.getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        progressDoalog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressDoalog.setTitleText("Chargement");
        progressDoalog.setCancelable(true);
        progressDoalog.show();
        try {
            RequestQueue queue = Volley.newRequestQueue(this.getActivity());
            JSONObject jsonBody = new JSONObject();

            if (getPreferences().getUser().getId_client() != null) {
                jsonBody.put("id_client", Integer.parseInt(getPreferences().getUserId()));
            }

            final String requestBody = jsonBody.toString();

            StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.clientUsersUrl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e(TAG, "Response is " + response);
                    Log.i("VOLLEY", response);
                    Log.e("TAG", "Token is : " + getPreferences().getToken());

                    parseJSONResponse(response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDoalog.setTitleText("Erreur!")
                            .setContentText("Une erreur est survenue, veuillez vérifier votre connexion Internet ou essayer plus tard.")
                            .setConfirmText("OK")
                            .changeAlertType(SweetAlertDialog.ERROR_TYPE);
                    Log.e("VOLLEYY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("X-Auth-Token", getPreferences().getToken());
                    Log.e("TAG", "Token is : " + getPreferences().getToken());
                    return params;
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        Log.e("requestBody ", requestBody);
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }
                        /*@Override
                        protected Response<String> parseNetworkResponse(NetworkResponse response) {
                            String responseString = "";
                            if (response != null) {
                                responseString = String.valueOf(response);
                                Log.e(TAG, "ResponseString is " + response);
                                // can get more details such as response.headers
                            }
                            return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                        }*/
            };
            queue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String parseJSONResponse(String jsonResponse) {
        try {
            final JSONObject json = new JSONObject(jsonResponse);

            String code = json.getString("code");
            String message = json.getString("message");
            Log.e(TAG, String.valueOf(code));
            progressDoalog.dismiss();

            if (code.equals("200")) {
                JSONArray responseArray = json.getJSONArray("response");
                //JSONArray cadeaux = response.getJSONArray("cadeaux");
                //JSONArray produits = response.getJSONArray("produits");
                if (responseArray.length() > 0) {
                    for (int i = 0; i < responseArray.length(); i++) {
                        JSONObject user = (JSONObject) responseArray.get(i);
                        String id = user.getString("id");
                        String firstname = user.getString("firstname");
                        String lastname = user.getString("lastname");
                        String lienParente = user.getString("lienParente");
                        addViews(id, firstname, lastname, lienParente);
                    }
                } else {
                    _linearL.removeAllViews();
                    _linearL.removeAllViewsInLayout();
                }
            } else {
                progressDoalog.setTitleText("Erreur!")
                        .setContentText(getString(R.string.fb_connect_canceled))
                        .setConfirmText("OK")
                        .changeAlertType(SweetAlertDialog.ERROR_TYPE);
                String msgErr = json.getString("message");
                Log.e("msgErr ", msgErr);
            }
            return code;
        } catch (JSONException ex) {
            ex.printStackTrace();
            return "250";
        }
    }

    private void addViews(final String id, String firstname, String lastname, String lienParente) {
        View v = LayoutInflater.from(getActivity()).inflate(R.layout.member, null);
        _linearL.addView(v);
        _linearL.requestLayout();

        CustomTextView mbr_name = v.findViewById(R.id.mbr_name);
        mbr_name.setText(firstname + " " + lastname);

        CustomTextView mbr_parente = v.findViewById(R.id.mbr_parente);
        mbr_parente.setText(lienParente);

        ImageView trash = v.findViewById(R.id.trash);
        trash.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("Suppression");
                builder.setMessage("Êtes-vous sûr de supprimer ce membre de famille ?");
                builder.setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        delUserClient(id);
                    }
                });
                builder.setNegativeButton("Non", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
                builder.show();
            }
        });
    }

    private void deleteViews(final String id, String firstname, String lastname, String lienParente) {
        View v = LayoutInflater.from(getActivity()).inflate(R.layout.member, null);
        _linearL.addView(v);
        _linearL.requestLayout();

        CustomTextView mbr_name = v.findViewById(R.id.mbr_name);
        mbr_name.setText(firstname + " " + lastname);

        CustomTextView mbr_parente = v.findViewById(R.id.mbr_parente);
        mbr_parente.setText(lienParente);

        ImageView trash = v.findViewById(R.id.trash);
        trash.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                delUserClient(id);
            }
        });
    }

    public void delUserClient(String id) {
        progressDoalog = new SweetAlertDialog(this.getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        progressDoalog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressDoalog.setTitleText("Chargement");
        progressDoalog.setCancelable(true);
        progressDoalog.show();
        try {
            RequestQueue queue = Volley.newRequestQueue(this.getActivity());
            JSONObject jsonBody = new JSONObject();

            jsonBody.put("id_user", id);

            final String requestBody = jsonBody.toString();

            StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.delUsersUrl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e(TAG, "Response is " + response);
                    Log.i("VOLLEY", response);
                    _linearL.invalidate();
                    _linearL.requestLayout();
                    ViewGroup vg = (ViewGroup) view.findViewById(R.id.container);
                    getLayoutInflater().inflate(R.layout.fragment_family, vg, false);
                    progressDoalog.setTitleText("Succès")
                            .setContentText("Le membre a été retiré avec succès.")
                            .setConfirmText("OK")
                            .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                    _linearL.removeAllViewsInLayout();
                    _linearL.removeAllViews();
                    getUsers();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDoalog.setTitleText("Erreur!")
                            .setContentText("Une erreur est survenue, veuillez vérifier votre connexion Internet ou essayer plus tard.")
                            .setConfirmText("OK")
                            .changeAlertType(SweetAlertDialog.ERROR_TYPE);
                    Log.e("VOLLEYY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("X-Auth-Token", getPreferences().getToken());
                    return params;
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        Log.e("requestBody ", requestBody);
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }
                        /*@Override
                        protected Response<String> parseNetworkResponse(NetworkResponse response) {
                            String responseString = "";
                            if (response != null) {
                                responseString = String.valueOf(response);
                                Log.e(TAG, "ResponseString is " + response);
                                // can get more details such as response.headers
                            }
                            return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                        }*/
            };

            queue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        Log.e("DEBUG", "onResume of FamilyFragment");
        //getUsers();
        super.onResume();
    }

    @Override
    public void onPause() {
        Log.e("DEBUG", "OnPause of FamilyFragment");
        super.onPause();
    }

    @Override
    public void onClick(View v) {
        if (v == add_mbr) {
            goToAddMember();
        }
    }

    public void goToAddMember() {
        Fragment fragment = null;
        try {
            fragment = AddMemberFragment.class.newInstance();
            closeFragment();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        //fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();
        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).addToBackStack("tag").commit();
    }
}