package com.tunisie.dotit.carthageland.fragments;


import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.tunisie.dotit.carthageland.R;
import com.tunisie.dotit.carthageland.Utils.Constants;
import com.tunisie.dotit.carthageland.activities.HomeActivity;
import com.tunisie.dotit.carthageland.application.BaseApplication;
import com.tunisie.dotit.carthageland.custom.CustomTextView;
import com.tunisie.dotit.carthageland.models.Message;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotificationsFragment extends BaseFragment {

    View view;
    BaseApplication myApp;
    private SweetAlertDialog progressDoalog;
    LinearLayout _linearL;
    CustomTextView mEmptyTv;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_messages, container, false);
        myApp = ((BaseApplication) this.getActivity().getApplication());
        _linearL = (LinearLayout) view.findViewById(R.id.messages);
        mEmptyTv = view.findViewById(R.id.empty_msg_tv);
        getMessages(getPreferences().getUser().getId_client());
        //this.getActivity().setTitle("Boite de messagerie");
        Toolbar toolbarTop = (Toolbar) getActivity().findViewById(R.id.toolbar);
        TextView mTitle = (TextView) toolbarTop.findViewById(R.id.toolbar_title);
        mTitle.setText("Boite de messagerie");

        HomeActivity.toolbar.setNavigationIcon(R.drawable.back_arrow_ic);
        HomeActivity.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getFragmentManager() != null) {
                    Fragment currentFragment = getFragmentManager().findFragmentById(R.id.flContent);
                    if (currentFragment instanceof NotificationsFragment) {
                        navigateToHomeFragment();
                    }
                } else {
                    HomeActivity.openDrawer();
                }
            }
        });

        return view;
    }

    private void navigateToHomeFragment() {
        Fragment fragment = null;
        try {
            closeFragment();
            fragment = HomeFragment.class.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).addToBackStack(null).commit();
    }

    private void closeFragment() {
        getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
    }

    public void getMessages(final String id_user) {

        progressDoalog = new SweetAlertDialog(this.getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        progressDoalog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressDoalog.setTitleText("Chargement");
        progressDoalog.setCancelable(true);
        progressDoalog.show();

        try {
            RequestQueue queue = Volley.newRequestQueue(this.getActivity());
            JSONObject jsonBody = new JSONObject();
            Log.e("PROFIL ", "***" + getPreferences().getUser() + "***");

            jsonBody.put("client_id", id_user);

            final String requestBody = jsonBody.toString();

            StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.messagesUrl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e("Messagerie", "Response is " + response);
                    Log.i("VOLLEY", response);

                    parseJSONResponse(response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDoalog.setTitleText("Erreur!")
                            .setContentText("Une erreur est survenue, veuillez vérifier votre connexion Internet ou essayer plus tard.")
                            .setConfirmText("OK")
                            .changeAlertType(SweetAlertDialog.ERROR_TYPE);
                    Log.e("VOLLEYY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("X-Auth-Token", getPreferences().getToken());

                    return params;
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        Log.e("requestBody ", requestBody);
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }
            };
            queue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String parseJSONResponse(String jsonResponse) {

        try {
            final JSONObject json = new JSONObject(jsonResponse);
            String code = json.getString("code");
            Log.e("Scan json", String.valueOf(code));

            if (code.equals("200")) {
                progressDoalog.dismissWithAnimation();

                JSONArray list_mess = json.getJSONArray("response");
                Log.e("TAG", "the mesgs list size is " + list_mess.length());

                if (list_mess.length() <= 1) {
                    mEmptyTv.setVisibility(View.VISIBLE);
                } else {

                    for (int i = 0; i < list_mess.length(); i++) {
                        JSONObject mess = (JSONObject) list_mess.get(i);
                        Message message = new Message();
                        message.setObjet(mess.getString("type"));
                        message.setTitle(mess.getString("titre"));
                        message.setContent(mess.getString("content"));

                        String date = mess.getJSONObject("date").getString("date");
                        DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.FRENCH);
                        DateFormat targetFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
                        Date date_ = null;
                        try {
                            date_ = originalFormat.parse(date);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        String formattedDate = targetFormat.format(date_);
                        message.setDate(formattedDate);
                        if (mess.getString("type").contains("receptions")) {
                            addViews(message);
                        }
                    }
                }
            } else {
                progressDoalog.setTitleText("Erreur!")
                        .setContentText("Veuillez vérifier votre login/mot de passe")
                        .setConfirmText("OK")
                        .changeAlertType(SweetAlertDialog.ERROR_TYPE);
                String msgErr = json.getString("message");
                Log.e("msgErr ", msgErr);
            }
            return code;
        } catch (JSONException ex) {
            ex.printStackTrace();
            return "250";
        }
    }

    private void addViews(final Message message) {

        View v = LayoutInflater.from(getActivity()).inflate(R.layout.message, null);

        _linearL.addView(v);
        _linearL.requestLayout();

        CustomTextView notif_title = v.findViewById(R.id.notif_title);
        notif_title.setText(message.getTitle());

        CustomTextView notif_date = v.findViewById(R.id.notif_date);
        notif_date.setText(message.getDate());

        CustomTextView notif_content = v.findViewById(R.id.notif_content);
        if (message.getContent().length() > 30) {
            notif_content.setText(message.getContent().subSequence(0, 29) + "...");
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new AlertDialog.Builder(getActivity())
                            .setTitle(message.getTitle())
                            .setMessage(message.getContent())
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            }).show();
                }
            });
        } else {
            notif_content.setText(message.getContent());
        }
    }

}