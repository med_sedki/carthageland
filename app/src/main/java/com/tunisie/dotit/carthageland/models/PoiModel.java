package com.tunisie.dotit.carthageland.models;

import com.google.gson.annotations.SerializedName;

public class PoiModel {

    @SerializedName("name")
    private String name;
    @SerializedName("filename")
    private String filename;
    @SerializedName("taille")
    private String taille;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getTaille() {
        return taille;
    }

    public void setTaille(String taille) {
        this.taille = taille;
    }
}
