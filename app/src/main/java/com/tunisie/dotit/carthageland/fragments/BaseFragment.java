package com.tunisie.dotit.carthageland.fragments;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.tunisie.dotit.carthageland.R;
import com.tunisie.dotit.carthageland.application.BaseApplication;
import com.tunisie.dotit.carthageland.custom.CustomButton;
import com.tunisie.dotit.carthageland.custom.CustomProgressDialog;
import com.tunisie.dotit.carthageland.custom.CustomTextView;
import com.tunisie.dotit.carthageland.global.AppUtils;
import com.tunisie.dotit.carthageland.global.BackgroundManager;
import com.tunisie.dotit.carthageland.global.SharedPreferences;

import javax.inject.Inject;

import dagger.Lazy;


/**
 * Created on 2/4/18.
 */

public class BaseFragment extends Fragment {

    private static final String TAG = BaseFragment.class.getSimpleName();

    private Runnable mRestartRunnable;

    private CustomProgressDialog mProgressDialog;

    @Inject
    protected Lazy<SharedPreferences> mSharedPreferences;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BaseApplication.getInstance().getApplicationComponent().inject(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onPause() {
        super.onPause();
        hideKeyboard();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mRestartRunnable != null) {
            Log.d(TAG, "start runnable activity");
            mRestartRunnable.run();
        }
    }

    protected void onBindViewCompleted() {
        Log.d(TAG, "on Bind View Completed");
    }

    /**
     * show unavailable Network Error
     */
    public void showErrorNetworkDialog() {

        showSimpleOkDialog(R.string.error_network, R.string.ok);
    }

    public void showSimpleOkDialog(@StringRes int msgId, @StringRes int btnText) {
        showSimpleOkDialog(null, 0, msgId, btnText, null);
    }

    /**
     * show subscribe ok dialog
     *
     * @param msgId    message resources id
     * @param okAction action to do on click
     */
    public void showSimpleOkDialog(String message, @StringRes int titleId, @StringRes int msgId, @StringRes int btnText, final Runnable okAction) {
        if (!getActivity().isFinishing()) {

            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.custom_error_dialog));
            final View dialogView = getLayoutInflater().inflate(R.layout.dialog_layout, null);
            dialogBuilder.setView(dialogView);

            final AlertDialog alertDialog = dialogBuilder.create();
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            alertDialog.setCancelable(okAction == null ? true : false);
            alertDialog.setCanceledOnTouchOutside(false);

            CustomTextView titleTextView = dialogView.findViewById(R.id.tv_title);
            if (titleId != 0) {
                titleTextView.setVisibility(View.VISIBLE);
                titleTextView.setText(getString(titleId));
            } else {
                titleTextView.setVisibility(View.GONE);
            }

            CustomTextView contentTextView = dialogView.findViewById(R.id.tv_content);
            if (message != null && !TextUtils.isEmpty(message)) {
                contentTextView.setText(message);
            } else {
                contentTextView.setText(getString(msgId));
            }

            CustomButton okBtn = dialogView.findViewById(R.id.btn_ok);
            okBtn.setText(btnText);
            okBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                    if (okAction != null) {
                        okAction.run();
                    }
                }
            });

            CustomButton cancelBtn = dialogView.findViewById(R.id.btn_cancel);
            cancelBtn.setVisibility(View.GONE);
            alertDialog.show();
        }
    }

    public void showEmptyError(CustomTextView errorTextView) {
        errorTextView.setVisibility(View.VISIBLE);
    }

    public void showProgressBar() {
        if (!getActivity().isFinishing()) {

            if (mProgressDialog == null) {
                mProgressDialog = new CustomProgressDialog(getActivity(), R.style.ProgressDialogStyle);
                mProgressDialog.setCancelable(false);

            }

            if (!mProgressDialog.isShowing()) {
                mProgressDialog.show();
            }
        }
    }

    /**
     * hide blocking progressBar
     */
    public void hideProgressBar() {
        if (getActivity() != null) {
            if (!getActivity().isFinishing()) {
                if (mProgressDialog != null && mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                }
            }
        }
    }

    /**
     * Start new activity when the app on foreground
     */
    public void startActivityWhenForeground(final Intent intent) {
        startActivityWhenForeground(intent, false);
    }

    /**
     * Start new activity when the app on foreground
     */
    public void startActivityWhenForeground(final Intent intent, final boolean shouldFinish) {
        mRestartRunnable = buildStartRunnable(intent, shouldFinish);
        if (!BackgroundManager.getInstance().isInBackground()) {
            Log.d(TAG, "start activity when foreground isApp on foreground true");
            mRestartRunnable.run();
        } else {
            Log.d(TAG, "start activity when foreground isApp on foreground false");
        }
    }

    /**
     * Start new activity when the app on foreground
     */
    public void startActivityWhenForeground(final Class activity) {
        startActivityWhenForeground(activity, false);
    }

    /**
     * Start new activity when the app on foreground
     *
     * @param shouldFinish current activity
     */

    public void startActivityWhenForeground(final Class activity, final boolean shouldFinish) {
        Intent intent = new Intent(getActivity(), activity);
        startActivityWhenForeground(intent, shouldFinish);
    }

    @NonNull
    private Runnable buildStartRunnable(final Intent intent, final boolean shouldFinish) {
        return new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "run build start runnable");
                startActivity(intent);
                if (shouldFinish) getActivity().finish();
                Log.e("TAG", "the home activity is finished here in this point fragment");
                mRestartRunnable = null;
            }
        };
    }

    /**
     * Start new activity for result when the app on foreground
     */
    public void startActivityForResultWhenForeground(final Intent intent, int requestCode) {
        startActivityForResultWhenForeground(intent, false, requestCode);
    }

    /**
     * Start new activity for result when the app on foreground
     */
    public void startActivityForResultWhenForeground(final Intent intent, final boolean shouldFinish, int requestCode) {

        mRestartRunnable = buildStartForResultRunnable(intent, shouldFinish, requestCode);
        if (!BackgroundManager.getInstance().isInBackground()) {
            Log.d(TAG, "start activity when foreground isApp on foreground true");
            mRestartRunnable.run();
        } else {
            Log.d(TAG, "start activity when foreground isApp on foreground false");
        }
    }

    /**
     * Start new activity for result when the app on foreground
     */
    public void startActivityForResultWhenForeground(final Class activity, int requestCode) {
        startActivityForResultWhenForeground(activity, false, requestCode);
    }

    /**
     * Start new activity for result when the app on foreground
     *
     * @param shouldFinish current activity
     */

    public void startActivityForResultWhenForeground(final Class activity, final boolean shouldFinish, int requestCode) {
        Intent intent = new Intent(getActivity(), activity);
        startActivityForResultWhenForeground(intent, shouldFinish, requestCode);
    }

    @NonNull
    private Runnable buildStartForResultRunnable(final Intent intent, final boolean shouldFinish, final int requestCode) {
        return new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "run build start runnable");
                startActivityForResult(intent, requestCode);
                if (shouldFinish) getActivity().finish();
                Log.e("TAG", "the home activity is finished here in this point fragment");
                mRestartRunnable = null;
            }
        };
    }

    /**
     * ic_status_validated if device is connected to network
     *
     * @return true if connected
     */
    public boolean isNetworkAvailable() {
        return AppUtils.isNetworkAvailable(getActivity());
    }

    /**
     * try to hide Keyboard from the focused screen
     */
    /**
     * try to hide Keyboard from the focused screen
     */
    public void hideKeyboard() {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
            Log.d(TAG, "Could not hide keyboard, window unreachable. " + e.toString());
        }
    }

    public void onBackPressed() {

        if (getActivity() != null) {
            getActivity().onBackPressed();
        }
    }

    /**
     * This method return a SharedPreferences instance
     *
     * @return A SharedPreferences
     */
    protected SharedPreferences getPreferences() {
        return mSharedPreferences.get();
    }

}