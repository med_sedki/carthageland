package com.tunisie.dotit.carthageland.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tunisie.dotit.carthageland.R;
import com.tunisie.dotit.carthageland.adapters.holder.HistoryHolder;
import com.tunisie.dotit.carthageland.models.Historique;

import java.text.ParseException;
import java.util.List;

/**
 * Created on 2/15/18.
 */

public class HistoryLogAdapter extends RecyclerView.Adapter<HistoryHolder> {

    private List<Historique> mHistoryList;
    public HistoryLogAdapter(List<Historique> historylogs) {
        mHistoryList = historylogs ;
    }

    @Override
    public HistoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_history, parent, false);
        return new HistoryHolder(view);
    }


    @Override
    public void onBindViewHolder(HistoryHolder holder, int position) {
        Historique historique = mHistoryList.get(position);
        try {
            holder.bind(historique);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mHistoryList.size();
    }
}