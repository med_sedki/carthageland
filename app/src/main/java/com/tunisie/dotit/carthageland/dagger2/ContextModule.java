package com.tunisie.dotit.carthageland.dagger2;

import android.content.Context;

import dagger.Module;
import dagger.Provides;

/**
 * Created on 2/2/18.
 */

@Module
public class ContextModule {

    private final Context mContext;

    public ContextModule(Context context) {
        mContext = context.getApplicationContext();
    }

    @Provides
    @ApplicationScope
    @ApplicationContext
    public Context context() {
        return mContext;
    }
}
