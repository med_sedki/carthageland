package com.tunisie.dotit.carthageland.fragments;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.tunisie.dotit.carthageland.R;
import com.tunisie.dotit.carthageland.Utils.Constants;
import com.tunisie.dotit.carthageland.application.BaseApplication;
import com.tunisie.dotit.carthageland.custom.CustomEditText;
import com.tunisie.dotit.carthageland.custom.CustomTextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class InviteFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

    ImageView add_mail;
    View view;
    LinearLayout emails;
    CustomEditText edittext_email;
    CustomTextView btn_add;
    BaseApplication myApp;
    String mails = "";
    String mailsPar = "";
    private SweetAlertDialog progressDoalog;
    boolean areMailsValid = true;
    public static final Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile(
            "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                    "\\@" +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                    "(" +
                    "\\." +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                    ")+"
    );

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_invite, container, false);
        myApp = ((BaseApplication) this.getActivity().getApplication());

        add_mail = (ImageView) view.findViewById(R.id.add_mail);
        add_mail.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                addViews();
            }
        });
        emails = (LinearLayout) view.findViewById(R.id.parrains);
        edittext_email = (CustomEditText) view.findViewById(R.id.edittext_email);
        btn_add = (CustomTextView) view.findViewById(R.id.btn_add);

        btn_add.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                View focusView = null;
                boolean cancel = false;
                final int childCount = emails.getChildCount();
                for (int i = 0; i < childCount; i++) {
                    View element = emails.getChildAt(i);
                    if (element instanceof CustomEditText) {
                        CustomEditText editText = (CustomEditText) element;
                        if (!TextUtils.isEmpty(editText.getText().toString())) {
                            if (!isMailValid(editText.getText().toString())) {
                                editText.setError(getString(R.string.error_invalid_mail));
                                focusView = editText;
                                cancel = true;
                            }
                        }
                    }
                }

                if (TextUtils.isEmpty(edittext_email.getText().toString())) {
                    edittext_email.setError(getString(R.string.error_field_required));
                    focusView = edittext_email;
                    cancel = true;
                }

                if (cancel) {
                    // There was an error; don't attempt login and focus the first
                    // form field with an error.
                    focusView.requestFocus();
                } else {
                    progressDoalog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
                    progressDoalog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
                    progressDoalog.setTitleText("Chargement");
                    progressDoalog.setCancelable(true);
                    progressDoalog.show();
                    SHowProgress parrain = new SHowProgress();
                    parrain.execute();
                }
            }
        });
        Toolbar toolbarTop = (Toolbar) getActivity().findViewById(R.id.toolbar);
        TextView mTitle = (TextView) toolbarTop.findViewById(R.id.toolbar_title);
        mTitle.setText("Inviter des amis");
        return view;
    }

    private void addViews() {
        View v = LayoutInflater.from(getActivity()).inflate(R.layout.email_add, null);
        emails.addView(v);
        emails.requestLayout();
    }

    private void navigateToHomeFragment() {
        Fragment fragment = null;
        try {
            closeFragment();
            fragment = HomeFragment.class.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).addToBackStack("tag").commit();
    }

    private void closeFragment() {
        getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
    }

    private void parrain(final ArrayList<String> mailList) {
        try {
            RequestQueue queue = Volley.newRequestQueue(this.getActivity());
            JSONObject jsonBody = new JSONObject();
            Log.e("PROFIL ", "***" + getPreferences().getUser() + "***");

            jsonBody.put("id_client", Integer.parseInt(getPreferences().getUserId()));
            JSONArray array = new JSONArray();
            for (int i = 0; i < mailList.size(); i++) {
                if (mailList.get(i) != null) {
                    array.put(mailList.get(i));
                }
            }
            System.out.println(array.toString());
            jsonBody.put("mail", array);

            final String requestBody = jsonBody.toString();

            StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.parrainerUrl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e("HISTORY", "url " + response);
                    //setContentView(R.layout.activity_main);
                    parseJSONResponse(response, mailList);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDoalog.setTitleText("Erreur!")
                            .setContentText("Une erreur est survenue, veuillez vérifier votre connexion Internet ou essayer plus tard.")
                            .setConfirmText("OK")
                            .changeAlertType(SweetAlertDialog.ERROR_TYPE);
                    Log.e("VOLLEYY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("X-Auth-Token", getPreferences().getToken());

                    return params;
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        Log.e("requestBody ", requestBody);
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }
            };
            queue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String parseJSONResponse(String jsonResponse, ArrayList<String> mailList) {
        try {
            final JSONObject json = new JSONObject(jsonResponse);

            String code = json.getString("code");
            String message = json.getString("message");

            if (code.equals("200")) {
                //progressDoalog.dismiss();
                mailsPar = mailsPar + mailList + ", ";
                progressDoalog.setTitleText("")
                        .setContentText("Succès de parrainage.")
                        .setConfirmText("OK")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                progressDoalog.dismissWithAnimation();
                                for (int i = 0; i < emails.getChildCount(); i++) {
                                    View element = emails.getChildAt(i);
                                    if (element instanceof CustomEditText) {
                                        CustomEditText editText = (CustomEditText) element;
                                        navigateToHomeFragment();
                                        editText.setText("");
                                    }
                                }
                            }
                        })
                        .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
            } else if (code.equals("400")) {
                //progressDoalog.dismiss();
                mails = mails + mailList + ", ";
                String[] separated = message.split(" ");
                String firstPart = separated[0];
                String secondPart = separated[1];

                String[] secondSplit = secondPart.split(" ");
                String alreadyUsedMail = secondSplit[0];

                if (mails.split(",").length > 1) {
                    progressDoalog.setTitleText("")
                            .setContentText("Les destinataires " + alreadyUsedMail + " sont déjà inscrits ou parrainés.")
                            .setConfirmText("OK")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    progressDoalog.dismissWithAnimation();
                                    for (int i = 0; i < emails.getChildCount(); i++) {
                                        View element = emails.getChildAt(i);
                                        if (element instanceof CustomEditText) {
                                            CustomEditText editText = (CustomEditText) element;
                                            editText.setText("");
                                        }
                                    }
                                }
                            })
                            .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                }
            } else if (code.equals("500")) {
                //progressDoalog.dismiss();
                Log.e("TAG", jsonResponse);
                progressDoalog.setTitleText("Erreur!")
                        .setContentText("Un problème est survenu, veuillez essayer plus tard!")
                        .setConfirmText("OK")
                        .changeAlertType(SweetAlertDialog.ERROR_TYPE);
                String msgErr = json.getString("message");
                Log.e("msgErr ", msgErr);
            } else {
                progressDoalog.setTitleText("Erreur!")
                        .setContentText("Un problème est survenu, veuillez essayer plus tard!")
                        .setConfirmText("OK")
                        .changeAlertType(SweetAlertDialog.ERROR_TYPE);
                String msgErr = json.getString("message");
                Log.e("msgErr ", msgErr);
            }
            return code;
        } catch (JSONException ex) {
            ex.printStackTrace();
            return "250";
        }
    }

    public boolean isMailValid(String email) {
        return EMAIL_ADDRESS_PATTERN.matcher(email).matches();
    }

    private class SHowProgress extends AsyncTask<Void, Integer, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... arg0) {

            final int childCount = emails.getChildCount();
            ArrayList<String> mailList = new ArrayList<String>();
            for (int i = 0; i < childCount; i++) {

                View element = emails.getChildAt(i);
                if (element instanceof CustomEditText) {
                    CustomEditText editText = (CustomEditText) element;
                    if (!TextUtils.isEmpty(editText.getText().toString())) {
                        //parrain(editText.getText().toString());
                        mailList.add(editText.getText().toString());
                    }
                }
            }
            parrain(mailList);
            try {
                Log.v("msg", "WAIT CheckFrequencyRun");
                Thread.sleep(2000); // giving time to connect to wifi
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Log.e("Parraiange ", "mails " + mails);
            Log.e("Parraiange ", "mailsPar " + mailsPar);

            mails = "";
            mailsPar = "";
        }
    }
}