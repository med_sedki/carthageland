package com.tunisie.dotit.carthageland.fragments;


import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.tunisie.dotit.carthageland.activities.HomeActivity;
import com.tunisie.dotit.carthageland.adapters.GridViewAdapter;
import com.tunisie.dotit.carthageland.models.ImageItem;
import com.tunisie.dotit.carthageland.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class PhotosFragment extends BaseFragment {

    private GridView gridView;
    private GridViewAdapter gridAdapter;
    View view;

    public PhotosFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_photos, container, false);
        gridView = (GridView) view.findViewById(R.id.gridView);
        gridAdapter = new GridViewAdapter(this.getActivity(), R.layout.grid_item_layout, getData());
        gridView.setAdapter(gridAdapter);
        // Inflate the layout for this fragment
        return view;
    }

    // Prepare some dummy data for gridview
    private ArrayList<ImageItem> getData() {
        final ArrayList<ImageItem> imageItems = new ArrayList<>();
        TypedArray imgs = getResources().obtainTypedArray(R.array.image_ids);
        for (int i = 0; i < imgs.length(); i++) {
            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), imgs.getResourceId(i, -1));
            imageItems.add(new ImageItem(bitmap, "Image#" + i));
        }
        return imageItems;
    }

}
