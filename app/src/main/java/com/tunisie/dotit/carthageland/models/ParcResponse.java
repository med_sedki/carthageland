package com.tunisie.dotit.carthageland.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ParcResponse {

    @SerializedName("code")
    private String code;
    @SerializedName("message")
    private String message;
    @SerializedName("response")
    private List<Parc> response ;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Parc> getResponse() {
        return response;
    }

    public void setResponse(List<Parc> response) {
        this.response = response;
    }
}
