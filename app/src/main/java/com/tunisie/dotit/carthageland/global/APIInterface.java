package com.tunisie.dotit.carthageland.global;

import com.tunisie.dotit.carthageland.models.CategoryResponse;
import com.tunisie.dotit.carthageland.models.Code;
import com.tunisie.dotit.carthageland.models.GiftsResponse;
import com.tunisie.dotit.carthageland.models.HomeTextResponse;
import com.tunisie.dotit.carthageland.models.LogOutResponse;
import com.tunisie.dotit.carthageland.models.ParcPhotoResponse;
import com.tunisie.dotit.carthageland.models.ParcResponse;
import com.tunisie.dotit.carthageland.models.PoiResponse;
import com.tunisie.dotit.carthageland.models.SliderPhotoResponse;
import com.tunisie.dotit.carthageland.models.UseVoucherResponse;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface APIInterface {

    @GET("/api/getPOIs")
    Call<PoiResponse> getRestaurants(@Header("X-Auth-Token") String token, @Query("category") String category, @Query("site") String site);

    @GET("/api/getProduits")
    Call<GiftsResponse> getProducts(@Header("X-Auth-Token") String token,@Query("site") String site);

    @GET("/api/getPOIs")
    Call<PoiResponse> getPoiList(@Header("X-Auth-Token") String token, @Query("site") String site);

    @GET("/api/getParcs")
    Call<ParcResponse> getParcsList(@Header("X-Auth-Token") String token, @Query("site") String site, @Query("category") String category);

    @GET("/api/getPOICategories")
    Call<CategoryResponse> getCategories(@Header("X-Auth-Token") String token);

    @Headers({"Content-type: application/json",
            "Accept: */*"})
    @POST("/api/voucherBoutiqueConvert")
    Call<UseVoucherResponse> useVoucher(@Header("X-Auth-Token") String token, @Body JsonObject body);

    @GET("/api/getParcsPhotos")
    Call<ParcPhotoResponse> getParcPhotos(@Header("X-Auth-Token") String token, @Query("parc") String parc,@Query("poi") String poi);

    @GET("/api/getNews")
    Call<HomeTextResponse> getNewsText(@Header("X-Auth-Token") String token, @Query("site") String parc);

    @POST("/api/WsLogout")
    Call<LogOutResponse> logOut(@Header("X-Auth-Token") String token);

    @GET("/api/sliders")
    Call<SliderPhotoResponse> getSliders();
}
