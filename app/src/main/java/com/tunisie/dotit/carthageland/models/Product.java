package com.tunisie.dotit.carthageland.models;

import com.google.gson.annotations.SerializedName;

public class Product {
    @SerializedName("code")
    private String code;
    @SerializedName("name")
    private String name;
    @SerializedName("price")
    private String price;
    @SerializedName("category")
    private Object category;
    @SerializedName("description")
    private String description;
    @SerializedName("id")
    private Integer id;
    @SerializedName("filename")
    private Object filename;
    @SerializedName("fidPointsPrice")
    private int fidelityPriceNbr;
    @SerializedName("webPath")
    private String webPath;
    @SerializedName("fidPointsEstimation")
    private String fidPointsEstimation;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Object getCategory() {
        return category;
    }

    public void setCategory(Object category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Object getFilename() {
        return filename;
    }

    public void setFilename(Object filename) {
        this.filename = filename;
    }

    public int getFidelityPriceNbr() {
        return fidelityPriceNbr;
    }

    public void setFidelityPriceNbr(int fidelityPriceNbr) {
        this.fidelityPriceNbr = fidelityPriceNbr;
    }

    public String getFidPointsEstimation() {
        return fidPointsEstimation;
    }

    public void setFidPointsEstimation(String fidPointsEstimat) {
        this.fidPointsEstimation = fidPointsEstimat;
    }

    public String getWebPath() {
        return webPath;
    }

    public void setWebPath(String webPath) {
        this.webPath = webPath;
    }
}
