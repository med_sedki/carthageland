package com.tunisie.dotit.carthageland.activities;

import android.os.Bundle;

import com.tunisie.dotit.carthageland.R;
import com.tunisie.dotit.carthageland.global.AppUtils;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        pendingTransition();
    }

    private void pendingTransition() {
        overridePendingTransition(R.anim.no, R.anim.slide_in_top);
    }

    public boolean isNetworkAvailable() {
        return AppUtils.isNetworkAvailable(this);
    }

}
