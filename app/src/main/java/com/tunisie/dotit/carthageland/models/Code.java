package com.tunisie.dotit.carthageland.models;

import com.google.gson.annotations.SerializedName;

public class Code {


    @SerializedName("code")
    private String code;

    @SerializedName("nb")
    private int number;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

}
