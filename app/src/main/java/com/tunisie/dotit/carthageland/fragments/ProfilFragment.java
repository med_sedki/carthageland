package com.tunisie.dotit.carthageland.fragments;


import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ibotta.android.support.pickerdialogs.SupportedDatePickerDialog;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.tunisie.dotit.carthageland.R;
import com.tunisie.dotit.carthageland.Utils.Constants;
import com.tunisie.dotit.carthageland.activities.HomeActivity;
import com.tunisie.dotit.carthageland.application.BaseApplication;
import com.tunisie.dotit.carthageland.custom.CustomEditText;
import com.tunisie.dotit.carthageland.custom.CustomTextView;
import com.tunisie.dotit.carthageland.models.Carte;
import com.tunisie.dotit.carthageland.models.Client;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import static android.app.Activity.RESULT_OK;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProfilFragment extends BaseFragment implements View.OnClickListener, AdapterView.OnItemSelectedListener, SupportedDatePickerDialog.OnDateSetListener {

    CustomEditText txt_birthday;
    CustomEditText txt_name;
    CustomEditText txt_name2;
    CustomEditText txt_phone;
    CustomTextView txt_email;
    CustomEditText txt_address;
    CustomEditText txt_nbr_enfants;
    CustomTextView txt_member;
    CustomTextView mFullNameTv;
    CustomEditText txt_gov;
    CustomEditText txt_zip;
    //ImageView addButton;
    CustomTextView btn_edit;
    private SweetAlertDialog progressDoalog;
    LinearLayout mainLinearLayout;
    ScrollView scrollView;
    private ImageView uploadButton;
    private ImageView imageProfile;
    private String imagepath = null;
    private int mYear, mMonth, mDay;
    View view;
    LinearLayout _linearL;
    ImageView add_mbr;
    BaseApplication myApp;

    public static final Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile(
            "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                    "\\@" +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                    "(" +
                    "\\." +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                    ")+"
    );

    String name;
    String name2;
    String phone;
    String email;
    String address;
    String nbrEnfants;
    String birthday;

    String govTxt;
    String zipTxt;

    Spinner spinnerG;
    Spinner spinner;
    Spinner spinnerZ;
    Spinner spinnerC;

    ArrayAdapter<String> dataAdapterC;
    ArrayAdapter<String> dataAdapterG;
    ArrayAdapter<String> dataAdapterZ;

    List<String> countries;
    List<String> governates;
    List<String> zipCodes;

    String governate;
    String country;
    String zipCode;

    Integer age = 0;

    String TAG = ProfilFragment.class.getSimpleName();
    private SweetAlertDialog mProgressDialog;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_profil, container, false);

        Toolbar toolbarTop = (Toolbar) getActivity().findViewById(R.id.toolbar);
        TextView mTitle = (TextView) toolbarTop.findViewById(R.id.toolbar_title);
        mTitle.setText("Mon profil");

        myApp = ((BaseApplication) this.getActivity().getApplication());

        HomeActivity.toolbar.setNavigationIcon(R.drawable.drawer_menu_ic);
        HomeActivity.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getFragmentManager() != null) {
                    HomeActivity.openDrawer();
                }
            }
        });

        //addButton = (ImageView) view.findViewById(R.id.btn_add);
        //addButton.setOnClickListener(this);

        mainLinearLayout = (LinearLayout) view.findViewById(R.id.mainLinearLayout);
        scrollView = (ScrollView) view.findViewById(R.id.scrollView);

        txt_birthday = (CustomEditText) view.findViewById(R.id.txt_birthday);
        btn_edit = (CustomTextView) view.findViewById(R.id.btn_edit);
        txt_name = (CustomEditText) view.findViewById(R.id.txt_name);
        txt_name2 = (CustomEditText) view.findViewById(R.id.txt_name2);
        txt_phone = (CustomEditText) view.findViewById(R.id.txt_phone);
        txt_email = (CustomTextView) view.findViewById(R.id.txt_email);
        txt_address = (CustomEditText) view.findViewById(R.id.txt_address);
        txt_nbr_enfants = (CustomEditText) view.findViewById(R.id.txt_nbr_enfants);
        txt_gov = (CustomEditText) view.findViewById(R.id.txt_gov);
        txt_zip = (CustomEditText) view.findViewById(R.id.txt_zip);

        uploadButton = (ImageView) view.findViewById(R.id.camera);
        imageProfile = (ImageView) view.findViewById(R.id.profil_image);

        mFullNameTv = view.findViewById(R.id.full_name_tv);
        mFullNameTv.setText(getPreferences().getUser().getPrenom() + " " + getPreferences().getUser().getNom());

        txt_birthday.setOnClickListener(this);
        btn_edit.setOnClickListener(this);
        uploadButton.setOnClickListener(this);
        //upLoadServerUri = "192.168.2.7/news/upload.php";
        ImageView img = new ImageView(this.getActivity());

        governates = new ArrayList<String>();
        countries = new ArrayList<String>();
        zipCodes = new ArrayList<String>();

        initGenders();
        initCountries();
        initGovernorates();
        initZipCode();
        getUserInfo();

        getCountries();
        getUsers();

        _linearL = view.findViewById(R.id.members);
        add_mbr = view.findViewById(R.id.add_mbr);
        // ((HomeActivity) getActivity()).getSupportActionBar().setIcon(R.drawable.back_arrow_ic);
        add_mbr.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Fragment fragment = null;
                try {
                    fragment = AddMemberFragment.class.newInstance();
                    closeFragment();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                // Insert the fragment by replacing any existing fragment
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.flContent, fragment).addToBackStack("tag").commit();
               /* try {
                    fragmentreplace(AddMemberFragment.class,"");
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (java.lang.InstantiationException e) {
                    e.printStackTrace();
                }*/
            }
        });

        //getGovernates();

        txt_gov.setVisibility(View.GONE);
        txt_zip.setVisibility(View.GONE);

        // Inflate the layout for this fragment
        return view;
    }

    /*public void fragmentreplace(Class fragmentClass, String tag) throws IllegalAccessException, java.lang.InstantiationException {
        FragmentManager fragmentManager = getFragmentManager();
        Fragment fragment = (Fragment) fragmentClass.newInstance();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.flContent, fragment, tag);
        fragmentTransaction.addToBackStack(null); //this will add it to back stack
        fragmentTransaction.commit();
    }*/

    private void closeFragment() {
        getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
    }

    private void navigateToFamilyFragment() {
        Fragment fragment = null;
        try {
            closeFragment();
            fragment = ProfilFragment.class.newInstance();

        } catch (Exception e) {
            e.printStackTrace();
        }

        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).addToBackStack("tag").commit();
    }


    public void getUsers() {

        try {
            RequestQueue queue = Volley.newRequestQueue(this.getActivity());
            JSONObject jsonBody = new JSONObject();

            if (getPreferences().getUser().getId_client() != null) {
                jsonBody.put("id_client", Integer.parseInt(getPreferences().getUserId()));
            }

            final String requestBody = jsonBody.toString();

            StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.clientUsersUrl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e(TAG, "Response is " + response);
                    Log.i("VOLLEY", response);
                    Log.e("TAG", "Token is : " + getPreferences().getToken());

                    parseJSONResponse(response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDoalog.setTitleText("Erreur!")
                            .setContentText("Une erreur est survenue, veuillez vérifier votre connexion Internet ou essayer plus tard.")
                            .setConfirmText("OK")
                            .changeAlertType(SweetAlertDialog.ERROR_TYPE);
                    Log.e("VOLLEYY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("X-Auth-Token", getPreferences().getToken());
                    Log.e("TAG", "Token is : " + getPreferences().getToken());
                    return params;
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        Log.e("requestBody ", requestBody);
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }

                        /*@Override
                        protected Response<String> parseNetworkResponse(NetworkResponse response) {
                            String responseString = "";
                            if (response != null) {
                                responseString = String.valueOf(response);
                                Log.e(TAG, "ResponseString is " + response);
                                // can get more details such as response.headers
                            }
                            return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                        }*/
            };

            queue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String parseJSONResponse(String jsonResponse) {
        try {
            final JSONObject json = new JSONObject(jsonResponse);

            String code = json.getString("code");
            String message = json.getString("message");
            Log.e(TAG, String.valueOf(code));
            progressDoalog.dismiss();

            if (code.equals("200")) {

                JSONArray responseArray = json.getJSONArray("response");
                //JSONArray cadeaux = response.getJSONArray("cadeaux");
                //JSONArray produits = response.getJSONArray("produits");
                if (responseArray.length() > 0) {
                    for (int i = 0; i < responseArray.length(); i++) {
                        JSONObject user = (JSONObject) responseArray.get(i);

                        String id = user.getString("id");
                        String firstname = user.getString("firstname");
                        String lastname = user.getString("lastname");
                        String lienParente = user.getString("lienParente");
                        addViews(id, firstname, lastname, lienParente);
                    }
                } else {
                    _linearL.removeAllViews();
                    _linearL.removeAllViewsInLayout();
                }
                Log.e("responseArray ", String.valueOf(responseArray));

                progressDoalog.setTitleText("Succès!")
                        .setContentText("Votre modifications sont validés et enregistrés avec succés.")
                        .setConfirmText("OK")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                progressDoalog.dismiss();
                            }
                        })
                        .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);

            }/* else if (code.equals("201")) {
                progressDoalog.setTitleText("Succès!")
                        .setContentText("Votre modifications sont validés et enregistrés avec succés.")
                        .setConfirmText("OK")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                progressDoalog.dismiss();
                            }
                        })
                        .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
            }*/ else {
                progressDoalog.setTitleText("Erreur!")
                        .setContentText(getString(R.string.fb_connect_canceled))
                        .setConfirmText("OK")
                        .changeAlertType(SweetAlertDialog.ERROR_TYPE);
                String msgErr = json.getString("message");
                Log.e("msgErr ", msgErr);
            }
            return code;
        } catch (JSONException ex) {
            ex.printStackTrace();
            return "250";
        }
    }

    private void addViews(final String id, final String firstname, String lastname, String lienParente) {

        View v = LayoutInflater.from(getActivity()).inflate(R.layout.member, null);
        _linearL.addView(v);
        _linearL.requestLayout();

        CustomTextView mbr_name = v.findViewById(R.id.mbr_name);
        mbr_name.setText(firstname + " " + lastname);

        CustomTextView mbr_parente = v.findViewById(R.id.mbr_parente);
        mbr_parente.setText(lienParente);

        ImageView trash = v.findViewById(R.id.trash);
        trash.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

                builder.setTitle("Suppression");
                builder.setMessage("Êtes-vous sûr de supprimer ce membre de famille ?");
                builder.setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        delUserClient(id);
                    }
                });
                builder.setNegativeButton("Non", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
                builder.show();
            }
        });
    }

    public void delUserClient(String id) {
        progressDoalog = new SweetAlertDialog(this.getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        progressDoalog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressDoalog.setTitleText("Chargement");
        progressDoalog.setCancelable(true);
        progressDoalog.show();
        try {
            RequestQueue queue = Volley.newRequestQueue(this.getActivity());
            JSONObject jsonBody = new JSONObject();

            jsonBody.put("id_user", id);

            final String requestBody = jsonBody.toString();

            StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.delUsersUrl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e(TAG, "Response is " + response);
                    Log.i("VOLLEY", response);
                    _linearL.invalidate();
                    _linearL.requestLayout();
                    ViewGroup vg = (ViewGroup) view.findViewById(R.id.container);
                    getLayoutInflater().inflate(R.layout.fragment_family, vg, false);
                    progressDoalog.setTitleText("Succès")
                            .setContentText("Le membre a été retiré avec succès.")
                            .setConfirmText("OK")
                            .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                    _linearL.removeAllViewsInLayout();
                    _linearL.removeAllViews();
                    progressDoalog.dismiss();
                    getUsers();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDoalog.setTitleText("Erreur!")
                            .setContentText("Une erreur est survenue, veuillez vérifier votre connexion Internet ou essayer plus tard.")
                            .setConfirmText("OK")
                            .changeAlertType(SweetAlertDialog.ERROR_TYPE);
                    Log.e("VOLLEYY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("X-Auth-Token", getPreferences().getToken());

                    return params;
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        Log.e("requestBody ", requestBody);
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }

                        /*@Override
                        protected Response<String> parseNetworkResponse(NetworkResponse response) {
                            String responseString = "";
                            if (response != null) {
                                responseString = String.valueOf(response);
                                Log.e(TAG, "ResponseString is " + response);
                                // can get more details such as response.headers
                            }
                            return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                        }*/
            };

            queue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        Log.e("DEBUG", "onResume of FamilyFragment");
        //getUsers();
        super.onResume();
    }

    @Override
    public void onPause() {
        Log.e("DEBUG", "OnPause of FamilyFragment");
        super.onPause();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        // On selecting a spinner item
        String item = parent.getItemAtPosition(position).toString();

        // Showing selected spinner item
        //Toast.makeText(parent.getContext(), "Selected: " + item, Toast.LENGTH_LONG).show();
    }

    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub
    }

    public void initGenders() {
        // Spinner element
        spinnerG = (Spinner) view.findViewById(R.id.gender);

        // Spinner click listener
        spinnerG.setOnItemSelectedListener(this);

        // Spinner Drop down elements
        List<String> genders = new ArrayList<String>();
        genders.add("Homme");
        genders.add("Femme");

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_spinner_item, genders) {
            public View getView(int position, View convertView, ViewGroup parent) {

                View v = super.getView(position, convertView, parent);

                //applyFontToSpinner((TextView) v);

                return v;
            }

            public View getDropDownView(int position, View convertView, ViewGroup parent) {

                View v = super.getDropDownView(position, convertView, parent);

                applyFontToSpinner((TextView) v);

                return v;
            }
        };

        spinnerG.setPrompt("Vous êtes");

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinnerG.setAdapter(dataAdapter);
    }

    public void initGovernorates() {

        // Spinner element
        spinner = (Spinner) view.findViewById(R.id.gov);

        // Spinner click listener
        if (governates.size() > 0) {
            getZipCodesForGov(governates.get(0));
            getZipCodesForGovAlg(governates.get(0));
        }
        Log.e(TAG, "governatess " + governates);
        // Spinner click listener
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                zipCodes.clear();
                dataAdapterZ.notifyDataSetChanged();
                if (governates.size() > 0) {
                    getZipCodesForGov(spinner.getSelectedItem().toString());
                    getZipCodesForGovAlg(spinner.getSelectedItem().toString());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });

        // Spinner Drop down elements
        /*List<String> categories = new ArrayList<String>();
        categories.add("Sousse");
        categories.add("Tunis");*/

        // Creating adapter for spinner
        dataAdapterG = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_spinner_item, governates) {
            public View getView(int position, View convertView, ViewGroup parent) {

                View v = super.getView(position, convertView, parent);

                applyFontToSpinner((TextView) v);

                return v;
            }

            public View getDropDownView(int position, View convertView, ViewGroup parent) {

                View v = super.getDropDownView(position, convertView, parent);

                applyFontToSpinner((TextView) v);

                return v;
            }
        };

        // Drop down layout style - list view with radio button
        dataAdapterG.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapterG);
    }

    public void initCountries() {
        // Spinner element
        spinnerC = (Spinner) view.findViewById(R.id.country);

        // Spinner click listener
        spinnerC.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                governates.clear();
                zipCodes.clear();

                if (spinnerC.getSelectedItem().toString().equals("Tunisie")) {
                    spinner.setVisibility(View.VISIBLE);
                    spinnerZ.setVisibility(View.VISIBLE);
                    txt_gov.setVisibility(View.GONE);
                    txt_zip.setVisibility(View.GONE);
                    getGovernates();
                } else if (spinnerC.getSelectedItem().toString().equals("Algérie")) {
                    spinner.setVisibility(View.VISIBLE);
                    spinnerZ.setVisibility(View.VISIBLE);
                    txt_gov.setVisibility(View.GONE);
                    txt_zip.setVisibility(View.GONE);
                    getGovernatesAlg();
                } else {
                    spinner.setVisibility(View.GONE);
                    spinnerZ.setVisibility(View.GONE);
                    txt_gov.setVisibility(View.VISIBLE);
                    txt_zip.setVisibility(View.VISIBLE);

                    txt_gov.setText(governate);
                    txt_zip.setText(zipCode);
                }
                dataAdapterG.notifyDataSetChanged();
                dataAdapterZ.notifyDataSetChanged();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });

        // Creating adapter for spinner
        dataAdapterC = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_spinner_item, countries) {
            public View getView(int position, View convertView, ViewGroup parent) {

                View v = super.getView(position, convertView, parent);

                applyFontToSpinner((TextView) v);

                return v;
            }

            public View getDropDownView(int position, View convertView, ViewGroup parent) {

                View v = super.getDropDownView(position, convertView, parent);

                applyFontToSpinner((TextView) v);

                return v;
            }
        };

        // Drop down layout style - list view with radio button
        dataAdapterC.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinnerC.setAdapter(dataAdapterC);
    }

    public void initZipCode() {

        // Spinner element
        spinnerZ = (Spinner) view.findViewById(R.id.zipcode);


        // Spinner click listener
        spinnerZ.setOnItemSelectedListener(this);

        // Spinner Drop down elements
       /* List<String> zips = new ArrayList<String>();
        zips.add("4060");
        zips.add("4062");*/

        // Creating adapter for spinner
        dataAdapterZ = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_spinner_item, zipCodes) {
            public View getView(int position, View convertView, ViewGroup parent) {


                View v = super.getView(position, convertView, parent);


                applyFontToSpinner((TextView) v);

                return v;


            }


            public View getDropDownView(int position, View convertView, ViewGroup parent) {


                View v = super.getDropDownView(position, convertView, parent);

                applyFontToSpinner((TextView) v);

                return v;


            }
        };

        // Drop down layout style - list view with radio button
        dataAdapterZ.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinnerZ.setAdapter(dataAdapterZ);

        //getUserInfo();
    }

    public void getGovernates() {
        progressDoalog = new SweetAlertDialog(this.getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        progressDoalog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressDoalog.setTitleText("Chargement");
        progressDoalog.setCancelable(true);
        progressDoalog.show();
        Log.e("Register ", "get all governates");
        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(this.getActivity());

// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constants.govUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDoalog.dismiss();
                        Log.e("Register ", "governates " + response);
                        try {
                            JSONArray json = new JSONArray(response);
                            for (int i = 0; i < json.length(); i++) {
                                JSONObject row = json.getJSONObject(i);
                                governates.add(row.getString("governorate"));
                            }
                            dataAdapterG.notifyDataSetChanged();
                            for (int i = 0; i < spinner.getCount(); i++) {
                                Log.e("TOKEN ", "***governorate spinner*** " + spinner.getItemAtPosition(i));
                                if (spinner.getItemAtPosition(i).equals(governate)) {
                                    spinner.setSelection(i);
                                    break;
                                }
                            }
                        } catch (JSONException ex) {
                            ex.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Register ", "Error is" + error.toString());
                progressDoalog.dismiss();
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    public void getGovernatesAlg() {
        progressDoalog = new SweetAlertDialog(this.getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        progressDoalog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressDoalog.setTitleText("Chargement");
        progressDoalog.setCancelable(true);
        progressDoalog.show();
        Log.e("Register ", "get all governates alg");
        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(this.getActivity());

// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constants.govUrlAlg,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDoalog.dismiss();
                        Log.e("Register ", "governates alg" + response);
                        try {
                            JSONArray json = new JSONArray(response);
                            for (int i = 0; i < json.length(); i++) {
                                JSONObject row = json.getJSONObject(i);
                                Log.e("governates ", "Response is" + row.getString("nom"));
                                governates.add(row.getString("nom"));
                            }
                            dataAdapterG.notifyDataSetChanged();
                            for (int i = 0; i < spinner.getCount(); i++) {
                                Log.e("TOKEN ", "***governorate spinner Alg*** " + spinner.getItemAtPosition(i));
                                if (spinner.getItemAtPosition(i).equals(governate)) {
                                    spinner.setSelection(i);
                                    break;
                                }
                            }
                        } catch (JSONException ex) {
                            ex.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Register ", "Error is" + error.toString());
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    public void getZipCodesForGov(String gov) {
        Log.e("Register ", "get all zipCodes " + gov);
        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(this.getActivity());

// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constants.zipCodesUrl + "?governorate=" + gov,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Register ", "zipCodes " + response);
                        try {
                            JSONArray json = new JSONArray(response);
                            for (int i = 0; i < json.length(); i++) {
                                JSONObject row = json.getJSONObject(i);
                                zipCodes.add(row.getString("zipCode"));
                            }
                            dataAdapterZ.notifyDataSetChanged();
                            for (int i = 0; i < spinnerZ.getCount(); i++) {
                                Log.e("TOKEN ", "***zipCode spinner*** " + spinnerZ.getItemAtPosition(i));
                                if (spinnerZ.getItemAtPosition(i).equals(zipCode)) {
                                    spinnerZ.setSelection(i);
                                    break;
                                }
                            }
                        } catch (JSONException ex) {
                            ex.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Register ", "Error is" + error.toString());
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    public void getZipCodesForGovAlg(String gov) {
        Log.e("Register ", "get all zipCodes " + gov);
        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(this.getActivity());

// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constants.zipCodesUrlAlg + "?governorate=" + gov,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Register ", "zipCodesAlg " + response);
                        try {
                            JSONArray json = new JSONArray(response);
                            for (int i = 0; i < json.length(); i++) {
                                JSONObject row = json.getJSONObject(i);
                                Log.e("zipCodesAlg ", "Response is" + row.getString("codePostal"));
                                zipCodes.add(row.getString("codePostal"));
                            }
                            dataAdapterZ.notifyDataSetChanged();
                            for (int i = 0; i < spinnerZ.getCount(); i++) {
                                Log.e("TOKEN ", "***zipCode spinner*** " + spinnerZ.getItemAtPosition(i));
                                if (spinnerZ.getItemAtPosition(i).equals(zipCode)) {
                                    spinnerZ.setSelection(i);
                                    break;
                                }
                            }
                        } catch (JSONException ex) {
                            ex.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Register ", "Error is" + error.toString());
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    public void getCountries() {
        Log.e(TAG, "get all countries");
        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(this.getActivity());

// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constants.countriesUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e(TAG, "Response is" + response);
                        try {
                            JSONArray json = new JSONArray(response);
                            for (int i = 0; i < json.length(); i++) {
                                JSONObject row = json.getJSONObject(i);
                                countries.add(row.getString("nomFrFr"));
                            }

                            dataAdapterC.notifyDataSetChanged();
                            for (int i = 0; i < spinnerC.getCount(); i++) {
                                Log.e("TOKEN ", "***country spinner*** " + spinnerC.getItemAtPosition(i));
                                if (spinnerC.getItemAtPosition(i).equals(country)) {
                                    spinnerC.setSelection(i);
                                    break;
                                }
                            }

                        } catch (JSONException ex) {
                            ex.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error is" + error.toString());
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    public boolean isTextValid(String text) {

        return Character.isDigit(text.charAt(0));

    }

    public boolean isMailValid(String email) {

        return EMAIL_ADDRESS_PATTERN.matcher(email).matches();

    }

    public boolean isNumberValid(String phone) {
        String MobilePattern = "[0-9]{8}";
        return phone.matches(MobilePattern);
    }

    @Override
    public void onClick(final View v) {
        /*if (v == addButton) {
            addMembersLayout();
        } else*/
        if (v == btn_edit) {
            editProfile();
        } else if (v == uploadButton) {
            uploadImage();
        } else {
            showCalendarFor(v);
        }
    }

    public void editProfile() {

        progressDoalog = new SweetAlertDialog(this.getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        progressDoalog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressDoalog.setTitleText("Chargement");
        progressDoalog.setCancelable(true);
        progressDoalog.show();

        View focusView = null;
        boolean cancel = false;

        name = txt_name.getText().toString();
        name2 = txt_name2.getText().toString();
        phone = txt_phone.getText().toString();
        email = txt_email.getText().toString();
        address = txt_address.getText().toString();
        nbrEnfants = txt_nbr_enfants.getText().toString();
        birthday = txt_birthday.getText().toString();
        govTxt = txt_gov.getText().toString();
        zipTxt = txt_zip.getText().toString();
        if (TextUtils.isEmpty(name)) {
            txt_name.setError(getString(R.string.error_field_required));
            focusView = txt_name;
            cancel = true;
        } else if (isTextValid(name)) {
            txt_name.setError(getString(R.string.error_invalid_nom));
            focusView = txt_name;
            cancel = true;
        }

        if (TextUtils.isEmpty(name2)) {
            txt_name2.setError(getString(R.string.error_field_required));
            focusView = txt_name2;
            cancel = true;
        } else if (isTextValid(name2)) {
            txt_name2.setError(getString(R.string.error_invalid_nom));
            focusView = txt_name2;
            cancel = true;
        }

        if (!(governates.size() > 0) && TextUtils.isEmpty(govTxt)) {
            txt_gov.setError(getString(R.string.error_field_required));
            focusView = txt_gov;
            cancel = true;
            progressDoalog.dismiss();
        }
        if (!(zipCodes.size() > 0) && TextUtils.isEmpty(zipTxt)) {
            txt_zip.setError(getString(R.string.error_field_required));
            focusView = txt_zip;
            cancel = true;
            progressDoalog.dismiss();
        }

        if (TextUtils.isEmpty(phone)) {
            txt_phone.setError(getString(R.string.error_field_required));
            focusView = txt_phone;
            cancel = true;
        } /*else if (!isNumberValid(phone)) {
            txt_phone.setError(getString(R.string.error_invalid_number_chiffres));
            focusView = txt_phone;
            cancel = true;
        }*/

        if (TextUtils.isEmpty(email)) {
            txt_email.setError(getString(R.string.error_field_required));
            focusView = txt_email;
            cancel = true;
        } else if (!isMailValid(email)) {
            txt_email.setError(getString(R.string.error_invalid_mail));
            focusView = txt_email;
            cancel = true;
        }

        if (TextUtils.isEmpty(address)) {
            txt_address.setError(getString(R.string.error_field_required));
            focusView = txt_address;
            cancel = true;
        } /*else if (isTextValid(address)) {
            txt_address.setError(getString(R.string.error_invalid_nom));
            focusView = txt_address;
            cancel = true;
        }*/

        if (TextUtils.isEmpty(nbrEnfants)) {
            txt_nbr_enfants.setError(getString(R.string.error_field_required));
            focusView = txt_nbr_enfants;
            cancel = true;
            progressDoalog.dismiss();
        }

        //birthday
        if (TextUtils.isEmpty(birthday)) {
            txt_birthday.setError(getString(R.string.error_field_required));
            focusView = txt_birthday;
            cancel = true;
            progressDoalog.dismiss();
        }

        if (age < 18) {
            //txt_birthday.setError("Age minimum est 18");
            focusView = txt_birthday;
            cancel = true;
            //progressDoalog.dismiss();
            progressDoalog.setTitleText("Erreur!")
                    .setContentText("Age minimum doit être 18")
                    .setConfirmText("OK")
                    .changeAlertType(SweetAlertDialog.ERROR_TYPE);
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            sendRequest(Constants.editProfilUrl);
        }
    }

    public void sendRequest(final String url) {

        try {
            RequestQueue queue = Volley.newRequestQueue(this.getActivity());
            JSONObject jsonBody = new JSONObject();

            jsonBody.put("id_client", getPreferences().getUser().getId_client());
            jsonBody.put("firstname", txt_name.getText().toString());
            jsonBody.put("lastname", txt_name2.getText().toString());
            jsonBody.put("gender", spinnerG.getSelectedItem().toString());
            jsonBody.put("address", txt_address.getText().toString());
            jsonBody.put("nbrEnfants", txt_nbr_enfants.getText().toString());
            jsonBody.put("phone", txt_phone.getText().toString());
            jsonBody.put("birthDay", txt_birthday.getText().toString());


            if (governates.size() > 0) {
                jsonBody.put("governera", spinner.getSelectedItem().toString());
            } else if (!(governates.size() > 0)) {
                //jsonBody.put("governorate", "");
                jsonBody.put("governera", txt_gov.getText().toString());
            }

            if (zipCodes.size() > 0) {
                jsonBody.put("zipCode", spinnerZ.getSelectedItem().toString());
            } else if (!(zipCodes.size() > 0)) {
                jsonBody.put("zipCode", txt_zip.getText().toString());
            }


            //jsonBody.put("governera", spinner.getSelectedItem().toString());
            //jsonBody.put("zipCode", spinnerZ.getSelectedItem().toString());
            jsonBody.put("nationality", spinnerC.getSelectedItem().toString());
            final String requestBody = jsonBody.toString();
            Log.e(TAG, "jsonBody " + requestBody);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e(TAG, "Response is " + response);
                    Log.i("VOLLEY", response);
                    parseProfileJSONResponse(response, url);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDoalog.setTitleText("Erreur!")
                            .setContentText("Une erreur est survenue, veuillez essayer plus tard.")
                            .setConfirmText("OK")
                            .changeAlertType(SweetAlertDialog.ERROR_TYPE);
                    Log.e("VOLLEY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("X-Auth-Token", getPreferences().getToken());

                    return params;
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }
            };
            queue.add(stringRequest);

            progressDoalog.setTitleText("Succès!")
                    .setContentText("Modification de votre profil avec succès.")
                    .setConfirmText("OK")
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            progressDoalog.dismiss();
                        }
                    })
                    .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void addMembersLayout() {

        LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT, 3);
        p.setMargins(0, 0, 0, 0);

        final LinearLayout firstlayout = new LinearLayout(this.getActivity());
        firstlayout.setOrientation(LinearLayout.HORIZONTAL);
        firstlayout.setWeightSum(3);
        LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT, txt_name.getHeight(), 1.4f);
        params1.setMargins(0, 0, 0, 10);

        final CustomEditText txt_mbr = new CustomEditText(this.getActivity());
        txt_mbr.setHint("Nom ");
        txt_mbr.setTextSize(14);
        txt_mbr.setPadding(0, 0, 0, 0);
        txt_mbr.setBackgroundResource(R.drawable.textlines);
        firstlayout.addView(txt_mbr, params1);

        LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT, txt_name.getHeight(), 1.5f);
        params2.setMargins(20, 0, 0, 10);

        final CustomEditText txt_date = new CustomEditText(this.getActivity());
        txt_date.setHint("Date de naissance");
        txt_date.setTextSize(14);
        txt_date.setPadding(0, 0, 0, 0);
        txt_date.setCursorVisible(false);

        txt_date.setFocusable(false);
        txt_date.setBackgroundResource(R.drawable.textlines);
        txt_date.setLayoutParams(params2);
        firstlayout.addView(txt_date);
        txt_date.setOnClickListener(new View.OnClickListener() {
            public void onClick(final View v) {

                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);
                Log.e(TAG, "Click to show calendar...");

                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                //if (v == txt_birthday) {
                                txt_date.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                                //}
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT, 0.05f);
        param.setMargins(5, 0, 0, 0);
        param.gravity = Gravity.CENTER_VERTICAL;
        final ImageView btn_moins = new ImageView(this.getActivity());
        btn_moins.setBackgroundResource(R.mipmap.ic_minus);

        btn_moins.setOnClickListener(new View.OnClickListener() {
            public void onClick(final View v) {

                firstlayout.removeView(txt_mbr);
                firstlayout.removeView(txt_date);
                firstlayout.removeView(btn_moins);
                txt_mbr.setVisibility(View.GONE);
                txt_date.setVisibility(View.GONE);
                btn_moins.setVisibility(View.GONE);
            }
        });

        firstlayout.addView(btn_moins, param);

        mainLinearLayout.addView(firstlayout, p);

        scrollView.post(new Runnable() {
            @Override
            public void run() {
                scrollView.scrollTo(0, scrollView.getBottom());
            }
        });
    }

    public void showCalendarFor(View v) {
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        //mYear = c.get(Calendar.YEAR);
        mYear = 2001;
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        Log.e(TAG, "Click to show calendar...");

        SupportedDatePickerDialog dpd =
                new SupportedDatePickerDialog(getContext(), R.style.SpinnerDatePickerDialogTheme, this, mYear, mMonth, mDay);
        dpd.getDatePicker().setMaxDate(c.getTimeInMillis());
        dpd.show();
    }

    @Override
    public void onDateSet(DatePicker datePicker, int mYear, int mMonth, int mDay) {
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, mYear);
        c.set(Calendar.MONTH, mMonth);
        c.set(Calendar.DAY_OF_MONTH, mDay);

        txt_birthday.setText(mYear + "-" + (mMonth + 1) + "-" + mDay);

        age = getAge(mYear, (mMonth + 1), mDay);
        Log.e("Res Age", "" + age);
    }

    public int getAge(int year, int month, int day) {
        GregorianCalendar cal = new GregorianCalendar();
        int y, m, d, age;
        y = cal.get(Calendar.YEAR);
        m = cal.get(Calendar.MONTH);
        d = cal.get(Calendar.DAY_OF_MONTH);
        cal.set(year, month, day);
        age = y - cal.get(Calendar.YEAR);
        if ((m < cal.get(Calendar.MONTH))
                || ((m == cal.get(Calendar.MONTH)) && (d < cal
                .get(Calendar.DAY_OF_MONTH)))) {
            --age;
        }
        if (age < 0)
            throw new IllegalArgumentException("Age < 0");
        return age;
    }

    public void uploadImage() {
        // if(arg0==btnselectpic)
        // {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Complete action using"), 1);
        // }
        /*else if (arg0==uploadButton) {

            dialog = ProgressDialog.show(MainActivity.this, "", "Uploading file...", true);
            messageText.setText("uploading started.....");
            new Thread(new Runnable() {
                public void run() {

                    uploadFile(imagepath);

                }
            }).start();
        }*/
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1 && resultCode == RESULT_OK) {
            //Bitmap photo = (Bitmap) data.getData().getPath();
            //Uri imagename=data.getData();
            Uri selectedImageUri = data.getData();
            imagepath = getPath(selectedImageUri);
            Bitmap bitmap = BitmapFactory.decodeFile(imagepath);
            //imageProfile.setImageBitmap(bitmap);
            imageProfile.setImageURI(selectedImageUri);
        }
    }

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = this.getActivity().managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public void getUserInfo() {

        progressDoalog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        progressDoalog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressDoalog.setTitleText("Chargement");
        progressDoalog.setCancelable(true);
        progressDoalog.show();

        try {
            RequestQueue queue = Volley.newRequestQueue(this.getActivity());
            JSONObject jsonBody = new JSONObject();
            Log.e("PROFIL ", "***" + getPreferences().getUser() + "***");

            if (getPreferences().getUser().getId_member() != null) {
                jsonBody.put("id_user", getPreferences().getUser().getId_member());
            }

            final String requestBody = jsonBody.toString();

            StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.profilInfosUrl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e("PROFIL", "Response is " + response);
                    Log.i("VOLLEY", response);
                    parseProfileJSONResponse(response, Constants.profilInfosUrl);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDoalog.setTitleText("Erreur!")
                            .setContentText("Une erreur est survenue, veuillez vérifier votre connexion Internet ou essayer plus tard.")
                            .setConfirmText("OK")
                            .changeAlertType(SweetAlertDialog.ERROR_TYPE);
                    Log.e("VOLLEYY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("X-Auth-Token", getPreferences().getToken());
                    return params;
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        Log.e("requestBody ", requestBody);
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }
                    /*@Override
                    protected Response<String> parseNetworkResponse(NetworkResponse response) {
                        String responseString = "";
                        if (response != null) {
                            responseString = String.valueOf(response);
                            Log.e(TAG, "ResponseString is " + response);
                            // can get more details such as response.headers
                        }
                        return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                    }*/
            };
            queue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String parseProfileJSONResponse(String jsonResponse, String url) {

        try {
            final JSONObject json = new JSONObject(jsonResponse);
            String code = json.getString("code");
            //progressDoalog.dismiss();

            if (code.equals("200")) {

                JSONObject response = json.getJSONObject("response");
                Log.e("Response", response.toString());
                //JSONObject user = response.getJSONObject("user");
                JSONObject client = response.getJSONObject("client");

                final String firstname = response.getString("firstname");
                txt_name.setText(firstname);
                Log.e("TOKEN ", "***firstname*** " + firstname);

                final String lasttname = response.getString("lastname");
                txt_name2.setText(lasttname);
                Log.e("TOKEN ", "***lastname*** " + lasttname);

                final String birthdate;
                final Integer y, m, d;
                if ((client.has("birthday")) && !client.isNull("birthday")) {
                    birthdate = client.getString("birthday").substring(0, 10);
                    y = Integer.valueOf(client.getString("birthday").substring(0, 4));
                    m = Integer.valueOf(client.getString("birthday").substring(5, 7));
                    d = Integer.valueOf(client.getString("birthday").substring(8, 10));
                    txt_birthday.setText(birthdate);
                    Log.e("TOKEN ", "***birthday*** " + birthdate);
                    //calcul age
                    age = getAge(y, m, d);
                } else {
                    birthdate = "";
                }

                String id_user = response.getString("id");
                Log.e("TOKEN ", "***id_user*** " + id_user);

                final String email = response.getString("mail");
                txt_email.setText(email);
                Log.e("TOKEN ", "***email*** " + email);

                final String phone = response.getString("phone");
                txt_phone.setText(phone);
                Log.e("TOKEN ", "***phone*** " + phone);

                final String address = client.getString("address");
                txt_address.setText(address);
                Log.e("TOKEN ", "***address*** " + address);

                //final String enfants = client.getString("nbrEnfants");
                //txt_nbr_enfants.setText(enfants);
                //Log.e("TOKEN ", "***enfants*** " + enfants);

                final String gender = response.getString("gender");
                Log.e("TOKEN ", "***gender*** " + gender);

                /*boolean spinnerFilled = false;
                do {

                    spinnerFilled = false;

                }while ( spinnerG.getCount() == 0 );
                if (spinnerFilled) {*/
                /*for (int i = 0; i < spinnerG.getCount(); i++) {
                    Log.e("TOKEN ", "***gender spinner*** " + spinnerG.getItemAtPosition(i));
                    if (!spinnerG.getItemAtPosition(i).toString().equals(gender)) {
                        spinnerG.setSelection(i);
                        break;
                    }
                }*/
                // }
                country = client.getString("nationality");
                Log.e("TOKEN ", "***country*** " + country);

                governate = client.getString("governorate");
                Log.e("TOKEN ", "***governorate*** " + governate);
                /*for (int i = 0; i < spinner.getCount(); i++) {
                    if (spinner.getItemAtPosition(i).toString().equals("")) {
                        txt_gov.setText("ttttttttttttttttt");
                    }
                }*/

                zipCode = client.getString("zipCode");
                Log.e("TOKEN ", "***zipCode*** " + zipCode);

                /*for (int i = 0; i < spinnerC.getCount(); i++) {
                    Log.e("TOKEN ", "***country spinner*** " + spinnerC.getItemAtPosition(i));
                    if (spinnerC.getItemAtPosition(i).toString().equals(country)) {

                        String pos = spinnerC.getItemAtPosition(i).toString();
                        Log.e("pos gov", "" + pos);

                        spinnerC.setSelection(i);
                        break;
                    }
                }*/

                String points = client.getString("points");
                Log.e("TOKEN ", "***points*** " + points);
                String clientType = client.getJSONObject("clientType").getString("type");
                Log.e("TOKEN ", "***clientType*** " + clientType);
                Carte crt = null;
                try {
                    if ((client.has("carte")) && (!client.isNull("carte"))) {
                        JSONObject carte = client.getJSONObject("carte");
                        Log.e("TOKEN ", "***carte*** " + carte);
                        String num_carte = "";
                        String type_carte = "";
                        if (!client.isNull("carte")) {
                            num_carte = carte.getString("code");
                            Log.e("TOKEN ", "***num_carte*** " + num_carte);
                            type_carte = carte.getJSONObject("type").getString("name");
                            Log.e("TOKEN ", "***type_carte*** " + type_carte);
                            crt = new Carte(num_carte, points, type_carte, id_user);
                        }
                    }
                    Client cl = new Client(getPreferences().getUserId(), id_user, lasttname, firstname, email, birthdate, address, phone, crt);
                    getPreferences().saveActiveUser(cl);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                /*if (url.equals(Constants.editProfilUrl)) {
                    progressDoalog.setTitleText("Succès!")
                            .setContentText("Modification de votre profil avec succès.")
                            .setConfirmText("OK")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    progressDoalog.dismissWithAnimation();
                                    txt_name.setText(lasttname);
                                    txt_name2.setText(firstname);
                                    txt_phone.setText(phone);
                                    txt_email.setText(email);
                                    txt_address.setText(address);
                                    //txt_birthday.setText("");
                                    txt_birthday.setText(birthdate);

                                    //txt_nbr_enfants.setText(enfants);
                                }
                            })
                            .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                    getFragmentManager().beginTransaction().detach(this).attach(this).commit();
                }*/

            } else if (code.equals("201")) {
                progressDoalog.setTitleText("Succès!")
                        .setContentText("Modification de votre profil avec succès.")
                        .setConfirmText("OK")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                progressDoalog.dismiss();
                            }
                        })
                        .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
            } else {
                progressDoalog.setTitleText("Erreur!")
                        .setContentText(getResources().getString(R.string.fb_connect_canceled))
                        .setConfirmText("OK")
                        .changeAlertType(SweetAlertDialog.ERROR_TYPE);
                String msgErr = json.getString("message");
                Log.e("msgErr ", msgErr);
            }
            return code;
        } catch (JSONException ex) {
            ex.printStackTrace();
            return "250";
        }
    }

    private void applyFontToSpinner(TextView v) {
        Typeface externalFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Montserrat-Regular.ttf");
        v.setTypeface(externalFont);
    }
}