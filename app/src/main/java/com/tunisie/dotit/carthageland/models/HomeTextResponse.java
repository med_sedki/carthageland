package com.tunisie.dotit.carthageland.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class HomeTextResponse {

    @SerializedName("code")
    private String code;
    @SerializedName("message")
    private String message;
    @SerializedName("response")
    private TextModel response ;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public TextModel getResponse() {
        return response;
    }

    public void setResponse(TextModel response) {
        this.response = response;
    }
}
