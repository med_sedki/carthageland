package com.tunisie.dotit.carthageland.dagger2;


import com.tunisie.dotit.carthageland.activities.BaseActivity;
import com.tunisie.dotit.carthageland.fragments.BaseFragment;
import com.tunisie.dotit.carthageland.global.SharedPreferences;

import dagger.Component;

/**
 * Created by MacBook on 8/28/17.
 */

@ApplicationScope
@Component(modules = {PreferencesModule.class})
public interface ApplicationComponent {

    SharedPreferences getPreferences();
    void inject(BaseActivity baseActivity);

    void inject(BaseFragment baseFragment);

}
