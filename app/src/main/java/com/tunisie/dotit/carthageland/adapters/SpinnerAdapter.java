package com.tunisie.dotit.carthageland.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.tunisie.dotit.carthageland.custom.CustomTextView;
import com.tunisie.dotit.carthageland.models.Subject;

/**
 * Created by eniso on 22/01/2018.
 */

public class SpinnerAdapter extends ArrayAdapter<Subject>{

    // Your sent context
    private Context context;
    // Your custom values for the spinner (User)
    private Subject[] values;

    public SpinnerAdapter(Context context, int textViewResourceId,
                       Subject[] values) {
        super(context, textViewResourceId, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public int getCount(){
        return values.length;
    }

    @Override
    public Subject getItem(int position){
        return values[position];
    }

    @Override
    public long getItemId(int position){
        return position;
    }


    // And the "magic" goes here
    // This is for the "passive" state of the spinner
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // I created a dynamic TextView here, but you can reference your own  custom layout for each spinner item
        TextView label = (TextView) super.getView(position, convertView, parent);

        label.setTextColor(Color.BLACK);
        applyFontToSpinner(label);
        // Then you can get the current item using the values array (Users array) and the current position
        // You can NOW reference each method you has created in your bean object (User class)
        label.setText(values[position].getName_subject());

        // And finally return your dynamic (or custom) view for each spinner item
        return label;
    }

    // And here is when the "chooser" is popped up
    // Normally is the same view, but you can customize it if you want
    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        TextView label = (TextView) super.getDropDownView(position, convertView, parent);
        //CustomTextView label = new CustomTextView(context);
        label.setTextColor(Color.BLACK);
        applyFontToSpinner(label);
        label.setText(values[position].getName_subject());

        return label;
    }

    private void applyFontToSpinner(TextView v) {
        Typeface externalFont=Typeface.createFromAsset(getContext().getAssets(), "fonts/Montserrat-Regular.ttf");


        v.setTypeface(externalFont);
    }

}
