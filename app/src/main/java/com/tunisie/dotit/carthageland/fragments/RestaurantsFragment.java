package com.tunisie.dotit.carthageland.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.tunisie.dotit.carthageland.R;
import com.tunisie.dotit.carthageland.Utils.Constants;
import com.tunisie.dotit.carthageland.activities.HomeActivity;
import com.tunisie.dotit.carthageland.adapters.RestaurantsAdapter;
import com.tunisie.dotit.carthageland.global.APIClient;
import com.tunisie.dotit.carthageland.global.APIInterface;
import com.tunisie.dotit.carthageland.models.POI;
import com.tunisie.dotit.carthageland.models.PoiResponse;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by dotit on 4/12/18.
 */

public class RestaurantsFragment extends BaseFragment {

    RecyclerView mRestaurantsRecycler;
    View mView;
    private APIInterface mApiInerface;
    private ArrayList<POI> mRestaurantsList = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.restaurants_fragment, container, false);
        HomeActivity.toolbar.setNavigationIcon(R.drawable.back_arrow_ic);
        HomeActivity.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getFragmentManager() != null) {
                    Fragment currentFragment = getFragmentManager().findFragmentById(R.id.flContent);
                    if (currentFragment instanceof RestaurantsFragment) {
                        navigateToHomeFragment();
                    }
                } else {
                    HomeActivity.openDrawer();
                }

            }
        });
        //this.getActivity().setTitle("Restaurants");
        mRestaurantsRecycler = mView.findViewById(R.id.restaurants_recycler);
        mRestaurantsRecycler.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        if (getPreferences().getCarthageLandLocation().toString().equals(Constants.Parameters.CL_TUNIS)) {
            getRerstaurantsList(Constants.Parameters.RESTAURANTS_PARAM, Constants.Sites.TUNIS_PARAM);
        } else if (getPreferences().getCarthageLandLocation().toString().equals(Constants.Parameters.CL_HAMMAMET)) {
            getRerstaurantsList(Constants.Parameters.RESTAURANTS_PARAM, Constants.Sites.HHAMMAMET_PARAM);
        }
        // mRestaurantsRecycler.setAdapter(new RestaurantsAdapter(getPreferences().get(),getPreferences().getCarthageLandLocation()));

        Toolbar toolbarTop = (Toolbar) getActivity().findViewById(R.id.toolbar);
        TextView mTitle = (TextView) toolbarTop.findViewById(R.id.toolbar_title);
        mTitle.setText("Nos restaurants");
        return mView;
    }

    public void getRerstaurantsList(final String category_param, final String site_praram) {
        mApiInerface = APIClient.getClient().create(APIInterface.class);

        showProgressBar();

        /**
         GET List Resources
         **/
        Call<PoiResponse> call = mApiInerface.getRestaurants(getPreferences().getToken(), category_param, site_praram);
        call.enqueue(new Callback<PoiResponse>() {
            @Override
            public void onResponse(Call<PoiResponse> call, Response<PoiResponse> response) {


                // Log.d("TAG", response.body().getResponse() + " response get poi ");
                Log.d("TAG", response + "");
                if (response.isSuccessful() && response.body() != null && response.body().getResponse() != null) {
                    if (!response.body().getResponse().isEmpty()) {
                        for (int i = 0; i < response.body().getResponse().size(); i++) {

                            POI poi = response.body().getResponse().get(i);
                            Log.e("TAG", poi.getName().toString());
                            mRestaurantsList.add(poi);

                        }
                        hideProgressBar();
                        mRestaurantsRecycler.setAdapter(new RestaurantsAdapter(mRestaurantsList, getPreferences().getCarthageLandLocation(), getActivity()));
                    } else {
                        hideProgressBar();
                        Toast.makeText(getActivity(), getString(R.string.error_empty_list), Toast.LENGTH_SHORT).show();
                    }

                } else {

                    Toast.makeText(getActivity(), getString(R.string.error_server), Toast.LENGTH_SHORT).show();
                    hideProgressBar();
                }

            }

            @Override
            public void onFailure(Call<PoiResponse> call, Throwable t) {
                hideProgressBar();
                call.cancel();
            }
        });
    }

    private void closeFragment() {
        getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
    }

    private void navigateToHomeFragment() {
        Fragment fragment = null;
        try {
            closeFragment();
            //fragment = HomeFragment.class.newInstance();

        } catch (Exception e) {
            e.printStackTrace();
        }

        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        //fragmentManager.beginTransaction().replace(R.id.flContent, fragment).addToBackStack("tag").commit();
        fragmentManager.popBackStack();
    }
}
