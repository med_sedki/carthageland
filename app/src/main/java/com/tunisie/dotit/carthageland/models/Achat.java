package com.tunisie.dotit.carthageland.models;

/**
 * Created by eniso on 06/10/2017.
 */

public class Achat {

    private String id_achat;
    private Produit produit;

    public Achat(String id_achat, Produit produit) {
        this.id_achat = id_achat;
        this.produit = produit;
    }

    public String getId_achat() {
        return id_achat;
    }

    public void setId_achat(String id_achat) {
        this.id_achat = id_achat;
    }

    public Produit getProduit() {
        return produit;
    }

    public void setProduit(Produit produit) {
        this.produit = produit;
    }
}
