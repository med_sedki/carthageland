package com.tunisie.dotit.carthageland.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by eniso on 06/10/2017.
 */

public class POI implements  Parcelable {

    @SerializedName("name")
    private String name;
    @SerializedName("description")
    private String description;
    @SerializedName("lat")
    private float lat;
    @SerializedName("lng")
    private float lng;
    @SerializedName("filename")
    private String filename;
    @SerializedName("taille")
    private String taille;
    @SerializedName("category")
    private CategoriePOI category;


    protected POI(Parcel in) {
        name = in.readString();
        description = in.readString();
        lat = in.readFloat();
        lng = in.readFloat();
        filename = in.readString();
        taille = in.readString();
    }

    public static final Creator<POI> CREATOR = new Creator<POI>() {
        @Override
        public POI createFromParcel(Parcel in) {
            return new POI(in);
        }

        @Override
        public POI[] newArray(int size) {
            return new POI[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getLat() {
        return lat;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }

    public float getLng() {
        return lng;
    }

    public void setLng(float lng) {
        this.lng = lng;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public Object getTaille() {
        return taille;
    }

    public void setTaille(String taille) {
        this.taille = taille;
    }

    public CategoriePOI getCategory() {
        return category;
    }

    public void setCategory(CategoriePOI category) {
        this.category = category;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

        parcel.writeString(name);
        parcel.writeString(description);
        parcel.writeFloat(lat);
        parcel.writeFloat(lng);
        parcel.writeString(filename);
        parcel.writeString(taille);
    }

}
