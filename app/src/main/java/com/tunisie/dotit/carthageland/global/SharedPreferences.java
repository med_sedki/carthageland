package com.tunisie.dotit.carthageland.global;

import android.content.Context;
import android.text.TextUtils;

import com.tunisie.dotit.carthageland.Utils.Constants;
import com.tunisie.dotit.carthageland.models.Carte;
import com.tunisie.dotit.carthageland.models.Client;
import com.tunisie.dotit.carthageland.models.POI;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.securepreferences.SecurePreferences;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created on 2/4/18.
 */

public class     SharedPreferences {

    private static final String USER_FLAG = "user_flag";
    private static final String PUBLIC_MODE = "public_mode_flag";
    private static final String USER_ID_FLAG = "user_id_flag";
    private static final String TOKEN_FLAG = "token_flag";
    private static final String POINTS_FLAG = "points_flag";
    private static final String CARD_FLAG = "card_flag";
    private static final String CARTHAGELAND_FLAG = "carthageland_flag";
    private static final String POI_FLAG = "poi_flag";
    private static final String EMAIL_FLAG = "email_flag";
    private static final String LOOTs_FLAG = "loots_flag";

    private static final String HOME_RESPONSE_FLAG = "home_response";
    private static final String LAST_CALL_TIME_FLAG = "last_call_time";
    private static final String NEXT_DRAW_FLAG = "next_draw";

    private static android.content.SharedPreferences mSharedPreferences;

    private static final String FILE_NAME_FLAG = "bingo_file.xml";

    private static final String SECRET_KEY = "secret_bingo_key";

    public SharedPreferences(Context context) {
        mSharedPreferences = new SecurePreferences(context, SECRET_KEY, FILE_NAME_FLAG);
    }

    public boolean isConnected() {
        return !TextUtils.isEmpty(getUserId());
    }

    public void setUseId(String token) {
        android.content.SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(USER_ID_FLAG, token);
        editor.commit();
    }

    public void setConnectedMode(String token) {
        android.content.SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(PUBLIC_MODE, token);
        editor.commit();
    }

    public String getConnectedMode() {
        return mSharedPreferences.getString(PUBLIC_MODE, null);
    }

    public static void set(String key, String value) {
        android.content.SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public String getUserId() {
        return mSharedPreferences.getString(USER_ID_FLAG, null);
    }

    public String getEmail() {
        return mSharedPreferences.getString(EMAIL_FLAG, null);
    }

    public void setEmail(String email) {
        android.content.SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(EMAIL_FLAG, email);
        editor.apply();
    }

    public void saveActiveUser(Client user) {
        android.content.SharedPreferences.Editor editor = mSharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(user);
        editor.putString(USER_FLAG, json);
        editor.apply();
    }

    public Client getUser() {
        Gson gson = new Gson();
        String json = mSharedPreferences.getString(USER_FLAG, null);
        return gson.fromJson(json, Client.class);
    }


    public void clearSharedPreferences() {
        setUseId(null);
        saveActiveUser(null);

    }

    public Carte getCard() {
        Gson gson = new Gson();
        String json = mSharedPreferences.getString(CARD_FLAG, null);
        return gson.fromJson(json, Carte.class);
    }

    public void setCard(Carte card) {
        android.content.SharedPreferences.Editor editor = mSharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(card);
        editor.putString(CARD_FLAG, json);
        editor.apply();
    }

    public void setToken(String token) {
        android.content.SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(TOKEN_FLAG, token);
        editor.commit();
    }

    public String getBalancePts() {
        return mSharedPreferences.getString(POINTS_FLAG, null);
    }

    public void setBalancePts(String points) {
        android.content.SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(POINTS_FLAG, points);
        editor.commit();
    }

    public String getToken() {
        return mSharedPreferences.getString(TOKEN_FLAG, null);
    }

    public void setCarthageLandLocation(String token) {
        android.content.SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(CARTHAGELAND_FLAG, token);
        editor.commit();
    }

    public String getCarthageLandLocation() {
        return mSharedPreferences.getString(CARTHAGELAND_FLAG, null);
    }

    public void saveHammametPois(List<POI> availableWinnings) {
        String listString = new Gson().toJson(availableWinnings);
        mSharedPreferences.edit().putString(Constants.Parameters.HAMMAMET_POIs, listString).apply();
    }

    public ArrayList<POI> getHammametPois() {
        Type type = new TypeToken<ArrayList<POI>>() {
        }.getType();
        return new Gson().fromJson(mSharedPreferences.getString(Constants.Parameters.HAMMAMET_POIs, ""), type);
    }

    public void saveTunisPois(List<POI> availableWinnings) {
        String listString = new Gson().toJson(availableWinnings);
        mSharedPreferences.edit().putString(Constants.Parameters.TUNIS_POIs, listString).apply();
    }

    public ArrayList<POI> getTunisPois() {
        Type type = new TypeToken<ArrayList<POI>>() {
        }.getType();
        return new Gson().fromJson(mSharedPreferences.getString(Constants.Parameters.TUNIS_POIs, ""), type);
    }

    /*public void saveTunisParcs(List<POI> availableWinnings) {
        String listString = new Gson().toJson(availableWinnings);
        mSharedPreferences.edit().putString(Constants.Parameters.TUNIS_POIs, listString).apply();
    }

    public ArrayList<POI> getTunisParcs() {
        Type type = new TypeToken<ArrayList<POI>>() {
        }.getType();
        return new Gson().fromJson(mSharedPreferences.getString(Constants.Parameters.TUNIS_POIs, ""), type);
    }
    public void saveTunisPois(List<POI> availableWinnings) {
        String listString = new Gson().toJson(availableWinnings);
        mSharedPreferences.edit().putString(Constants.Parameters.TUNIS_POIs, listString).apply();
    }

    public ArrayList<POI> getTunisPois() {
        Type type = new TypeToken<ArrayList<POI>>() {
        }.getType();
        return new Gson().fromJson(mSharedPreferences.getString(Constants.Parameters.TUNIS_POIs, ""), type);
    }
    public void saveTunisPois(List<POI> availableWinnings) {
        String listString = new Gson().toJson(availableWinnings);
        mSharedPreferences.edit().putString(Constants.Parameters.TUNIS_POIs, listString).apply();
    }

    public ArrayList<POI> getTunisPois() {
        Type type = new TypeToken<ArrayList<POI>>() {
        }.getType();
        return new Gson().fromJson(mSharedPreferences.getString(Constants.Parameters.TUNIS_POIs, ""), type);
    }
    public void saveTunisPois(List<POI> availableWinnings) {
        String listString = new Gson().toJson(availableWinnings);
        mSharedPreferences.edit().putString(Constants.Parameters.TUNIS_POIs, listString).apply();
    }

    public ArrayList<POI> getTunisPois() {
        Type type = new TypeToken<ArrayList<POI>>() {
        }.getType();
        return new Gson().fromJson(mSharedPreferences.getString(Constants.Parameters.TUNIS_POIs, ""), type);
    }*/


}
