package com.tunisie.dotit.carthageland.adapters.holder;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tunisie.dotit.carthageland.R;
import com.tunisie.dotit.carthageland.Utils.Constants;
import com.tunisie.dotit.carthageland.application.BaseApplication;
import com.tunisie.dotit.carthageland.global.AttractionItemClickListener;
import com.tunisie.dotit.carthageland.models.POI;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Created on 2/15/18.
 */

public class AttractionHolder extends BaseHolder {

    TextView mTitle, mDetails, mFidelityPoints;
    CircleImageView mProductImage;
    LinearLayout mAttractionLayout;
    AttractionItemClickListener  mAttratcionItemClickListener ;

    public AttractionHolder(View itemView) {
        super(itemView);
        mTitle = itemView.findViewById(R.id.marker_title);
        mDetails = itemView.findViewById(R.id.marker_description);
        mProductImage = itemView.findViewById(R.id.marker_image);
        mAttractionLayout = itemView.findViewById(R.id.attraction_layout);

    }

    public void bind(final POI menuItem, AttractionItemClickListener attractionItemClickListener) {

        mTitle.setText(menuItem.getName());
        //mDetails.setText(menuItem.getDescription());
        if (menuItem.getDescription() != null) {

            setCustomDescriptionText(menuItem.getDescription());
        }

        if (menuItem.getFilename() != null) {
            String poiImage = menuItem.getFilename();
            Picasso.with(BaseApplication.getInstance()).load(Constants.poiImmagesUrl + poiImage).placeholder(R.drawable.placeholder).centerInside().fit().into(mProductImage);
        }

        mAttractionLayout.setOnClickListener(getItemClickListener(menuItem, attractionItemClickListener));


    }

    @NonNull
    private View.OnClickListener getItemClickListener(final POI menuItem, final AttractionItemClickListener attractionItemClickListener) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attractionItemClickListener.onItemClicked(menuItem);
            }
        };
    }


    private void setCustomDescriptionText(String description_label) {
        try {
            String sub_description = description_label.substring(0, 20);
            mDetails.setText(sub_description + BaseApplication.getInstance().getResources().getString(R.string.more_details));
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
            mDetails.setText(description_label + BaseApplication.getInstance().getResources().getString(R.string.more_details));
        }

    }

}
