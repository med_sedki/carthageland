package com.tunisie.dotit.carthageland;

import android.app.Application;
import android.util.Log;

import com.tunisie.dotit.carthageland.models.Carte;
import com.tunisie.dotit.carthageland.models.Client;

/**
 * Created by eniso on 06/10/2017.
 */

public class BaseApplication extends Application {


    private String carthage_location;
    private String email;
    private String token;
    private String id_user;
    private Client client;
    private Carte carte;

    @Override
    public void onCreate() {
        super.onCreate();

        Log.e("BaseApplication","onCreate");


    }

    public String getCarthage_location() {
        return carthage_location;
    }

    public void setCarthage_location(String carthage_location) {
        this.carthage_location = carthage_location;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Carte getCarte() {
        return carte;
    }

    public void setCarte(Carte carte) {
        this.carte = carte;
    }
}
