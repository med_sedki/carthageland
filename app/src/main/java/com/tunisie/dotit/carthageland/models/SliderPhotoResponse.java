package com.tunisie.dotit.carthageland.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Sedki YOUZBECHI  on 22/03/2021.
 */
public class SliderPhotoResponse {

    @SerializedName("code")
    private int code;
    @SerializedName("message")
    private String message;
    @SerializedName("response")
    private List<SlidersListResponse> response;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<SlidersListResponse> getResponse() {
        return response;
    }

    public void setResponse(List<SlidersListResponse> response) {
        this.response = response;
    }

}
