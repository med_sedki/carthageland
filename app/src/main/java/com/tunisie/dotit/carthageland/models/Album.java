package com.tunisie.dotit.carthageland.models;

/**
 * Created by eniso on 06/10/2017.
 */

public class
Album {

    private String id_album;
    private String id_client;
    private int nbreImages;
    private String dateCreation;
    private String name;
    private int thumbnail;

    public Album(String name, String id_album, String id_client, int nbreImages, String dateCreation, int thumbnail) {
        this.id_album = id_album;
        this.id_client = id_client;
        this.nbreImages = nbreImages;
        this.dateCreation = dateCreation;
        this.name = name;
        this.nbreImages = nbreImages;
        this.thumbnail = thumbnail;
    }

    public Album(String name, int nbreImages, int thumbnail) {
        this.name = name;
        this.nbreImages = nbreImages;
        this.thumbnail = thumbnail;
    }

    public String getId_album() {
        return id_album;
    }

    public void setId_album(String id_album) {
        this.id_album = id_album;
    }

    public String getId_client() {
        return id_client;
    }

    public void setId_client(String id_client) {
        this.id_client = id_client;
    }

    public int getNbreImages() {
        return nbreImages;
    }

    public void setNbreImages(int nbreImages) {
        this.nbreImages = nbreImages;
    }

    public String getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(String dateCreation) {
        this.dateCreation = dateCreation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(int thumbnail) {
        this.thumbnail = thumbnail;
    }
}
