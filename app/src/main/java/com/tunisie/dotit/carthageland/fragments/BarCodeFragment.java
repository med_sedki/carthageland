package com.tunisie.dotit.carthageland.fragments;


import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.ResultPoint;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.integration.android.IntentIntegrator;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.CompoundBarcodeView;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.tunisie.dotit.carthageland.R;
import com.tunisie.dotit.carthageland.Utils.Constants;
import com.tunisie.dotit.carthageland.activities.HomeActivity;
import com.tunisie.dotit.carthageland.application.BaseApplication;
import com.tunisie.dotit.carthageland.custom.CustomButton;
import com.tunisie.dotit.carthageland.custom.CustomEditText;
import com.tunisie.dotit.carthageland.custom.CustomTextView;
import com.tunisie.dotit.carthageland.models.Carte;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class BarCodeFragment extends BaseFragment implements View.OnClickListener {

    View view;
    CustomTextView code_carte;
    TextView type_carte;
    CustomTextView btn_assign;
    CustomTextView btn_confirm;
    CustomEditText txt_num;
    CustomButton scanBtn;
    ImageView image_card;
    BaseApplication myApp;
    Carte carte;
    CompoundBarcodeView barcodeView;
    private SweetAlertDialog progressDoalog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_bar_code, container, false);
        myApp = (BaseApplication) getActivity().getApplication();
        //this.getActivity().setTitle("Code à barre");
        Toolbar toolbarTop = (Toolbar) getActivity().findViewById(R.id.toolbar);
        TextView mTitle = (TextView) toolbarTop.findViewById(R.id.toolbar_title);
        mTitle.setText("Code à barre");
        code_carte = view.findViewById(R.id.code_carte);
        type_carte = view.findViewById(R.id.type_carte);
        btn_assign = view.findViewById(R.id.btn_assign);
        btn_confirm = view.findViewById(R.id.btn_confirm);
        barcodeView = view.findViewById(R.id.barcode_scanner);
        scanBtn = view.findViewById(R.id.btn_scan);
        scanBtn.setOnClickListener(this);

        HomeActivity.toolbar.setNavigationIcon(R.drawable.drawer_menu_ic);
        HomeActivity.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeActivity.openDrawer();
            }
        });

        btn_assign.setOnClickListener(this);
        btn_confirm.setOnClickListener(this);
        txt_num = view.findViewById(R.id.txt_num);
        image_card = view.findViewById(R.id.image_card);

        carte = getPreferences().getCard();
        if (carte != null) {
            code_carte.setText(getPreferences().getCard().getNum_carte());
            type_carte.setText("carte " + getPreferences().getCard().getType());
            code_carte.setVisibility(View.VISIBLE);
            btn_assign.setVisibility(View.GONE);
            image_card.setVisibility(View.VISIBLE);
            getBitmapImageCode();
        } else {
            type_carte.setText("Aucune carte assignée");
            code_carte.setVisibility(View.GONE);
            btn_assign.setVisibility(View.VISIBLE);
            image_card.setVisibility(View.GONE);
        }
        IntentIntegrator integrator = IntentIntegrator.forSupportFragment(this);
        barcodeView.decodeContinuous(callback);
        return view;
    }

    private BarcodeCallback callback = new BarcodeCallback() {
        @Override
        public void barcodeResult(BarcodeResult result) {
            if (result.getText() != null) {
                barcodeView.setStatusText(result.getText());
                txt_num.setText(result.getText());
            }
            //Do something with code result
        }

        @Override
        public void possibleResultPoints(List<ResultPoint> resultPoints) {
        }
    };

    public void getBitmapImageCode() {
        Bitmap bitmap = null;
        try {
            bitmap = encodeAsBitmap(carte.getNum_carte(), BarcodeFormat.CODE_128, 600, 300);
            image_card.setImageBitmap(bitmap);
        } catch (WriterException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        barcodeView.resume();
        super.onResume();
    }

    @Override
    public void onPause() {
        barcodeView.pause();
        super.onPause();
    }

    private static final int WHITE = 0xFFFFFFFF;
    private static final int BLACK = 0xFF000000;

    Bitmap encodeAsBitmap(String contents, BarcodeFormat format, int img_width, int img_height) throws WriterException {
        String contentsToEncode = contents;
        if (contentsToEncode == null) {
            return null;
        }
        Map<EncodeHintType, Object> hints = null;
        String encoding = guessAppropriateEncoding(contentsToEncode);
        if (encoding != null) {
            hints = new EnumMap<EncodeHintType, Object>(EncodeHintType.class);
            hints.put(EncodeHintType.CHARACTER_SET, encoding);
        }
        MultiFormatWriter writer = new MultiFormatWriter();
        BitMatrix result;
        try {
            result = writer.encode(contentsToEncode, format, img_width, img_height, hints);
        } catch (IllegalArgumentException iae) {
            // Unsupported format
            return null;
        }
        int width = result.getWidth();
        int height = result.getHeight();
        int[] pixels = new int[width * height];
        for (int y = 0; y < height; y++) {
            int offset = y * width;
            for (int x = 0; x < width; x++) {
                pixels[offset + x] = result.get(x, y) ? BLACK : WHITE;
            }
        }

        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
        return bitmap;
    }

    private static String guessAppropriateEncoding(CharSequence contents) {
        // Very crude at the moment
        for (int i = 0; i < contents.length(); i++) {
            if (contents.charAt(i) > 0xFF) {
                return "UTF-8";
            }
        }
        return null;
    }

    public void assignCarte(final String num_carte) {

        progressDoalog = new SweetAlertDialog(this.getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        progressDoalog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressDoalog.setTitleText("Chargement");
        progressDoalog.setCancelable(true);
        progressDoalog.show();

        try {
            RequestQueue queue = Volley.newRequestQueue(this.getActivity());
            JSONObject jsonBody = new JSONObject();
            Log.e("PROFIL ", "***" + getPreferences().getUser() + "***");

            jsonBody.put("id_client", getPreferences().getUser().getId_client());
            jsonBody.put("code_carte", num_carte);

            final String requestBody = jsonBody.toString();

            StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.assignCarteUrl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e("ASSIGN CARTE", "Response is " + response);
                    Log.i("VOLLEY", response);
                    parseJSONResponse(response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDoalog.setTitleText("Erreur!")
                            .setContentText("Une erreur est survenue, veuillez vérifier votre connexion Internet ou essayer plus tard.")
                            .setConfirmText("OK")
                            .changeAlertType(SweetAlertDialog.ERROR_TYPE);
                    Log.e("VOLLEYY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("X-Auth-Token", getPreferences().getToken());
                    return params;
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        Log.e("requestBody ", requestBody);
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }
            };
            queue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String parseJSONResponse(String jsonResponse) {
        try {
            final JSONObject json = new JSONObject(jsonResponse);
            String code = json.getString("code");
            Log.e("Scan json", String.valueOf(code));

            if (code.equals("200")) {
                try {
                    carte = new Carte(txt_num.getText().toString(), carte.getSolde_points(), carte.getType().toString(), getPreferences().getUser().getId_client());
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }

                getPreferences().setCard(carte);
                code_carte.setVisibility(View.VISIBLE);
                code_carte.setText(getPreferences().getCard().getNum_carte());
                type_carte.setVisibility(View.VISIBLE);
                type_carte.setText("Carte " + getPreferences().getCard());
                getBitmapImageCode();

                progressDoalog.setTitleText("Succès!")
                        .setContentText("Carte assignée avec succès")
                        .setConfirmText("OK")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                progressDoalog.dismissWithAnimation();
                            }
                        })
                        .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                scanBtn.setVisibility(View.GONE);
                txt_num.setVisibility(View.GONE);
                btn_confirm.setVisibility(View.GONE);
            } else {
                progressDoalog.setTitleText("Erreur!")
                        .setContentText(getResources().getString(R.string.fb_connect_canceled))
                        .setConfirmText("OK")
                        .changeAlertType(SweetAlertDialog.ERROR_TYPE);
                String msgErr = json.getString("message");
                Log.e("msgErr ", msgErr);
            }
            return code;
        } catch (JSONException ex) {
            ex.printStackTrace();
            return "250";
        }
    }

    public void onClick(View v) {
        if (v == scanBtn) {
            barcodeView.setVisibility(View.VISIBLE);
        } else if (v == btn_assign) {
            scanBtn.setVisibility(View.VISIBLE);
            txt_num.setVisibility(View.VISIBLE);
            btn_confirm.setVisibility(View.VISIBLE);
        } else if (v == btn_confirm) {
            if (txt_num.getText().toString() != null) {
                assignCarte(txt_num.getText().toString());
            }
        }
    }

}