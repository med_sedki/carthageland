package com.tunisie.dotit.carthageland.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by dotit on 4/11/18.
 */

public class Restaurant implements Parcelable {
    private int mTitle;
    private int mDetails;


    protected Restaurant(Parcel in) {
        mTitle = in.readInt();
        mDetails = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mTitle);
        dest.writeInt(mDetails);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Restaurant> CREATOR = new Creator<Restaurant>() {
        @Override
        public Restaurant createFromParcel(Parcel in) {
            return new Restaurant(in);
        }

        @Override
        public Restaurant[] newArray(int size) {
            return new Restaurant[size];
        }
    };

    public int getTitle() {
        return mTitle;
    }

    public void setTitle(int title) {
        this.mTitle = title;
    }

    public int getDetails() {
        return mDetails;
    }

    public void setDetails(int details) {
        this.mDetails = details;
    }



    public Restaurant(int title,int details) {
        this.mTitle = title;
        this.mDetails = details;
    }
}
