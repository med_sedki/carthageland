package com.tunisie.dotit.carthageland.adapters.holder;

import android.view.View;

import com.tunisie.dotit.carthageland.R;
import com.tunisie.dotit.carthageland.custom.CustomTextView;
import com.tunisie.dotit.carthageland.models.Historique;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Created on 2/15/18.
 */

public class HistoryHolder extends BaseHolder {

    public CustomTextView date;
    public CustomTextView code;
    public CustomTextView montant;
    public CustomTextView points;


    public HistoryHolder(View itemView) {
        super(itemView);
         code = itemView.findViewById(R.id.code);

         montant = itemView.findViewById(R.id.montant);

         date = itemView.findViewById(R.id.first_date);

         points = itemView.findViewById(R.id.points);
    }

    public void bind(final Historique menuHistorique) throws ParseException {
        String CurrentString = menuHistorique.getId_historique().toString() ;
        String[] separated = CurrentString.split("\\+");
        SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
        Date hitsoryItemDate = myFormat.parse(separated[0]);
        SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy HH:mm:ss");
        String newFormat = fmtOut.format(hitsoryItemDate);
        date.setText(newFormat.toString());
        code.setText(menuHistorique.getId_historique());
        montant.setText(menuHistorique.getCreationDate());
        points.setText("-"+menuHistorique.getMontant() + " points");
    }

}
