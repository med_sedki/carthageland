package com.tunisie.dotit.carthageland.models;

/**
 * Created by eniso on 20/12/2017.
 */

public class Subject {

    private String id_subject;
    private String name_subject;

    public Subject(String id_subject, String name_subject) {
        this.id_subject = id_subject;
        this.name_subject = name_subject;
    }

    public String getId_subject() {
        return id_subject;
    }

    public void setId_subject(String id_subject) {
        this.id_subject = id_subject;
    }

    public String getName_subject() {
        return name_subject;
    }

    public void setName_subject(String name_subject) {
        this.name_subject = name_subject;
    }
}
