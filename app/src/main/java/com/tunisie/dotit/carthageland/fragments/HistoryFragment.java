package com.tunisie.dotit.carthageland.fragments;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.tunisie.dotit.carthageland.activities.HomeActivity;
import com.tunisie.dotit.carthageland.adapters.HistoryLogAdapter;
import com.tunisie.dotit.carthageland.application.BaseApplication;
import com.tunisie.dotit.carthageland.Utils.Constants;
import com.tunisie.dotit.carthageland.R;
import com.tunisie.dotit.carthageland.custom.CustomProgressBar;
import com.tunisie.dotit.carthageland.custom.CustomTextView;
import com.tunisie.dotit.carthageland.models.Historique;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class HistoryFragment extends BaseFragment {

    View view;
    BaseApplication myApp;
    private SweetAlertDialog progressDoalog;
    RelativeLayout _linearL;
    RelativeLayout _linearL2;
    LinearLayout _linearL3;
    CustomProgressBar mProgress;
    TextView pts_txt;
    RecyclerView mVouchersRecycler;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_history, container, false);
        myApp = ((BaseApplication) this.getActivity().getApplication());
        _linearL = view.findViewById(R.id.histos);
        mVouchersRecycler = view.findViewById(R.id.vouchers_recycler);
        _linearL2 = view.findViewById(R.id.histosB);
        _linearL3 = (LinearLayout) view.findViewById(R.id.histosA);
        mProgress = view.findViewById(R.id.progress_bar);
        mProgress.setVisibility(View.VISIBLE);
        pts_txt = view.findViewById(R.id.pts_txt);
        HomeActivity.toolbar.setNavigationIcon(R.drawable.back_arrow_ic);
        HomeActivity.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getFragmentManager() != null) {
                    Fragment currentFragment = getFragmentManager().findFragmentById(R.id.flContent);
                    if (currentFragment instanceof HistoryFragment) {
                        navigateToHomeFragment();

                    }
                } else {
                    HomeActivity.openDrawer();
                }

            }
        });
        progressDoalog = new SweetAlertDialog(this.getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        progressDoalog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressDoalog.setTitleText("Chargement");
        progressDoalog.setCancelable(true);
        progressDoalog.show();
        getHistory(Constants.pointsConfigUrl);
        getHistory(Constants.historiqueUrl);
        getHistory(Constants.historiqueVoucherUrl);
        getHistory(Constants.historiqueVBoutiqueUrl);

        Toolbar toolbarTop = (Toolbar) getActivity().findViewById(R.id.toolbar);
        TextView mTitle = (TextView) toolbarTop.findViewById(R.id.toolbar_title);
        mTitle.setText("Historique");

        return view;
    }

    private void closeFragment() {
        getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();

    }
    private void navigateToSmilesFragment()
    {
        Fragment fragment = null;
        try {
            closeFragment();
            fragment = SmilesFragment.class.newInstance();

        } catch (Exception e) {
            e.printStackTrace();
        }

        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();
    }

    public void fragmentreplace(Class fragmentClass,String tag) throws IllegalAccessException, java.lang.InstantiationException {
        FragmentManager fragmentManager=getFragmentManager();
        Fragment  fragment = (Fragment) fragmentClass.newInstance();
        FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.flContent,fragment , tag);
        fragmentTransaction.addToBackStack("tag"); //this will add it to back stack
        fragmentTransaction.commit();
    }

    private void navigateToHomeFragment() {
        Fragment fragment = null;
        try {
            closeFragment();
            fragment = HomeFragment.class.newInstance();

        } catch (Exception e) {
            e.printStackTrace();
        }

        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).addToBackStack(null).commit();
    }

    public void getHistory(final String url) {


        try {
            mProgress.setVisibility(View.VISIBLE);
            RequestQueue queue = Volley.newRequestQueue(this.getActivity());
            JSONObject jsonBody = new JSONObject();
            Log.e("PROFIL ", "***" + getPreferences().getUser() + "***");
            if (url.equals(Constants.historiqueUrl)) {
                jsonBody.put("id_user", Integer.parseInt(getPreferences().getUser().getId_client()));
            } else {
                jsonBody.put("id_client", Integer.parseInt(getPreferences().getUser().getId_client()));
            }


            final String requestBody = jsonBody.toString();

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e("HISTORY", "url " + url);
                    Log.e("HISTORY", "url " + response);


                    parseJSONResponse(response, url);

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDoalog.setTitleText("Erreur!")
                            .setContentText("Une erreur est survenue, veuillez vérifier votre connexion Internet ou essayer plus tard.")
                            .setConfirmText("OK")
                            .changeAlertType(SweetAlertDialog.ERROR_TYPE);
                    mProgress.setVisibility(View.GONE);
                    Log.e("VOLLEYY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("X-Auth-Token", getPreferences().getToken());

                    return params;
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        Log.e("requestBody ", requestBody);
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }

            };

            queue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public String parseJSONResponse(String jsonResponse, String url) {

        try {

            final JSONObject json = new JSONObject(jsonResponse);

            String code = json.getString("code");

            if (code.equals("200")) {
                progressDoalog.dismiss();
                if (url.equals(Constants.pointsConfigUrl)) {
                    JSONObject response = json.getJSONObject("response");
                    if (response.getString("bienvenue").equals("0")) {
                        pts_txt.setVisibility(View.GONE);
                    } else {
                        pts_txt.setText(response.getString("bienvenue") + " points");
                    }

                } else if (url.equals(Constants.historiqueUrl)) {
                    JSONArray histo = json.getJSONArray("response");
                    for (int i = 0; i < histo.length(); i++) {
                        int montant = 0;
                        JSONObject obj = (JSONObject) histo.get(i);
                        Historique history = new Historique();
                        history.setCreationDate(obj.getString("createdAt"));

                        history.setPointsF(obj.getString("fidelityPoints"));
                        history.setId_historique(obj.getString("code"));
                        JSONArray lignesC = obj.getJSONArray("lignesCommandes");
                        for (int j = 0; j < lignesC.length(); j++) {
                            JSONObject ligne = (JSONObject) lignesC.get(j);
                            montant += Integer.parseInt(ligne.getString("prix"));
                        }
                        /*List<Historique> historyList ;
                        historyList = fromJson(histo);
                        mProgress.setVisibility(View.GONE);
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
                        mVouchersRecycler.setAdapter(new HistoryLogAdapter(historyList));
                        mVouchersRecycler.setLayoutManager(linearLayoutManager);*/
                        history.setMontant(String.valueOf(montant));
                        //history.setAvailabilityDate("");
                        history.setType("");
                        history.setName("");
                        addViews(history, Constants.historiqueUrl);
                    }
                } else if (url.equals(Constants.historiqueVoucherUrl)) {
                        JSONArray histo = json.getJSONArray("response");
                    List<Historique> historyList;
                        historyList = fromJson(histo);
                        mProgress.setVisibility(View.GONE);
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
                        mVouchersRecycler.setAdapter(new HistoryLogAdapter(historyList));
                        mVouchersRecycler.setLayoutManager(linearLayoutManager);
                        mVouchersRecycler.setVisibility(View.VISIBLE);
                    /*for (int i = 0; i < histo.length(); i++) {
                        JSONObject obj = (JSONObject) histo.get(i);
                        Historique history = new Historique();
                        history.setCreationDate(obj.getString("createdAt"));
                        history.setMontant(obj.getString("solde"));
                        history.setPointsF(obj.getString("pointConverted"));
                        history.setId_historique(obj.getString("qrcode"));
                        history.setType("");
                        history.setName("");

                        //addViews(history, Constants.historiqueVoucherUrl);

                    }*/
                } else if (url.equals(Constants.historiqueVBoutiqueUrl)) {
                    JSONArray histo = json.getJSONArray("response");
                    for (int i = 0; i < histo.length(); i++) {
                        JSONObject obj = (JSONObject) histo.get(i);
                        Historique history = new Historique();
                        //history.setAvailabilityDate(obj.getString("valableAt"));
                        history.setCreationDate(obj.getString("createdAt"));
                        history.setPointsF(obj.getString("pointConverted"));
                        history.setId_historique(obj.getString("qrcode"));
                        addViews(history, Constants.historiqueVBoutiqueUrl);
                    }
                }


            } else {
                progressDoalog.setTitleText("Erreur!")
                        .setContentText("Veuillez vérifier votre login/mot de passe")
                        .setConfirmText("OK")
                        .changeAlertType(SweetAlertDialog.ERROR_TYPE);
                String msgErr = json.getString("message");
                Log.e("msgErr ", msgErr);

            }
            return code;

        } catch (JSONException ex) {

            ex.printStackTrace();
            return "250";
        }
    }

    public static List<Historique> fromJson(JSONArray array) throws JSONException {
        ArrayList<Historique> res = new ArrayList<>();
        for (int i = 0; i < array.length(); ++i) {
            JSONObject beacon = array.getJSONObject(i);
            //res.add(new Historique(beacon.getString("createdAt"),beacon.getString("valableAt"), beacon.getString("solde"), beacon.getString("pointConverted"), beacon.getString("qrcode"), null, null));
            res.add(new Historique(beacon.getString("createdAt"), beacon.getString("solde"), beacon.getString("pointConverted"), beacon.getString("qrcode"), null, null));
        }

        return res;
    }

    private void addViews(Historique history, String url) {

        View v = LayoutInflater.from(getActivity()).inflate(R.layout.item_history, null);


        String CurrentString = history.getCreationDate().toString() ;
        String[] separated = CurrentString.split("\\+");
        SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
        Date hitsoryItemDate = null;
        try {
            hitsoryItemDate = myFormat.parse(separated[0]);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM, yyyy HH:mm:ss");
        String newFormat = fmtOut.format(hitsoryItemDate);

        /*String CurrentString1 = history.getAvailabilityDate().toString() ;
        String[] separated1 = CurrentString1.split("\\+");
        SimpleDateFormat myFormat1 = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
        Date hitsoryItemDate1 = null;
        try {
            hitsoryItemDate1 = myFormat1.parse(separated1[0]);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat fmtOut1 = new SimpleDateFormat("dd MMM, yyyy HH:mm:ss");
        String newFormat1 = fmtOut1.format(hitsoryItemDate1);*/

        CustomTextView type = v.findViewById(R.id.type);

        CustomTextView code = v.findViewById(R.id.code);
        code.setText(history.getId_historique());

        CustomTextView montant = v.findViewById(R.id.montant);
        montant.setText(history.getMontant() + " DT");

        CustomTextView creationDate = v.findViewById(R.id.first_date);


        CustomTextView availabitlityDate = v.findViewById(R.id.second_date);

        creationDate.setText(newFormat);

        CustomTextView points = v.findViewById(R.id.points);
        points.setText("-" + history.getPointsF() + " points");
        if (url.equals(Constants.historiqueVoucherUrl)) {
            _linearL.addView(v);
            _linearL.requestLayout();
            type.setVisibility(View.GONE);
        } else if (url.equals(Constants.historiqueVBoutiqueUrl)) {
            _linearL2.addView(v);
            _linearL2.requestLayout();
            type.setVisibility(View.VISIBLE);
            montant.setVisibility(View.INVISIBLE);
            code.setVisibility(View.VISIBLE);
           // code.setText(history.getPointsF());
            //availabitlityDate.setText(newFormat1);



           // type.setText(history.getId_historique());
        } else if (url.equals(Constants.historiqueUrl)) {
            _linearL3.addView(v);
            _linearL3.requestLayout();
            type.setVisibility(View.GONE);
        }




    }

}
