package com.tunisie.dotit.carthageland.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by dotit on 4/11/18.
 */

public class Parc implements Parcelable{
    @SerializedName("code")
    private String code;
    @SerializedName("name")
    private String name;
    @SerializedName("site")
    private String site;
    @SerializedName("filename")
    private String filename;
    @SerializedName("description")
    private String description;

    protected Parc(Parcel in) {
        code = in.readString();
        name = in.readString();
        site = in.readString();
        filename = in.readString();
        description = in.readString();
    }

    public static final Creator<Parc> CREATOR = new Creator<Parc>() {
        @Override
        public Parc createFromParcel(Parcel in) {
            return new Parc(in);
        }

        @Override
        public Parc[] newArray(int size) {
            return new Parc[size];
        }
    };

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }




    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public Object getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(code);
        parcel.writeString(name);
        parcel.writeString(site);
        parcel.writeString(filename);
        parcel.writeString(description);
    }
}
