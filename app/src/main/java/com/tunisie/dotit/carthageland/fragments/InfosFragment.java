package com.tunisie.dotit.carthageland.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tunisie.dotit.carthageland.R;
import com.tunisie.dotit.carthageland.activities.HomeActivity;
import com.tunisie.dotit.carthageland.adapters.RestaurantsAdapter;

/**
 * Created by dotit on 4/12/18.
 */

public class InfosFragment extends BaseFragment {

    View mView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.infos_fragment, container, false);
        Toolbar toolbarTop = (Toolbar) getActivity().findViewById(R.id.toolbar);
        TextView mTitle = (TextView) toolbarTop.findViewById(R.id.toolbar_title);
        mTitle.setText("Informations pratiques");
        return mView;
    }

    private void closeFragment() {
        getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();

    }

    public void fragmentreplace(Class fragmentClass,String tag) throws IllegalAccessException, java.lang.InstantiationException {
        FragmentManager fragmentManager=getFragmentManager();
        Fragment  fragment = (Fragment) fragmentClass.newInstance();
        FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.flContent,fragment , tag);
        fragmentTransaction.addToBackStack("tag"); //this will add it to back stack
        fragmentTransaction.commit();
    }
    private void navigateToHomeFragment()
    {
        Fragment fragment = null;
        try {
            closeFragment();
            fragment = HomeFragment.class.newInstance();

        } catch (Exception e) {
            e.printStackTrace();
        }

        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).addToBackStack(null).commit();
    }
}
