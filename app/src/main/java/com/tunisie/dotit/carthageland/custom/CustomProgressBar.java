package com.tunisie.dotit.carthageland.custom;

import android.content.Context;
import android.graphics.PorterDuff;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.widget.ProgressBar;

import com.tunisie.dotit.carthageland.R;


/**
 * Created on 2/2/18.
 */

public class CustomProgressBar extends ProgressBar {

    public CustomProgressBar(Context context) {
        super(context);
        initialize();
    }

    public CustomProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize();
    }

    public CustomProgressBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize();
    }

    private void initialize() {

        getIndeterminateDrawable()
                .setColorFilter(ContextCompat.getColor(getContext(), R.color.colorPrimary)
                        , PorterDuff.Mode.MULTIPLY);
    }
}
